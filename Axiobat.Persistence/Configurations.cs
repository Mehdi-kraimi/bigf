﻿namespace Axiobat.Persistence
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataAccess;
    using DataContext;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.DependencyInjection;
    using System;

    /// <summary>
    /// the Configurations class for registering the layer services
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public static class Configurations
    {
        /// <summary>
        /// add the Persistence layer services
        /// </summary>
        /// <param name="builder">the <see cref="ConfigurationsBuilder"/> instant</param>
        /// <returns>the <see cref="ConfigurationsBuilder"/> instant</returns>
        public static ConfigurationsBuilder AddPersistence(this ConfigurationsBuilder builder, Action<PersistenceOptions> options)
        {
            if (options is null)
                throw new ArgumentNullException(nameof(options));

            var option = new PersistenceOptions();
            options(option);

            if (!option.DatabaseConnectionString.IsValid())
                throw new ArgumentNullException(nameof(option.DatabaseConnectionString));

            builder.Services.AddDbContext<ApplicationDbContext>(contextOptions =>
            {
                contextOptions.EnableSensitiveDataLogging();
                contextOptions.UseMySql(option.DatabaseConnectionString);
                contextOptions.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                contextOptions.ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning));
            });

            builder.Services.AddSingleton(typeof(DataRequestBuilder<>));

            RegisterApplicationDataAccess(builder.Services);
            return builder;
        }

        /// <summary>
        /// this method is used to register all application dataAccess services
        /// </summary>
        /// <param name="services">the DI Service collection</param>
        private static void RegisterApplicationDataAccess(IServiceCollection services)
        {
            // entity framework based dataAccess
            services.AddScoped(typeof(IDataAccess<,>), typeof(DataAccess<,>));
            services.AddScoped<IUserDataAccess, UserDataAccess>();
            services.AddScoped<IRoleDataAccess, RoleDataAccess>();
            services.AddScoped<ICountryDataAccess, CountryDataAccess>();
            services.AddScoped<IGlobalHistoryDataAccess, GlobalHistoryDataAccess>();
            services.AddScoped<IClassificationDataAccess, ClassificationDataAccess>();
            services.AddScoped<IAccountingPeriodDataAccess, AccountingPeriodDataAccess>();
            services.AddScoped<IRecurringDocumentDataAccess, RecurringDocumentDataAccess>();
            services.AddScoped<IChartOfAccountsDataAccess, ChartAccountCategoryDataAccess>();
            services.AddScoped<IApplicationConfigurationDataAccess, ApplicationConfigurationDataAccess>();

            // accounting
            services.AddScoped<IAccountingDataAccess, AccountingDataAccess>();
            services.AddScoped<IAnalyticsDataAccess, AnalyticsDataAccess>();
            services.AddScoped<IDashboardDataAccess, DashboardDataAccess>();

            // external partners
            services.AddScoped<IGroupDataAccess, GroupeDataAccess>();
            services.AddScoped<IClientDataAccess, ClientDataAccess>();
            services.AddScoped<ISupplierDataAccess, SupplierDataAccess>();

            // products
            services.AddScoped<IProductDataAccess, ProductDataAccess>();
            services.AddScoped<ILotDataAccess, LotDataAccess>();
            services.AddScoped<ILabelDataAccess, LabelDataAccess>();

            // workshop management
            services.AddScoped<IWorkshopDocumentRubricDataAccess, WorkshopDocumentRubricDataAccess>();
            services.AddScoped<IConstructionWorkshopDataAccess, ConstructionWorkshopDataAccess>();
            services.AddScoped<IWorkshopDocumentDataAccess, WorkshopDocumentDataAccess>();
            services.AddScoped<IOperationSheetDataAccess, OperationSheetDataAccess>();

            // legal documents
            services.AddScoped<IQuoteDataAccess, QuoteDataAccess>();
            services.AddScoped<IInvoiceDataAccess, InvoiceDataAccess>();
            services.AddScoped<IExpenseDataAccess, ExpenseDataAccess>();
            services.AddScoped<IPaymentDataAccess, PaymentDataAccess>();
            services.AddScoped<ISupplierOrderDataAccess, SupplierOrderDataAccess>();
            services.AddScoped<ICreditNoteDataAccess, CreditNoteDataAccess>();

            // Maintenance
            services.AddScoped<IEquipmentMaintenanceDataAccess, EquipmentMaintenanceDataAccess>();
            services.AddScoped<IMaintenanceContractDataAccess, MaintenanceContractDataAccess>();
            services.AddScoped<IMaintenanceOperationSheetDataAccess, MaintenanceOperationSheetDataAccess>();
            services.AddScoped<IMaintenanceVisitDataAccess, MaintenanceVisitDataAccess>();

            // Mission
            services.AddScoped<IMissionDataAccess, MissionDataAccess>();
            services.AddScoped<IMissionTypeDataAccess, MissionTypeDataAccess>();
        }
    }
}
