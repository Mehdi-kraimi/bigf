﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Persistence.DataContext;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IDashboardDataAccess"/>
    /// </summary>
    public partial class DashboardDataAccess
    {
        public Task<Quote[]> GetQuotesForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.Quotes.AsNoTracking();

            if (filterOptions.DateStart.HasValue)
                query.Where(e => e.CreationDate.Date >= filterOptions.DateStart);

            if (filterOptions.DateEnd.HasValue)
                query.Where(e => e.CreationDate.Date <= filterOptions.DateEnd);

            return query.ToArrayAsync();
        }

        public Task<Invoice[]> GetInvoicesForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.Invoices.AsNoTracking();

            if (filterOptions.DateStart.HasValue)
                query.Where(e => e.CreationDate.Date >= filterOptions.DateStart);

            if (filterOptions.DateEnd.HasValue)
                query.Where(e => e.CreationDate.Date <= filterOptions.DateEnd);

            return query.ToArrayAsync();
        }

        public Task<ConstructionWorkshop[]> GetWorkshopsForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.Workshops.AsNoTracking()
                .Include(e => e.Invoices)
                .Include(e => e.CreditNotes)
                .Include(e => e.OperationSheets)
                .Include(e => e.Expenses);

            return query.ToArrayAsync();
        }

        public Task<MaintenanceContract[]> GetContractsForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.MaintenanceContracts.AsNoTracking();

            if (filterOptions.DateStart.HasValue)
                query.Where(e => e.StartDate.Date >= filterOptions.DateStart);

            if (filterOptions.DateEnd.HasValue)
                query.Where(e => e.StartDate.Date <= filterOptions.DateEnd);

            return query.ToArrayAsync();
        }

        public Task<OperationSheet[]> GetOperationSheetForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.OperationSheets.AsNoTracking();

            if (filterOptions.DateStart.HasValue)
                query.Where(e => e.StartDate.Date >= filterOptions.DateStart);

            if (filterOptions.DateEnd.HasValue)
                query.Where(e => e.StartDate.Date <= filterOptions.DateEnd);

            return query.ToArrayAsync();
        }

        public Task<MaintenanceOperationSheet[]> GetOperationSheetMaintenceForDocumentDetailsAsync(DateRangeFilterOptions filterOptions)
        {
            var query = _context.MaintenanceOperationSheets.AsNoTracking();

            if (filterOptions.DateStart.HasValue)
                query.Where(e => e.StartDate.Date >= filterOptions.DateStart);

            if (filterOptions.DateEnd.HasValue)
                query.Where(e => e.StartDate.Date <= filterOptions.DateEnd);

            return query.ToArrayAsync();
        }

        public Task<ConstructionWorkshop[]> GetWorkshopsForTopsDetailsAsync()
        {
            return _context.Workshops.AsNoTracking()
                .Include(e => e.Invoices)
                .Include(e => e.CreditNotes)
                .Include(e => e.OperationSheets)
                .ToArrayAsync();
        }

        public Task<MaintenanceContract[]> GetContractsForTopsDetailsAsync()
        {
            return _context.MaintenanceContracts.AsNoTracking()
                .Include(e => e.MaintenanceOperationsSheets)
                    .ThenInclude(e => e.Technician)
                .ToArrayAsync();
        }

        public Task<CreditNote[]> GetCreditNotesForTopsDetailsAsync()
        {
            return _context.CreditNotes.AsNoTracking()
                .ToArrayAsync();
        }

        public Task<Expense[]> GetExpensesForTopsDetailsAsync()
        {
            return _context.Expenses.AsNoTracking()
                .ToArrayAsync();
        }

        public Task<Invoice[]> GetInvoicesForTopsDetailsAsync()
        {
            return _context.Invoices.AsNoTracking()
                .ToArrayAsync();
        }

        public Task<Expense[]> GetExpensesForDetailsAsync(int year)
        {
            var yearDate = new DateTime(year, 12, 31);
            var preYearDate = new DateTime(year, 01, 01);

            return _context.Expenses.AsNoTracking()
                .Where(e => e.CreationDate.Date <= yearDate  && e.CreationDate.Date >= preYearDate)
                .ToArrayAsync();
        }

        public Task<Expense[]> GetExpensesForCashSummaryDetailsAsync()
        {
            return _context.Expenses.AsNoTracking()
                .Where(e =>
                    e.Status != ExpenseStatus.Canceled &&
                    e.Status != ExpenseStatus.Draft
                )
                .ToArrayAsync();
        }

        public Task<Invoice[]> GetInvoicesForCashSummaryDetailsAsync()
        {
            return _context.Invoices.AsNoTracking()
                .Where(e =>
                    e.Status != InvoiceStatus.Canceled &&
                    e.Status != InvoiceStatus.Draft
                )
               .ToArrayAsync();
        }

        public Task<Invoice[]> GetInvoicesForTurnOverDetailsAsync(int year)
        {
            var yearDate = new DateTime(year, 12, 31);
            var preYearDate = new DateTime(year, 01, 01);

            return _context.Invoices.AsNoTracking()
                .Where(e =>
                    e.CreationDate.Date <= yearDate &&
                    e.CreationDate.Date >= preYearDate &&
                    e.Status != InvoiceStatus.Canceled &&
                    e.Status != InvoiceStatus.Draft
                )
                .ToArrayAsync();
        }

        public Task<CreditNote[]> GetCreditNotesForTurnOverDetailsAsync(int year)
        {
            var yearDate = new DateTime(year, 12, 31);
            var preYearDate = new DateTime(year, 01, 01);

            return _context.CreditNotes.AsNoTracking()
                .Where(e =>
                    e.CreationDate.Date <= yearDate &&
                    e.CreationDate.Date >= preYearDate &&
                    e.Status != CreditNoteStatus.Draft)
                .ToArrayAsync();
        }

        public Task<Quote[]> GetQuotesForTurnOverDetailsAsync(int year)
        {
            var yearDate = new DateTime(year, 12, 31);
            var preYearDate = new DateTime(year, 01, 01);

            return _context.Quotes.AsNoTracking()
                .Where(e =>
                    e.CreationDate.Date <= yearDate &&
                    e.CreationDate.Date >= preYearDate &&
                    e.Status != QuoteStatus.Canceled &&
                    e.Status != QuoteStatus.Draft &&
                    e.Status != QuoteStatus.Refused
                )
               .ToArrayAsync();
        }
    }

    /// <summary>
    /// partial part for <see cref="DashboardDataAccess"/>
    /// </summary>
    public partial class DashboardDataAccess : IDashboardDataAccess
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public DashboardDataAccess(ApplicationDbContext dbContext, ILoggerFactory loggerFactory)
        {
            _context = dbContext;
            _logger = loggerFactory.CreateLogger<AccountingDataAccess>();
        }
    }
}
