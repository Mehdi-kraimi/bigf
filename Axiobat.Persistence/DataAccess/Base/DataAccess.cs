﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// an abstract implementation of the IDataSource interface
    /// </summary>
    /// <typeparam name="TEntity">the entity type</typeparam>
    /// <typeparam name="TKey">the entity key type</typeparam>
    public partial class DataAccess<TEntity, TKey>
    {
        #region Data manipulation operations

        /// <summary>
        /// add the given entity to the database
        /// </summary>
        /// <param name="entity">the entity to be added</param>
        /// <returns>the new added entity</returns>
        public virtual async Task<Result<TEntity>> AddAsync(TEntity entity, bool setDefaultData = true)
        {
            try
            {
                if (setDefaultData)
                {
                    entity.CreatedOn = DateTimeOffset.UtcNow;
                    entity.LastModifiedOn = DateTimeOffset.UtcNow;
                    entity.BuildSearchTerms();
                }

                var entry = await _entity.AddAsync(entity);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.InsertARecored, "Failed to add the [{EntityType}], unknown reason", Entity);
                    return Result.Failed<TEntity>($"failed adding the {Entity}, unknown reason", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.InsertARecored, "[{EntityType}] Added successfully with id [{entityId}]", Entity, entry.Entity.Id);
                return entry.Entity;
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARecored, ex, "Failed to add the [{EntityType}]", Entity);
                return Result.Failed<TEntity>($"Failed adding the {Entity}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// add the list of entities to the underlying database
        /// </summary>
        /// <param name="entities">the Entity to be add</param>
        /// <returns>the result of the operation</returns>
        public virtual async Task<Result> AddRangeAsync(IEnumerable<TEntity> entities, bool setDefaultData = true)
        {
            try
            {
                if (setDefaultData)
                {
                    foreach (var entity in entities)
                    {
                        entity.CreatedOn = DateTimeOffset.UtcNow;
                        entity.LastModifiedOn = DateTimeOffset.UtcNow;
                        entity.BuildSearchTerms();
                    }
                }

                await _entity.AddRangeAsync(entities);
                var result = await _context.SaveChangesAsync();

                if (result <= 0)
                {
                    _logger.LogWarning(LogEvent.InsertARangeOfRecoreds, "Failed to add the range of [{EntityType}], unknown reason", Entity);
                    return Result.Failed($"failed to add the range of {Entity}, unknown reason", MessageCode.OperationFailedUnknown);
                }

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARangeOfRecoreds, ex, "Failed adding list of [{EntityType}]", Entity, entities);
                return Result.Failed($"Failed to add the {Entity}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// update list of entities at ones
        /// </summary>
        /// <param name="entities">list of entities to be updated</param>
        /// <returns>the updated result</returns>
        public virtual async Task<Result> UpdateRangeAsync(IEnumerable<TEntity> entities, bool setDefaultData = true)
        {
            try
            {
                if (setDefaultData)
                {
                    foreach (var entity in entities)
                    {
                        entity.LastModifiedOn = DateTimeOffset.UtcNow;
                        entity.BuildSearchTerms();
                    }
                }

                _entity.UpdateRange(entities);
                var result = await _context.SaveChangesAsync();

                if (result <= 0)
                {
                    _logger.LogWarning(LogEvent.UpdateRangeOfRecoreds, "Failed to update range of [{EntityType}], unknown reason", Entity);
                    return Result.Failed($"failed to update the range of {Entity}, unknown reason", MessageCode.OperationFailedUnknown);
                }

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.UpdateRangeOfRecoreds, ex, "Failed updating list of [{EntityType}]", Entity, entities);
                return Result.Failed($"Failed to update the {Entity}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// update the entity
        /// </summary>
        /// <param name="entity">the new entity</param>
        /// <returns>the new updated entity</returns>
        public virtual async Task<Result<TEntity>> UpdateAsync(TEntity entity, bool setDefaultData = true)
        {
            try
            {
                if (setDefaultData)
                {
                    entity.LastModifiedOn = DateTimeOffset.UtcNow;
                    entity.BuildSearchTerms();
                }

                var entry = _entity.Update(entity);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.UpdatingData, "Failed to update the [{EntityType}], unknown reason", Entity);
                    return Result.Failed<TEntity>($"Failed to update the {Entity}", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.UpdatingData, "[{EntityType}] [id: {entityId}] updated successfully", Entity, entry.Entity.Id);
                return entry.Entity;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update [{EntityType}] cause of 'DbUpdateConcurrency'", Entity);
                return Result.Failed<TEntity>($"Failed to update the {Entity}", MessageCode.OperationFailedException, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update [{EntityType}] [id: {entityId}]", Entity, entity.Id);
                return Result.Failed<TEntity>($"Failed to update the {Entity}", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// use this method to update the entity without retrieving it
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="entityUpdater">the entity updater function</param>
        /// <returns>the operation result</returns>
        public virtual async Task<Result<TEntity>> UpdateAsync(TKey entityId, Action<TEntity> entityUpdater)
        {
            // validate input
            if (entityUpdater is null)
                throw new ArgumentNullException(nameof(entityUpdater));

            // retrieve the entity to be updated
            var entity = await GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<TEntity>($"{Entity} not exits", MessageCode.NotFound);

            // update the entity
            entityUpdater(entity);

            // persist the changes
            return await UpdateAsync(entity);
        }

        /// <summary>
        /// delete the entity with the given id
        /// </summary>
        /// <param name="id">the id of the entity to be deleted</param>
        /// <returns>an operation result</returns>
        public virtual async Task<Result> DeleteAsync(TKey id)
        {
            var entity = await GetByIdAsync(id);
            if (entity is null)
            {
                _logger.LogWarning(LogEvent.DeleteARecored, "cannot delete [{EntityType}] with id: {id}, cause we couldn't retrieve the entity", Entity, id);
                return Result.Failed($"there is no entity with the id : {id}", MessageCode.NotFound);
            }

            return await DeleteAsync(entity);
        }

        /// <summary>
        /// delete the entity
        /// </summary>
        /// <param name="entity">the entity to be deleted</param>
        /// <returns></returns>
        public virtual Task<Result> DeleteAsync(TEntity entity) => DeleteRangeAsync(entity);

        /// <summary>
        /// delete the given entities
        /// </summary>
        /// <param name="entities">entities to be deleted</param>
        /// <returns>the result object that describe the operation result</returns>
        public virtual async Task<Result> DeleteRangeAsync(params TEntity[] entities)
        {
            try
            {
                _entity.RemoveRange(entities);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.DeleteARangeOfRecoreds, "Failed to delete [{EntityType}](s), unknown reason", Entity);
                    return Result.Failed($"failed to delete {Entity}(s)", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.DeleteARangeOfRecoreds, "[{EntityType}](s) deleted successfully", Entity);
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.DeleteARangeOfRecoreds, ex, "Failed to delete [{EntityType}](s)", Entity);
                return Result.Failed($"Failed to delete the {Entity}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        #endregion

        #region Data retrieval Operations

        /// <summary>
        /// get the entity with the specified id
        /// </summary>
        /// <param name="entityId">the entity id</param>
        /// <param name="request">the dataRequest</param>
        /// <returns>the founded entity</returns>
        public virtual Task<TEntity> GetByIdAsync(TKey entityId)
            => GetSingle(null).SingleOrDefaultAsync(e => e.Id.Equals(entityId));

        /// <summary>
        /// get all the entities with the given ids
        /// </summary>
        /// <param name="EntitiesIds">the ids of the entities to retrieve</param>
        /// <returns>the list of entities</returns>
        public Task<TEntity[]> GetByIdAsync(params TKey[] EntitiesIds)
            => GetList(null).Where(e => EntitiesIds.Contains(e.Id)).ToArrayAsync();

        /// <summary>
        /// get the first result using the given data request options
        /// </summary>
        /// <param name="request">Data Request builder options</param>
        /// <returns>the result</returns>
        public virtual Task<TEntity> GetFirstAsync(Action<DataRequestBuilder<TEntity>> options)
            => GetSingle(GetDataRequest(options))
                .FirstOrDefaultAsync();

        /// <summary>
        /// get the first result using the given data request options
        /// </summary>
        /// <typeparam name="TResult">the type of the output</typeparam>
        /// <param name="options">the options builder</param>
        /// <param name="selector">the selector to select the desired out put</param>
        /// <returns>the result</returns>
        public virtual Task<TResult> GetFirstAsync<TResult>(Action<DataRequestBuilder<TEntity>> options, Expression<Func<TEntity, TResult>> selector)
            => GetSingle(GetDataRequest(options))
                .Select(selector)
                .FirstOrDefaultAsync();

        /// <summary>
        /// get a single result using the given data request options
        /// </summary>
        /// <param name="request">Data Request builder options</param>
        /// <returns>a single result</returns>
        public virtual Task<TEntity> GetSingleAsync(Action<DataRequestBuilder<TEntity>> options)
            => GetSingle(GetDataRequest(options))
                .SingleOrDefaultAsync();

        /// <summary>
        /// get a single result using the given data request options
        /// </summary>
        /// <typeparam name="TResult">the type of the output</typeparam>
        /// <param name="options">the options builder</param>
        /// <param name="selector">the selector to select the desired out put</param>
        /// <returns>a single result</returns>
        public virtual Task<TResult> GetSingleAsync<TResult>(Action<DataRequestBuilder<TEntity>> options, Expression<Func<TEntity, TResult>> selector)
            => GetSingle(GetDataRequest(options))
                .Select(selector)
                .SingleOrDefaultAsync();

        /// <summary>
        /// get the a list result using the given data request options
        /// </summary>
        /// <param name="options">the data request builder options</param>
        /// <returns>a list result of a given entity</returns>
        public virtual Task<TEntity[]> GetAllAsync(Action<DataRequestBuilder<TEntity>> options)
            => GetList(GetDataRequest(options)).ToArrayAsync();

        /// <summary>
        /// get the a list result using the given data request options
        /// </summary>
        /// <param name="options">the data request builder options</param>
        /// <returns>a list result of a given entity</returns>
        public virtual Task<TResult[]> GetAllAsync<TResult>(Action<DataRequestBuilder<TEntity>> options, Expression<Func<TEntity, TResult>> selector)
            => GetList(GetDataRequest(options))
                .Select(selector)
                .ToArrayAsync();

        /// <summary>
        /// get the paged result using the given filter type
        /// </summary>
        /// <typeparam name="IFilter">the type of the filter to use</typeparam>
        /// <param name="filterOption">the filter instant</param>
        /// <param name="request">the data request</param>
        /// <returns>paged result</returns>
        public virtual Task<PagedResult<TEntity>> GetPagedResultAsync<IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions
        {
            var query = GetList(null);

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            query = SetPagedResultFilterOptions(query, filterOption);

            return query
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get list of all entities keys
        /// </summary>
        /// <returns>list of all keys</returns>
        public virtual Task<TKey[]> GetKeysAsync()
            => _entity.Select(e => e.Id).ToArrayAsync();

        /// <summary>
        /// get the count of items in dataSource using the given predicate
        /// </summary>
        /// <param name="predicate">the predicate to retrieve the count by it</param>
        public virtual Task<int> GetCountAsync(Expression<Func<TEntity, bool>> predicate)
            => _entity.AsNoTracking().CountAsync(predicate);

        #endregion

        #region Check existence

        /// <summary>
        /// check if there is any entity that matches the given predicate
        /// </summary>
        /// <param name="predicate">the predicate to be evaluated</param>
        /// <returns>true if exist, false if not</returns>
        public virtual Task<bool> IsExistAsync(Expression<Func<TEntity, bool>> predicate)
            => _entity.AnyAsync(predicate);

        /// <summary>
        /// check if the entity with the given id is exist
        /// </summary>
        /// <param name="entityId">the entity id</param>
        /// <returns>true if exist, false if not</returns>
        public virtual Task<bool> IsExistAsync(TKey entityId)
            => _entity.AnyAsync(e => e.Id.Equals(entityId));

        /// <summary>
        /// validate if all <see cref="TEntity"/> keys exist or not
        /// </summary>
        /// <param name="keysToValidate">the list of keys to validate</param>
        /// <returns>true if all exist, false if not</returns>
        public async Task<bool> IsAllkeysExistAsync(params TKey[] keysToValidate)
        {
            var keys = await GetKeysAsync();
            return keysToValidate.All(keys.Contains);
        }

        #endregion
    }

    /// <summary>
    /// the partial part of <see cref="DataAccess{TEntity, TKey}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity </typeparam>
    /// <typeparam name="TKey">the type of the primary key</typeparam>
    public partial class DataAccess<TEntity, TKey> : IDataAccess<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        protected readonly DataRequestBuilder<TEntity> _dataRequestBuilder;
        protected readonly ApplicationDbContext _context;
        protected readonly ILogger _logger;

        /// <summary>
        /// the entity Db set
        /// </summary>
        protected readonly DbSet<TEntity> _entity;

        /// <summary>
        /// the entity name, this is just the name of the class
        /// </summary>
        public string Entity => typeof(TEntity).Name;

        /// <summary>
        /// default constructor that tacks the context
        /// </summary>
        /// <param name="context">the DataSource</param>
        public DataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _entity = _context.Set<TEntity>();
            _dataRequestBuilder = new DataRequestBuilder<TEntity>();
            _logger = loggerFactory.CreateLogger($"[{Entity}].DataAccess");
        }

        /// <summary>
        /// get the data request instant from the given action builder
        /// </summary>
        /// <param name="options">the action builder</param>
        /// <returns>the data request instant</returns>
        private DataRequest<TEntity> GetDataRequest(Action<DataRequestBuilder<TEntity>> options)
        {
            if (options is null)
                return null;

            options(_dataRequestBuilder);
            return _dataRequestBuilder.Build();
        }

        /// <summary>
        /// get the IQueryable of entities to query the entities as a single instant from the database using the passed in DataRequest
        /// </summary>
        /// <param name="request">the DataRequest object</param>
        /// <param name="defaultIncludes">whether to include the default Includes or not</param>
        /// <returns>IQueryable of entities</returns>
        protected IQueryable<TEntity> GetSingle(DataRequest<TEntity> request = null)
        {
            var query = _entity.AsNoTracking();
            if (request is null)
            {
                query = SetDefaultIncludsForSingleRetrieve(query);
                return query;
            }

            if (request.WithDefaultIncludes)
                query = SetDefaultIncludsForSingleRetrieve(query);

            return query.GetWithDataRequest(request);
        }

        /// <summary>
        /// get the IQueryable of entities to query the entities from the database using the
        /// passed in DataRequest
        /// </summary>
        /// <param name="request">the DataRequest object</param>
        /// <param name="defaultIncludes">whether to include the default Includes or not</param>
        /// <returns>IQueryable of entities</returns>
        protected IQueryable<TEntity> GetList(DataRequest<TEntity> request = null)
        {
            var query = _entity.AsNoTracking();
            if (request is null)
            {
                query = SetDefaultIncludsForListRetrieve(query);
                return query;
            }

            if (request.WithDefaultIncludes)
                query = SetDefaultIncludsForListRetrieve(query);

            return query.GetWithDataRequest(request);
        }

        /// <summary>
        /// this method is used to set the default includes of the entity when returning a list of entities
        /// </summary>
        /// <param name="query">the query instant</param>
        /// <returns>the IQueryble instant</returns>
        protected virtual IQueryable<TEntity> SetDefaultIncludsForListRetrieve(IQueryable<TEntity> query) => query;

        /// <summary>
        /// set the default includes for single entity retrieval ex: GetById etc ...
        /// </summary>
        /// <param name="query">the query instant</param>
        /// <returns>the IQueryble instant</returns>
        protected virtual IQueryable<TEntity> SetDefaultIncludsForSingleRetrieve(IQueryable<TEntity> query) => query;

        /// <summary>
        /// set the default filter option
        /// </summary>
        /// <typeparam name="IFilter">the filter type</typeparam>
        /// <param name="query">the query instant</param>
        /// <param name="filterOption">the filter options instant</param>
        protected virtual IQueryable<TEntity> SetPagedResultFilterOptions<IFilter>(IQueryable<TEntity> query, IFilter filterOption) where IFilter : IFilterOptions => query;
    }
}
