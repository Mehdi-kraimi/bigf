﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using Persistence.DataContext;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    public partial class AccountingPeriodDataAccess
    {
        /// <summary>
        /// get the current accounting period
        /// </summary>
        /// <returns>the accounting period value</returns>
        public Task<AccountingPeriod> CurrentAccountingPeriodAsync()
            => _context.AccountingPeriods.OrderByDescending(e => e.StartingDate).FirstOrDefaultAsync(e => e.EndingDate == null);
    }

    public partial class AccountingPeriodDataAccess : DataAccess<AccountingPeriod, int>, IAccountingPeriodDataAccess
    {
        public AccountingPeriodDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }
    }
}
