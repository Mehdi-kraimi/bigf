﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// data access implantation for <see cref="ILotDataAccess"/>
    /// </summary>
    public partial class LotDataAccess
    {
        /// <summary>
        /// get the list of the products that the Lot owns
        /// </summary>
        /// <param name="lotId">the id of the lots</param>
        /// <returns>the list of products</returns>
        public Task<IEnumerable<Product>> GetLotProducts(string lotId)
            => _context.Products.AsNoTracking()
                .Join(_context.Lots_Products, p => p.Id, lp => lp.ProductId, (p, lp) => new { product = p, lotProduct = lp })
                .Where(q => q.lotProduct.LotId == lotId)
                .Select(q => q.product)
                .ToIEnumerableAsync();

        /// <summary>
        /// add a range of lot products
        /// </summary>
        public async Task<Result> AddLotProductRangeAsync(IEnumerable<LotProduct> lotProducts)
        {
            try
            {
                await _context.Lots_Products.AddRangeAsync(lotProducts);
                var result = await _context.SaveChangesAsync();

                if (result <= 0)
                {
                    _logger.LogWarning(LogEvent.InsertARangeOfRecoreds, "Failed to add the list of lotProducts, unknown reason");
                    return Result.Failed($"failed to add the list of lotProducts, unknown reason", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.InsertARangeOfRecoreds, "[{insertedCount}] lotProducts out of [{itemsCount}] have been inserted", result, lotProducts.Count());
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARangeOfRecoreds, ex, "Failed adding list of [{count}] lotProducts", lotProducts.Count());
                return Result.Failed($"Failed to add the {Entity}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// delete all lot products
        /// </summary>
        /// <param name="id">the id of the lot to delete the products for it</param>
        /// <returns>the operation result</returns>
        public Task DeleteLotProductsAsync(string id)
        {
            var sql = $"DELETE FROM {TablesNames.LotProducts} WHERE {nameof(LotProduct.LotId)} = '{id}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }


    }

    /// <summary>
    /// partial part for <see cref="LotDataAccess"/>
    /// </summary>
    public partial class LotDataAccess : ProductBaseDataAccess<Lot>, ILotDataAccess
    {
        public LotDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Lot> SetDefaultIncludsForSingleRetrieve(IQueryable<Lot> query)
            => query
                .Include(e => e.Products)
                    .ThenInclude(e => e.Product)
                        .ThenInclude(e => e.Suppliers)
                .Include(e => e.Products)
                    .ThenInclude(e => e.Product)
                        .ThenInclude(e => e.Classification)
                            .ThenInclude(e => e.ChartAccountItem);

        protected override IQueryable<Lot> SetDefaultIncludsForListRetrieve(IQueryable<Lot> query)
            => query
                .Include(e => e.Products)
                    .ThenInclude(e => e.Product)
                        .ThenInclude(e => e.Suppliers)
                .Include(e => e.Products)
                    .ThenInclude(e => e.Product)
                        .ThenInclude(e => e.Classification)
                            .ThenInclude(e => e.ChartAccountItem);
    }
}
