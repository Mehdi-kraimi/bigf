﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using DataContext;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the data access implantation for <see cref="ProductsBase"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public partial class ProductBaseDataAccess<TEntity>
    {

    }

    /// <summary>
    /// the partial part for <see cref="ProductBaseDataAccess{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the product</typeparam>
    public partial class ProductBaseDataAccess<TEntity> : DataAccess<TEntity, string>, IProductsBaseDataAccess<TEntity>
        where TEntity : ProductsBase
    {
        public ProductBaseDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }
    }
}
