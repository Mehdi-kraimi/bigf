﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implantation for <see cref="IProductDataAccess"/>
    /// </summary>
    public partial class ProductDataAccess
    {
        /// <summary>
        /// set the paged result filter options
        /// </summary>
        /// <typeparam name="IFilter">the filter options type</typeparam>
        /// <param name="query">the query instant</param>
        /// <param name="filterOption">the filter option instant</param>
        /// <returns>the new query</returns>
        protected override IQueryable<Product> SetPagedResultFilterOptions<IFilter>(IQueryable<Product> query, IFilter filterOption)
        {
            if (filterOption is ProductFilterOptions productFilter)
            {
                if (productFilter.ClassificationPrentId.HasValue)
                {
                    query = query
                        .Join(_context.Classifications, p => p.ClassificationId, s => s.Id, (p, s) => new { Product = p, Classification = s })
                        .Where(e => e.Classification.ParentId == productFilter.ClassificationPrentId)
                        .Select(e => e.Product);
                }
                if (productFilter.SupplierId.IsValid())
                {
                    query = query.
                        Join(_context.Products_Suppliers, p => p.Id, s => s.ProductId, (p, s) => new { Product = p, Supplier = s })
                         .Where(e => e.Supplier.SupplierId == productFilter.SupplierId)
                         .Select(e => e.Product);
                }
                if ( productFilter.Label.Count > 0)
                {
                    query = query                       
                       .Join(_context.Products_Labels, q => q.Id, l => l.ProductId, (q, l) => new { Product = q, Label = l })
                       .Where(e => productFilter.Label.Contains(e.Label.LabelId))
                       .Select(e => e.Product);
                }
            }

            return query;
        }

        /// <summary>
        /// add the product suppliers relationships
        /// </summary>
        /// <param name="supplierProdcuts">the supplier product</param>
        /// <returns>the operations result</returns>
        public async Task<Result> AddProductsSuppliersAsync(params ProductSupplier[] supplierProdcuts)
        {
            try
            {
                await _context.Products_Suppliers.AddRangeAsync(supplierProdcuts);
                var result = await _context.SaveChangesAsync();

                if (result <= 0 && supplierProdcuts.Length > 0)
                {
                    _logger.LogWarning(LogEvent.InsertARangeOfRecoreds, "Failed to add the list of products suppliers, unknown reason");
                    return Result.Failed($"failed to add the range of products suppliers, unknown reason", MessageCode.OperationFailedUnknown);
                }

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARangeOfRecoreds, ex, "Failed adding list of products suppliers");
                return Result.Failed($"Failed to add the products suppliers", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// add the products labels
        /// </summary>
        /// <param name="productLables">the product labels</param>
        /// <returns>the operations result</returns>
        public async Task<Result> AddProductLabelsAsync(params ProductLabel[] productLables)
        {
            try
            {
                await _context.Products_Labels.AddRangeAsync(productLables);
                var result = await _context.SaveChangesAsync();

                if (result <= 0 && productLables.Length > 0)
                {
                    _logger.LogWarning(LogEvent.InsertARangeOfRecoreds, "Failed to add the list of products labels, unknown reason");
                    return Result.Failed($"failed to add the range of  products labels, unknown reason", MessageCode.OperationFailedUnknown);
                }

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARangeOfRecoreds, ex, "Failed adding list of products labels");
                return Result.Failed($"Failed to add the products labels", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// delete the product suppliers
        /// </summary>
        /// <param name="productId">the id of the product</param>
        public Task DeleteProductSuppliersAsync(string productId)
        {
            var sql = $"DELETE FROM {TablesNames.ProductsSuppliers} WHERE {nameof(ProductSupplier.ProductId)} = '{productId}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        /// <summary>
        /// delete the product labels
        /// </summary>
        /// <param name="productId">the id of the product</param>
        public Task DeleProductLabelsAsync(string productId)
        {
            var sql = $"DELETE FROM {TablesNames.ProductsLabels} WHERE {nameof(ProductLabel.ProductId)} = '{productId}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        /// <summary>
        /// retrieve list of product by supplier Id
        /// </summary>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of products</returns>
        public Task<IEnumerable<Product>> GetBySupplierIdAsync(string supplierId)
            => _context.Products
                .Include(e => e.Suppliers)
                .Include(e => e.Classification)
                .Include(e => e.ProductCategoryType)
                .Join(_context.Products_Suppliers, p => p.Id, sp => sp.ProductId, (p, sp) => new { Product = p, ProductSupplier = sp })
                .Where(q => q.ProductSupplier.SupplierId == supplierId)
                .Select(q => q.Product)
                .ToIEnumerableAsync();

        /// <summary>
        /// get the list of the product categories types
        /// </summary>
        /// <returns>list of categories types</returns>
        public Task<IEnumerable<ProductCategoryType>> GetProductCategoriesTypeAsync()
            => _context.ProductCategoryTypes.ToIEnumerableAsync();

        /// <summary>
        /// check if the given category type id belongs to an existing category
        /// </summary>
        /// <param name="categoryTypeId">the id of the category to check</param>
        /// <returns>true if exist, false if not</ret
        public Task<bool> IsProductCategoryTypeExistAsync(string categoryTypeId)
            => _context.ProductCategoryTypes.AnyAsync(category => category.Id == categoryTypeId);
    }

    /// <summary>
    /// partial part for <see cref="ProductDataAccess"/>
    /// </summary>
    public partial class ProductDataAccess : ProductBaseDataAccess<Product>, IProductDataAccess
    {
        public ProductDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Product> SetDefaultIncludsForListRetrieve(IQueryable<Product> query)
            => query
                .Include(e => e.ProductCategoryType)
                .Include(e => e.Classification)
                    .ThenInclude(e => e.ChartAccountItem)
                .Include(e => e.ProductCategoryType)
                .Include(e => e.Labels)
                    .ThenInclude(e => e.Label)
                .Include(e => e.Suppliers)
                    .ThenInclude(e => e.Supplier);

        protected override IQueryable<Product> SetDefaultIncludsForSingleRetrieve(IQueryable<Product> query)
            => query
                .Include(e => e.ProductCategoryType)
                .Include(e => e.Classification)
                    .ThenInclude(e => e.ChartAccountItem)
                .Include(e => e.Labels)
                    .ThenInclude(e => e.Label)
                .Include(e => e.Suppliers)
                   .ThenInclude(e => e.Supplier);
    }
}
