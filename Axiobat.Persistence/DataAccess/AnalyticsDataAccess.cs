﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Persistence.DataContext;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Threading.Tasks;

    public partial class AnalyticsDataAccess
    {
        public Task<IEnumerable<ConstructionWorkshop>> GetWorkshopForAnalyticsAsync(WorkshopAnalyticsFilterOtions filterModel)
        {
            var query = _context.Workshops
                .Include(e => e.Invoices)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Quotes)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Expenses)
                .Include(e => e.OperationSheets)
                .Include(e => e.CreditNotes)
                .AsQueryable();

            if (!(filterModel is null))
            {
                if (filterModel.WorkshopId.Count() > 0)
                    query = query.Where(e => filterModel.WorkshopId.Contains(e.Id));

                if (filterModel.ClientId.Count() > 0)
                    query = query.Where(e => filterModel.ClientId.Contains(e.ClientId));

                if (filterModel.GroupeId.Count() > 0)
                    query = query
                        .Join(_context.Clients, w => w.ClientId, c => c.Id, (w, c) => new { Client = c, Workshop = w })
                        .Join(_context.Groupes, q => q.Client.GroupeId, g => g.Id, (q, g) => new { q.Client, q.Workshop, Groupe = g })
                        .Where(q => filterModel.GroupeId.Contains(q.Groupe.Id))
                        .Select(q => q.Workshop);
            }

            return query.ToIEnumerableAsync();
        }

        public Task<IEnumerable<CreditNote>> GetCreditNotesForAnalyticsAsync(ExternalPartnerFilterOptions filter)
        {
            var query = _context.CreditNotes.AsNoTracking();

            if (!(filter is null))
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.ClientId));
            }

            return query.ToIEnumerableAsync();
        }

        public Task<IEnumerable<Expense>> GetExpensesForAnalyticsAsync(ExternalPartnerFilterOptions filter)
        {
            var query = _context.Expenses
                .Include(e => e.Payments)
                      .ThenInclude(e => e.Payment)
                .AsNoTracking();

            if (!(filter is null))
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.SupplierId));
            }

            return query.ToIEnumerableAsync();
        }

        public Task<IEnumerable<Invoice>> GetInvoicesForAnalyticsAsync(ExternalPartnerFilterOptions filter)
        {
            var query = _context.Invoices
                .Include(e => e.Payments)
                      .ThenInclude(e => e.Payment)
                .AsNoTracking();

            if (!(filter is null))
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.ClientId));
            }

            return query.ToIEnumerableAsync();
        }

        public Task<IEnumerable<Expense>> GetExpensesForPurchaseAnalyticsAsync(TransactionAnalyticsFilterOptions filter)
        {
            var query = _context.Expenses
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                .AsQueryable();

            if (!(filter is null))
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.SupplierId));
            }

            return query.ToIEnumerableAsync();
        }

        public Task<IEnumerable<MaintenanceContract>> GetMaintenanceContractsForAnalyticsAsync(MaintenanceAnalyticsFilterOptions filter)
        {
            IQueryable<MaintenanceContract> query = _context.MaintenanceContracts
                .Include(e => e.MaintenanceOperationsSheets)
                    .ThenInclude(e => e.MaintenanceVisit)
                .Include(e => e.MaintenanceOperationsSheets)
                    .ThenInclude(e => e.Technician)
                .Include(e => e.Invoices);

            if (filter.DateStart.HasValue)
                query = query.Where(e => e.StartDate >= filter.DateStart.Value);

            if (filter.DateEnd.HasValue)
                query = query.Where(e => e.EndDate <= filter.DateEnd);

            if (filter.Technicians.Any())
            {
                query = query
                    .Join(_context.MaintenanceOperationSheets, c => c.Id, m => m.MaintenanceContractId, (contract, operationSheets) => new { contract, operationSheets })
                    .Where(e => filter.Technicians.Contains(e.operationSheets.TechnicianId.Value))
                    .Select(e => e.contract);
            }

            if (filter.ClientId.Any())
            {
                query = query
                    .Where(e => filter.ClientId.Contains(e.ClientId));
            }

            if (filter.GroupeId.Any())
            {
                query = query
                    .Join(_context.Clients, m => m.ClientId, c => c.Id, (m, c) => new { client = c, contract = m })
                    .Join(_context.Groupes, q => q.client.GroupeId, g => g.Id, (q, g) => new { q.client, q.contract, groupe = g })
                    .Where(e => filter.GroupeId.Contains(e.groupe.Id))
                    .Select(e => e.contract);
            }

            return query
                .ToIEnumerableAsync();
        }
    }

    public partial class AnalyticsDataAccess : IAnalyticsDataAccess
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public AnalyticsDataAccess(ApplicationDbContext dbContext, ILoggerFactory loggerFactory)
        {
            _context = dbContext;
            _logger = loggerFactory.CreateLogger<AccountingDataAccess>();
        }

        public Task<IEnumerable<Quote>> getQuotesForAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel)
        {
            return _context.Quotes.ToIEnumerableAsync();
        }
    }
}
