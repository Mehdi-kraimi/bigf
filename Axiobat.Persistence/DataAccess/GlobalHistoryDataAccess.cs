﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using DataContext;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// the data access for <see cref="GlobalHistory"/> implementing <see cref="IGlobalHistoryDataAccess"/>
    /// </summary>
    public partial class GlobalHistoryDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="GlobalHistoryDataAccess"/>
    /// </summary>
    public partial class GlobalHistoryDataAccess : DataAccess<GlobalHistory, int>, IGlobalHistoryDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="GlobalHistoryDataAccess"/>
        /// </summary>
        /// <param name="context">the <see cref="ApplicationDbContext"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        public GlobalHistoryDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<GlobalHistory> SetDefaultIncludsForListRetrieve(IQueryable<GlobalHistory> query)
            => base.SetDefaultIncludsForListRetrieve(query);

        protected override IQueryable<GlobalHistory> SetPagedResultFilterOptions<IFilter>(IQueryable<GlobalHistory> query, IFilter filterOption)
            => base.SetPagedResultFilterOptions(query, filterOption);
    }
}
