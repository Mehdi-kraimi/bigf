﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// the data access implementation for Mission Type
    /// </summary>
    public partial class MissionTypeDataAccess
    {

    }

    /// <summary>
    /// the partial part for <see cref="MissionTypeDataAccess"/>
    /// </summary>
    public partial class MissionTypeDataAccess : DataAccess<MissionType, int>, IMissionTypeDataAccess
    {
        public MissionTypeDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<MissionType> SetPagedResultFilterOptions<IFilter>(IQueryable<MissionType> query, IFilter filterOption)
        {
            if (filterOption is MissionTypeFilterOptions filter)
            {
                if (filter.MissionKinds.Any())
                    query = query.Where(missionType => filter.MissionKinds.Contains(missionType.MissionKind));
            }

            return query;
        }
    }
}
