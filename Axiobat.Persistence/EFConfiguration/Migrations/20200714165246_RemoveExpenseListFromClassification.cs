﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class RemoveExpenseListFromClassification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Classifications_ClassificationId",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_ClassificationId",
                table: "Expenses");

            migrationBuilder.DropColumn(
                name: "ClassificationId",
                table: "Expenses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassificationId",
                table: "Expenses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ClassificationId",
                table: "Expenses",
                column: "ClassificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Classifications_ClassificationId",
                table: "Expenses",
                column: "ClassificationId",
                principalTable: "Classifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
