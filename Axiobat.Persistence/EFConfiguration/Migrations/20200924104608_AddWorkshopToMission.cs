﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddWorkshopToMission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WorkshopId",
                table: "Missions",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Missions_WorkshopId",
                table: "Missions",
                column: "WorkshopId");

            migrationBuilder.AddForeignKey(
                name: "FK_Missions_ConstructionWorkshops_WorkshopId",
                table: "Missions",
                column: "WorkshopId",
                principalTable: "ConstructionWorkshops",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Missions_ConstructionWorkshops_WorkshopId",
                table: "Missions");

            migrationBuilder.DropIndex(
                name: "IX_Missions_WorkshopId",
                table: "Missions");

            migrationBuilder.DropColumn(
                name: "WorkshopId",
                table: "Missions");
        }
    }
}
