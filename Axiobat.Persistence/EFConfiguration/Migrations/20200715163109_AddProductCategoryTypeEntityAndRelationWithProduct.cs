﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddProductCategoryTypeEntityAndRelationWithProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductCategoryTypeId",
                table: "Products",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "ProductCategoryType",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Label = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategoryType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductCategoryTypeId",
                table: "Products",
                column: "ProductCategoryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategoryType_Label",
                table: "ProductCategoryType",
                column: "Label");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategoryType_SearchTerms",
                table: "ProductCategoryType",
                column: "SearchTerms");

            migrationBuilder.Sql("INSERT INTO `ProductCategoryType` (`CreatedOn`, `LastModifiedOn`, `SearchTerms`, `Id`, `Label`, `Description`, `Type`) VALUES ('2020-07-15 17:23:47.000000', '2020-07-15 17:23:49.000000', 'Achats Matériels', '1', 'Achats Matériels', 'Achats Matériels', '8') ON DUPLICATE KEY UPDATE `Label` = 'Achats Matériels';");
            migrationBuilder.Sql("UPDATE Products SET ProductCategoryTypeId = '1'");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ProductCategoryType_ProductCategoryTypeId",
                table: "Products",
                column: "ProductCategoryTypeId",
                principalTable: "ProductCategoryType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ProductCategoryType_ProductCategoryTypeId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "ProductCategoryType");

            migrationBuilder.DropIndex(
                name: "IX_Products_ProductCategoryTypeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ProductCategoryTypeId",
                table: "Products");
        }
    }
}
