﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddRecurringDocumentEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AlterColumn<string>(
            //    name: "Label",
            //    table: "ChartAccountCategories",
            //    maxLength: 256,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 100,
            //    oldNullable: true);

            //migrationBuilder.CreateTable(
            //    name: "Classifications",
            //    columns: table => new
            //    {
            //        CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
            //        LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
            //        SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
            //        Type = table.Column<int>(nullable: false),
            //        Label = table.Column<string>(maxLength: 256, nullable: true),
            //        Description = table.Column<string>(maxLength: 500, nullable: true),
            //        ChartAccountItemId = table.Column<string>(maxLength: 256, nullable: true),
            //        ParentId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Classifications", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Classifications_ChartAccountCategories_ChartAccountItemId",
            //            column: x => x.ChartAccountItemId,
            //            principalTable: "ChartAccountCategories",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_Classifications_Classifications_ParentId",
            //            column: x => x.ParentId,
            //            principalTable: "Classifications",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateTable(
                name: "RecurringDocuments",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    PeriodicityOptions = table.Column<string>(type: "LONGTEXT", nullable: true),
                    PeriodicityType = table.Column<int>(nullable: false),
                    EndingType = table.Column<int>(nullable: false),
                    OccurringCount = table.Column<uint>(nullable: false),
                    EndingDate = table.Column<DateTime>(nullable: true),
                    NextOccurring = table.Column<DateTime>(nullable: true),
                    StartingDate = table.Column<DateTime>(nullable: false),
                    Counter = table.Column<uint>(nullable: false),
                    Status = table.Column<string>(maxLength: 256, nullable: true),
                    DocumentType = table.Column<int>(nullable: false),
                    Document = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Error = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecurringDocuments", x => x.Id);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Classifications_ChartAccountItemId",
            //    table: "Classifications",
            //    column: "ChartAccountItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Classifications_ParentId",
            //    table: "Classifications",
            //    column: "ParentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Classifications_SearchTerms",
            //    table: "Classifications",
            //    column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_RecurringDocuments_SearchTerms",
                table: "RecurringDocuments",
                column: "SearchTerms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Classifications");

            migrationBuilder.DropTable(
                name: "RecurringDocuments");

            //migrationBuilder.AlterColumn<string>(
            //    name: "Label",
            //    table: "ChartAccountCategories",
            //    maxLength: 100,
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 256,
            //    oldNullable: true);
        }
    }
}
