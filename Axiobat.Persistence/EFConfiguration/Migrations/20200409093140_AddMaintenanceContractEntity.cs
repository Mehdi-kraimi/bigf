﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddMaintenanceContractEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MaintenanceContracts",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 20, nullable: true),
                    Site = table.Column<string>(maxLength: 256, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<string>(maxLength: 256, nullable: false),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Attachments = table.Column<string>(type: "LONGTEXT", nullable: true),
                    EquipmentMaintenance = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintenanceContracts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaintenanceContracts_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_Status",
                table: "SuppliersOrders",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_Status",
                table: "Quotes",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Status",
                table: "Invoices",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_Status",
                table: "Expenses",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_Status",
                table: "CreditNotes",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_ClientId",
                table: "MaintenanceContracts",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_Reference",
                table: "MaintenanceContracts",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_SearchTerms",
                table: "MaintenanceContracts",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_Status",
                table: "MaintenanceContracts",
                column: "Status");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaintenanceContracts");

            migrationBuilder.DropIndex(
                name: "IX_SuppliersOrders_Status",
                table: "SuppliersOrders");

            migrationBuilder.DropIndex(
                name: "IX_Quotes_Status",
                table: "Quotes");

            migrationBuilder.DropIndex(
                name: "IX_Invoices_Status",
                table: "Invoices");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_Status",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_CreditNotes_Status",
                table: "CreditNotes");
        }
    }
}
