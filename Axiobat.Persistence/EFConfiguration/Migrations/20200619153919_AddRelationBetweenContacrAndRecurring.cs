﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddRelationBetweenContacrAndRecurring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RecurringDocumentId",
                table: "MaintenanceContracts",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_RecurringDocumentId",
                table: "MaintenanceContracts",
                column: "RecurringDocumentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceContracts_RecurringDocuments_RecurringDocumentId",
                table: "MaintenanceContracts",
                column: "RecurringDocumentId",
                principalTable: "RecurringDocuments",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceContracts_RecurringDocuments_RecurringDocumentId",
                table: "MaintenanceContracts");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceContracts_RecurringDocumentId",
                table: "MaintenanceContracts");

            migrationBuilder.DropColumn(
                name: "RecurringDocumentId",
                table: "MaintenanceContracts");
        }
    }
}
