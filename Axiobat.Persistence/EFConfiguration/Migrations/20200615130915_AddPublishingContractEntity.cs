﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddPublishingContractEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PublishingContract",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 256, nullable: true),
                    FileName = table.Column<string>(maxLength: 256, nullable: true),
                    OrginalFileName = table.Column<string>(maxLength: 256, nullable: true),
                    UserId = table.Column<Guid>(nullable: true),
                    Tags = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublishingContract", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PublishingContract_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PublishingContract_SearchTerms",
                table: "PublishingContract",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_PublishingContract_UserId",
                table: "PublishingContract",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PublishingContract");
        }
    }
}
