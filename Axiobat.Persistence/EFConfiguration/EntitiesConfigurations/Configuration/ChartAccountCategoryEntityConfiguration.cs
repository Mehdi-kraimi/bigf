﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// the <see cref="ConstructionWorkshop"/> entity configuration
    /// </summary>
    internal class ChartAccountCategoryEntityConfiguration : IEntityTypeConfiguration<ChartAccountItem>
    {
        public void Configure(EntityTypeBuilder<ChartAccountItem> builder)
        {
            // table name
            builder.ToTable("ChartAccountCategories");

            // properties
            builder.Property(e => e.Description)
                .HasMaxLength(500);

            builder.Property(e => e.Code)
                .HasMaxLength(15);

            // relationships
            builder.HasMany(e => e.SubCategories)
                .WithOne()
                .HasForeignKey(e => e.ParentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}