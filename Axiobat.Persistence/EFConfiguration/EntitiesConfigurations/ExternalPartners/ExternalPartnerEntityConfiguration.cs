﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class ExternalPartnerEntityConfiguration<TEntity>
        where TEntity : ExternalPartner
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Ignore(e => e.DocumentType);
            builder.Ignore(e => e.FullName);

            // properties configurations
            builder.Property(e => e.Siret)
                .HasMaxLength(25);

            builder.Property(e => e.Reference)
                .HasMaxLength(50);

            builder.Property(e => e.AccountingCode)
                .HasMaxLength(100);

            builder.Property(e => e.PhoneNumber)
                .HasMaxLength(20);

            builder.Property(e => e.LandLine)
                .HasMaxLength(20);

            builder.HasQueryFilter(e => !e.IsDeleted);

            // indexes
            builder.HasIndex(e => e.Reference);

            // conversions
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.ContactInformations)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<ContactInformation>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");
        }
    }
}
