﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class SupplierOrderEntityConfiguration : DocumentEntityConfiguration<SupplierOrder>, IEntityTypeConfiguration<SupplierOrder>
    {
        public override void Configure(EntityTypeBuilder<SupplierOrder> builder)
        {
            // set the base configurations
            base.Configure(builder);

            // table name
            builder.ToTable("SuppliersOrders");

            // converters
            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<OrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Supplier)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<SupplierDocuement>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne<Supplier>()
                .WithMany(e => e.SupplierOrders)
                .HasForeignKey(e => e.SupplierId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            // relationships
            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.SupplierOrders)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Quote)
                .WithMany(e => e.SupplierOrders)
                .HasForeignKey(e => e.QuoteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
