﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class ExpenseEntityConfiguration : DocumentEntityConfiguration<Expense>, IEntityTypeConfiguration<Expense>
    {
        public override void Configure(EntityTypeBuilder<Expense> builder)
        {
            // base configuration
            base.Configure(builder);

            // table name
            builder.ToTable("Expenses");

            // converters
            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<OrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.CancellationDocument)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Memo>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Supplier)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<SupplierDocuement>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne<Supplier>()
                .WithMany(e => e.Expenses)
                .HasForeignKey(e => e.SupplierId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.Expenses)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Property(e => e.Attachments)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Attachment>>() ?? new List<Attachment>())
                 .HasColumnType("LONGTEXT");
        }
    }
}
