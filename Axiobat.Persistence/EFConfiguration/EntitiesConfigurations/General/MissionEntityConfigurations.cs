﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// the <see cref="Mission"/> entity configuration
    /// </summary>
    internal class MissionEntityConfigurations : IEntityTypeConfiguration<Mission>
    {
        public void Configure(EntityTypeBuilder<Mission> builder)
        {
            builder.ToTable("Missions");

            builder.Property(e => e.Title)
               .HasColumnType("LONGTEXT");

            builder.Property(e => e.MissionKind)
               .HasDefaultValue("technician_task");

            builder.Property(e => e.Duration)
               .HasMaxLength(25);

            builder.Property(e => e.Object)
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.AdditionalInfo)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<JObject>>() ?? new HashSet<JObject>())
                 .HasColumnType("LONGTEXT");

            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.Missions)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Client)
                .WithMany(e => e.Missions)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Technician)
                .WithMany(e => e.Missions)
                .HasForeignKey(e => e.TechnicianId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}