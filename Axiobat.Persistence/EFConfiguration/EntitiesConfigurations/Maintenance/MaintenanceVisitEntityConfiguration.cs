﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class MaintenanceVisitEntityConfiguration : IEntityTypeConfiguration<MaintenanceVisit>
    {
        public void Configure(EntityTypeBuilder<MaintenanceVisit> builder)
        {
            // the table name
            builder.ToTable("MaintenanceVisits");

            // properties
            builder.Ignore(e => e.DocumentType);

            // conversions
            builder.Property(e => e.EquipmentDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<MaintenanceContractEquipmentDetails>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Site)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Address>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.MaintenanceContract)
                .WithMany(e => e.MaintenanceVisits)
                .HasForeignKey(e => e.MaintenanceContractId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne<Client>()
                .WithMany(e => e.MaintenanceVisits)
                .HasForeignKey(e => e.ClientId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
