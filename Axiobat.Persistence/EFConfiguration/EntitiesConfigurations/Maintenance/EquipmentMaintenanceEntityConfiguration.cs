﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class EquipmentMaintenanceEntityConfiguration : IEntityTypeConfiguration<EquipmentMaintenance>
    {
        public void Configure(EntityTypeBuilder<EquipmentMaintenance> builder)
        {
            // the table name
            builder.ToTable("EquipmentMaintenances");

            // properties;

            // conversions
            builder.Property(e => e.MaintenanceOperations)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<EquipmentMaintenanceOperation>>())
                 .HasColumnType("LONGTEXT");

            // indexes
            builder.HasIndex(e => e.EquipmentName)
                .IsUnique();
        }
    }
}
