﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductCategoryTypeEntityConfiguration : IEntityTypeConfiguration<ProductCategoryType>
    {
        public void Configure(EntityTypeBuilder<ProductCategoryType> builder)
        {
            builder.ToTable("ProductCategoryType");

            // indexes
            builder.HasIndex(e => e.Label);
        }
    }
}
