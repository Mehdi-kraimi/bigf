﻿namespace Axiobat.Services.Products
{
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Products;
    using AutoMapper;
    using Axiobat.Application;
    using Axiobat.Application.Models;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IProductBaseService{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the product entity</typeparam>
    public partial class ProductsBaseService<TEntity>
    {

    }

    /// <summary>
    /// partial part for <see cref="ProductsBaseService{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public partial class ProductsBaseService<TEntity> : SynchronizeDataService<TEntity, string>, IProductBaseService<TEntity>
            where TEntity : ProductsBase
    {
        /// <summary>
        /// name of the entity of this service
        /// </summary>
        protected override string EntityName => typeof(TEntity).Name;

        public ProductsBaseService(
            IDataAccess<TEntity, string> dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }
    }
}
