﻿namespace Axiobat.Services.Configuration
{
    using Application.Enums;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services;
    using Application.Services.Configuration;
    using Axiobat.Domain.Enums;
    using Nager.Date;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the service implementation for <see cref="IDateService"/>
    /// </summary>
    public partial class DateService
    {
        /// <summary>
        /// get the total working hours between the given starting date and ending date, taking in consideration the starting working hours and ending hours
        /// </summary>
        /// <param name="startingDate">the starting date</param>
        /// <param name="endingTime">the ending date</param>
        /// <param name="startingHours">the starting hour</param>
        /// <param name="endingHours">the ending hour</param>
        /// <returns>the total working hours</returns>
        public double GetTotalWorkingHours(DateTime startingDate, DateTime endingTime, string startingHours, string endingHours)
        {
            var weekendProvider = DateSystem.GetWeekendProvider(CountryCode.FR);

            var weekends = GetWeekEnd(startingDate, endingTime);
            var holidays = DateSystem.GetPublicHoliday(startingDate, endingTime, CountryCode.FR).Where(x => x.Global && !weekendProvider.IsWeekend(x));

            var TotalDays = (endingTime - startingDate).TotalDays - (weekends.Count + holidays.Count());

            if (TotalDays < 0)
                return 0;

            DateTime debut = DateTime.ParseExact(startingHours, "HH:mm", System.Globalization.CultureInfo.CurrentCulture);
            DateTime fin = DateTime.ParseExact(endingHours, "HH:mm", System.Globalization.CultureInfo.CurrentCulture);

            var nbHoraireTravailheurs = (fin - debut).TotalHours;
            return nbHoraireTravailheurs * TotalDays;
        }

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        public void SetDateRange(DateRangeFilterOptions dateRangeFilter)
        {
            if (dateRangeFilter.PeriodType == FilterPeriodType.CurrentAccountingPeriod)
            {
                SetDateRange(dateRangeFilter, GetAccountingPeriod());
                return;
            }

            SetDateRange(dateRangeFilter, null);
        }

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="accountingPeriod">the accounting period</param>
        public void SetDateRange(DateRangeFilterOptions dateRangeFilter, AccountingPeriodModel accountingPeriod)
        {
            switch (dateRangeFilter.PeriodType)
            {
                // the date period type is an interval
                case FilterPeriodType.Interval: SetIntervalPeriod(dateRangeFilter); break;

                // the current month
                case FilterPeriodType.CurrentMonth: GetCurrentMonthPeriod(dateRangeFilter); break;

                // the Last month
                case FilterPeriodType.LastMonth: GetLastMonthPeriod(dateRangeFilter); break;

                // the Last three month
                case FilterPeriodType.LastThreeMonths: GetLastThreeMonthsPeriod(dateRangeFilter); break;

                // the Last six month
                case FilterPeriodType.LastSixMonths: GetLastSixMonthsPeriod(dateRangeFilter); break;

                // the current year
                case FilterPeriodType.CurrentYear: GetCurrentYearPeriod(dateRangeFilter); break;

                // the last year
                case FilterPeriodType.LastYear: GetLastYearPeriod(dateRangeFilter); break;

                // the Current Accounting Period
                case FilterPeriodType.CurrentAccountingPeriod: GetCurrentAccountingPeriod(dateRangeFilter, accountingPeriod); break;

                // no date falter should be applied
                case FilterPeriodType.None:
                    dateRangeFilter.DateEnd = null;
                    dateRangeFilter.DateStart = null;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// use this method to set the date range based on the given date filter, and than extend the period using the given extend options
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="extendOptions">the period extend options, if you select <see cref="PeriodType.Week"/> the value should be in days</param>
        /// <param name="extendDirection">the extend direction</param>
        /// <param name="value">the extend value</param>
        public void SetDateRange(DateRangeFilterOptions dateRangeFilter, PeriodType extendOptions, DateExtendDirection extendDirection, sbyte value)
        {
            SetDateRange(dateRangeFilter);
            ExtendDate(dateRangeFilter, extendOptions, extendDirection, value);
        }

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="accountingPeriod">the accounting period</param>
        /// <param name="extendOptions">the period extend options</param>
        /// <param name="extendDirection">the extend direction</param>
        /// <param name="value">the extend value</param>
        public void SetDateRange(DateRangeFilterOptions dateRangeFilter, AccountingPeriodModel accountingPeriod, PeriodType extendOptions, DateExtendDirection extendDirection, sbyte value)
        {
            SetDateRange(dateRangeFilter, accountingPeriod);
            ExtendDate(dateRangeFilter, extendOptions, extendDirection, value);
        }

        /// <summary>
        /// get the list of dates between the given start and end date, this will return the 1et of each month in every year between the given two dates
        /// </summary>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the list of the dates</returns>
        public IEnumerable<DateTime> GetDates(DateTime startDate, DateTime endDate)
        {
            // get the start month of the start date
            var start = new DateTime(startDate.Year, startDate.Month, 1);

            // create a date while we didn't reach the end date
            while (start <= endDate)
            {
                yield return start; // return the start date
                start = start.AddMonths(1); // then add a month
            }
        }
    }

    /// <summary>
    /// partial part for <see cref="DateService"/>
    /// </summary>
    public partial class DateService : IDateService
    {
        private readonly IAccountingPeriodService _accountingPeriodService;

        /// <summary>
        /// create an instant of <see cref="DateService"/>
        /// </summary>
        public DateService(IAccountingPeriodService accountingPeriodService)
        {
            _accountingPeriodService = accountingPeriodService;
        }

        /// <summary>
        /// get the accounting Period
        /// </summary>
        /// <returns>the accounting period instant</returns>
        private AccountingPeriodModel GetAccountingPeriod()
        {
            var accountingPeriod = _accountingPeriodService.GetCurrentAccountingPeriodAsync<AccountingPeriodModel>().GetAwaiter().GetResult();
            if (accountingPeriod is null)
                throw new ValidationException("there is no accounting period configure. consider adding one, for this filter to work", MessageCode.InvalidData);

            return accountingPeriod;
        }

        /// <summary>
        /// get list of weekends between the given starting date and ending date
        /// </summary>
        /// <param name="dateStart">the starting date</param>
        /// <param name="dateEnd">the ending date</param>
        /// <returns>the list of dates</returns>
        private List<DateTime> GetWeekEnd(DateTime dateStart, DateTime dateEnd)
        {
            var weekEnd = new List<DateTime>();
            var end = new DateTime(dateEnd.Year, dateEnd.Month, dateEnd.Day, 00, 00, 00);
            var start = new DateTime(dateStart.Year, dateStart.Month, dateStart.Day, 00, 00, 00);
            while (DateTime.Compare(start, end) <= 0)
            {
                if (start.DayOfWeek == DayOfWeek.Saturday || start.DayOfWeek == DayOfWeek.Sunday)
                {
                    weekEnd.Add(start);
                }
                start = start.AddDays(1);
            }
            return weekEnd;
        }

        private void ExtendDate(DateRangeFilterOptions dateRangeFilter, PeriodType extendOptions, DateExtendDirection extendDirection, sbyte value)
        {
            if (dateRangeFilter.PeriodType == FilterPeriodType.None)
                return;

            switch (extendOptions)
            {
                case PeriodType.Week:
                case PeriodType.Day:
                    switch (extendDirection)
                    {
                        case DateExtendDirection.BothSides:
                            dateRangeFilter.DateStart.Value.AddDays(-value);
                            dateRangeFilter.DateEnd.Value.AddDays(value);
                            return;
                        case DateExtendDirection.StartingDate:
                            dateRangeFilter.DateStart.Value.AddDays(-value);
                            return;
                        case DateExtendDirection.EndingDate:
                            dateRangeFilter.DateEnd.Value.AddDays(value);
                            break;
                        default:
                            return;
                    }
                    return;
                case PeriodType.Month:
                    switch (extendDirection)
                    {
                        case DateExtendDirection.BothSides:
                            dateRangeFilter.DateStart.Value.AddMonths(-value);
                            dateRangeFilter.DateEnd.Value.AddMonths(value);
                            return;
                        case DateExtendDirection.StartingDate:
                            dateRangeFilter.DateStart.Value.AddMonths(-value);
                            return;
                        case DateExtendDirection.EndingDate:
                            dateRangeFilter.DateEnd.Value.AddMonths(value);
                            break;
                        default:
                            return;
                    }
                    return;
                case PeriodType.Year:
                    switch (extendDirection)
                    {
                        case DateExtendDirection.BothSides:
                            dateRangeFilter.DateStart.Value.AddYears(-value);
                            dateRangeFilter.DateEnd.Value.AddYears(value);
                            return;
                        case DateExtendDirection.StartingDate:
                            dateRangeFilter.DateStart.Value.AddYears(-value);
                            return;
                        case DateExtendDirection.EndingDate:
                            dateRangeFilter.DateEnd.Value.AddYears(value);
                            break;
                        default:
                            return;
                    }
                    return;
                default:
                    return;
            }
        }

        #region Date Periods

        private static void SetIntervalPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            // validate the date ranges inputs
            if (!dateRangeFilter.DateStart.HasValue || !dateRangeFilter.DateStart.HasValue)
                throw new ValidationException("the interval starting date or ending date is null, you must supply a value", MessageCode.InvalidData);

            // set the date ranges
            return;
        }

        private static void GetLastYearPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var today = DateTime.Today;
            var startYear = new DateTime(today.Year - 1, 1, 1);
            var endYear = new DateTime(today.Year - 1, 12, 31);

            dateRangeFilter.DateStart = startYear;
            dateRangeFilter.DateEnd = endYear;
        }

        private static void GetCurrentYearPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var today = DateTime.Today;
            var startYear = new DateTime(today.Year, 1, 1);
            var endYear = new DateTime(today.Year, 12, 31);

            dateRangeFilter.DateStart = startYear;
            dateRangeFilter.DateEnd = endYear;
        }

        private static void GetLastSixMonthsPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var lastMonthDay = DateTime.Today.AddMonths(-1);
            var endLastMonth = lastMonthDay.AddDays(1 - lastMonthDay.Day).AddMonths(1);
            var LastSixMonth = endLastMonth.AddMonths(-6);

            dateRangeFilter.DateStart = LastSixMonth.AddDays(1 - LastSixMonth.Day);
            dateRangeFilter.DateEnd = endLastMonth;
        }

        private static void GetLastThreeMonthsPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var lastMonthDay = DateTime.Today.AddMonths(-1);
            var endLastMonth = lastMonthDay.AddDays(1 - lastMonthDay.Day).AddMonths(1);
            var LastThreeMonth = endLastMonth.AddMonths(-3);

            dateRangeFilter.DateStart = LastThreeMonth.AddDays(1 - LastThreeMonth.Day);
            dateRangeFilter.DateEnd = endLastMonth;
        }

        private static void GetLastMonthPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var lastMonthDay = DateTime.Today.AddMonths(-1);
            var start = lastMonthDay.AddDays(1 - lastMonthDay.Day);
            var end = start.AddMonths(1);

            dateRangeFilter.DateStart = start;
            dateRangeFilter.DateEnd = end;
        }

        private static void GetCurrentMonthPeriod(DateRangeFilterOptions dateRangeFilter)
        {
            var today = DateTime.Today;
            var start = today.AddDays(1 - today.Day);
            var end = start.AddMonths(1);

            dateRangeFilter.DateStart = start;
            dateRangeFilter.DateEnd = end;
        }

        private static void GetCurrentAccountingPeriod(DateRangeFilterOptions dateRangeFilter, AccountingPeriodModel accountingPeriod)
        {
            if (accountingPeriod is null)
                throw new ArgumentNullException(nameof(accountingPeriod), "the given accountingPeriod is null");

            dateRangeFilter.DateStart = accountingPeriod.StartingDate;
            dateRangeFilter.DateEnd = accountingPeriod.StartingDate.AddMonths(accountingPeriod.Period);
        }

        #endregion
    }
}
