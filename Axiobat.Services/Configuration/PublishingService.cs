﻿namespace Axiobat.Services.Configuration
{
    using App.Common;
    using Application.Services.Configuration;
    using Application.Data;
    using Application.Exceptions;
    using Application.Services.FileService;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Models;
    using AutoMapper;
    using Domain.Entities;
    using Axiobat.Application.Services.AccountManagement;

    /// <summary>
    /// service implementation for <see cref="IPublishingService"/>
    /// </summary>
    public partial class PublishingService : IPublishingService
    {
        /// <summary>
        /// extracting the tags from the given document steam
        /// </summary>
        /// <param name="stream">the file stream</param>
        /// <returns>the list of tags</returns>
        public string[] ExtractTagsFromDocument(Stream stream)
            => _documentService.GetTags(stream);

        /// <summary>
        /// save the given document (steam) using the given name
        /// </summary>
        /// <param name="stream">the document stream to save the </param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<string> SaveContractAsync(Stream stream, string fileName)
        {
            // the new file name generated
            var generatedFileName = $"{Guid.NewGuid()}{Path.GetExtension(fileName)}";

            // build path to save
            var savePath = Path.Combine(_applicationSettings.GetPublishContractFilesOutput(), generatedFileName);

            // save stream
            await _documentService.Async(stream, savePath);

            // return the generated file name
            return generatedFileName;
        }

        /// <summary>
        /// create a new publish contract recored
        /// </summary>
        /// <param name="model">the model to be used for creating a new contract</param>
        /// <returns>the operation result</returns>
        public async Task<Result<PublishingContractModel>> CreateAsync(PublishingContractCreateModel model)
        {
            var publishingContract = _mapper.Map<PublishingContract>(model);
            publishingContract.UserId = _loggedInUserService.User.UserId;

            var result = await _dataAccess.AddAsync(publishingContract);
            if (!result.IsSuccess)
                return Result.From<PublishingContractModel>(result);

            return _mapper.Map<PublishingContractModel>(result.Value);
        }

        /// <summary>
        /// update the publishing contract with the given id using the given model
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated version</returns>
        public async Task<Result<PublishingContractModel>> UpdateAsync(int contractId, PublishingContractUpdateModel model)
        {
            var publishingContract = await _dataAccess.GetByIdAsync(contractId);
            if (publishingContract is null)
                throw new NotFoundException("there is no published contract with the given id");

            var oldFileName = publishingContract.FileName;

            model.Update(publishingContract);
            var updateResult = await _dataAccess.UpdateAsync(publishingContract);
            if (!updateResult.IsSuccess)
                return Result.From<PublishingContractModel>(updateResult);

            if (updateResult.Value.FileName != oldFileName)
                RemoveDocumentPublishingContract(oldFileName);

            return _mapper.Map<PublishingContractModel>(updateResult.Value);
        }

        /// <summary>
        /// delete the publishing contract with the given id
        /// </summary>
        /// <param name="contractId">the id of the publishing contract to be deleted</param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteAsync(int contractId)
        {
            var publishingContract = await _dataAccess.GetByIdAsync(contractId);
            if (publishingContract is null)
                throw new NotFoundException("there is no published contract with the given id");

            var deleteResult = await _dataAccess.DeleteAsync(contractId);
            if (!deleteResult.IsSuccess)
                return deleteResult;

            RemoveDocumentPublishingContract(publishingContract.FileName);
            return Result.Success();
        }

        /// <summary>
        /// get the list of publishing contracts as a paged result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the paged result</returns>
        public async Task<PagedResult<PublishingContractModel>> GetAsPagedResultAsync(FilterOptions filterModel)
        {
            var result = await _dataAccess.GetPagedResultAsync(filterModel);
            var data = result.Value;
            return Result.PagedSuccess(_mapper.Map<PublishingContractModel[]>(data), result);
        }

        /// <summary>
        /// validate if the given file name is support file
        /// </summary>
        /// <param name="fileName">the name of the file</param>
        public void ValidateFileName(string fileName)
        {
            var fileExtention = Path.GetExtension(fileName);

            if (!_allowedExtentions.Contains(fileExtention))
                throw new ValidationException($"the given file is not supported, supported file types are {_allowedExtentions.ToJson()}", MessageCode.InvalidFileType);
        }

        /// <summary>
        /// download contract with the given id
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>the download details</returns>
        public async Task<PublishingContractDownloadModel> DownloadContractAsync(int contractId)
        {
            var publishingContract = await _dataAccess.GetByIdAsync(contractId);
            if (publishingContract is null)
                throw new NotFoundException("there is no published contract with the given id");

            var filePath = Path.Combine(_applicationSettings.GetPublishContractFilesOutput(), publishingContract.FileName);
            var data = await _documentService.GetFileAsync(filePath, publishingContract.Tags);

            return new PublishingContractDownloadModel
            {
                Filename = publishingContract.FileName,
                ContentType = FileHelper.GetFileMimeType(publishingContract.FileName),
                Data = data,
            };
        }


        public async Task<PublishingContractDownloadModel> DownloadContractAsync(PublishingContractMinimal publishingContract)
        {
            var filePath = Path.Combine(_applicationSettings.GetPublishContractFilesOutput(), publishingContract.FileName);
            var data = await _documentService.GetFileAsync(filePath, publishingContract.Tags);

            return new PublishingContractDownloadModel
            {
                Filename = publishingContract.FileName,
                ContentType = FileHelper.GetFileMimeType(publishingContract.FileName),
                Data = data,
            };
        }
    }

    /// <summary>
    /// partial part for <see cref="PublishingService"/>
    /// </summary>
    public partial class PublishingService
    {
        private static readonly string[] _allowedExtentions = new[] { ".doc", ".docx" };

        private readonly IMapper _mapper;
        private readonly IFileService _fileService;
        private readonly IDocumentService _documentService;
        private readonly ILoggedInUserService _loggedInUserService;
        private readonly IDataAccess<PublishingContract, int> _dataAccess;
        private readonly IApplicationSettingsAccessor _applicationSettings;

        public PublishingService(
            IFileService fileService,
            IDocumentService documentService,
            ILoggedInUserService loggedInUserService,
            IDataAccess<PublishingContract, int>  dataAccess,
            IApplicationSettingsAccessor applicationSettings,
            IMapper mapper)
        {
            _dataAccess = dataAccess;
            _fileService = fileService;
            _documentService = documentService;
            _loggedInUserService = loggedInUserService;
            _applicationSettings = applicationSettings;
            _mapper = mapper;
        }

        private void RemoveDocumentPublishingContract(string fileName)
        {
            var filePath = Path.Combine(_applicationSettings.GetPublishContractFilesOutput(), fileName);
            _documentService.RemoveFile(filePath);
        }
    }
}
