﻿namespace Axiobat.Services.Configuration
{
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Application.Exceptions;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// service implementation for <see cref="ICountryService"/>
    /// </summary>
    public partial class CountryService : ICountryService
    {
        /// <summary>
        /// get paged result of the countries
        /// </summary>
        /// <param name="filter">the filter option</param>
        /// <returns>list of countries</returns>
        public virtual async Task<PagedResult<TOut>> GetAsPagedResultAsync<TOut, IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions
        {
            var result = await _dataAccess.GetPagedResultAsync(filterOption);

            if (!result.IsSuccess)
                return Result.ListFrom<TOut, Country>(result);

            return Result.PagedSuccess(_mapper.Map<List<TOut>>(result.Value), result);
        }

        /// <summary>
        /// get the country by code
        /// </summary>
        /// <param name="countryCode">the code of the country</param>
        /// <returns>the country</returns>
        public async Task<Result<Country>> GetByCodeAsync(string countryCode)
        {
            var country = await _dataAccess.GetCountryByCodeAsync(countryCode);
            if (country is null)
                throw new NotFoundException(T("there is no country with the given code"));

            return country;
        }

        /// <summary>
        /// the name of the country
        /// </summary>
        /// <param name="countryName">the name of the country</param>
        /// <returns>the country</returns>
        public async Task<Result<Country>> GetByNameAsync(string countryName)
        {
            var country = await _dataAccess.GetCountryByNameAsync(countryName);
            if (country is null)
                throw new NotFoundException(T("there is no country with the given code"));

            return country;
        }
    }

    /// <summary>
    /// partial part for <see cref="CountryService"/>
    /// </summary>
    public partial class CountryService : BaseService
    {
        private readonly ICountryDataAccess _dataAccess;

        /// <summary>
        /// the name of the entity
        /// </summary>
        protected override string EntityName => typeof(Country).Name;

        /// <summary>
        /// create an instant of <see cref="CountryService"/>
        /// </summary>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="configurationService">the <see cref="IApplicationConfigurationService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        /// <param name="loggerFactory"><see cref="ILoggerFactory"/> instant</param>
        /// <param name="mapper">the <see cref="IMapper"/> instant</param>
        public CountryService(
            ICountryDataAccess countryDataAccess,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService configurationService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper) : base(loggedInUserService, configurationService, translationService, loggerFactory, mapper)
        {
            _dataAccess = countryDataAccess;
        }
    }
}
