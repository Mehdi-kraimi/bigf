﻿namespace Axiobat.Services
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Enums;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Axiobat.Domain.Constants;
    using System;
    using Axiobat.Application;
    using Axiobat.Application.Services.Contacts;

    /// <summary>
    /// the service implementation for <see cref="IConstructionWorkshopService"/>
    /// </summary>
    public partial class ConstructionWorkshopService
    {
        /// <summary>
        /// check if the given Construction Workshop name unique or not
        /// </summary>
        /// <param name="name">the name of Construction Workshop</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsNameUniqueAsync(string name)
            => !await _dataAccess.IsExistAsync(e => e.Name == name);

        /// <summary>
        /// update the status of the workshop using the given model
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop to be updated</param>
        /// <param name="model">the status update model</param>
        /// <returns>the operation result</returns>
        public async Task<Result<ConstructionWorkshopModel>> UpdateStatusAsync(string constructionWorkshopId, WorkshopUpdateStatusModel model)
        {
            // first get the workshop
            var workshop = await GetWorkshopAsync(constructionWorkshopId);

            // validate if workshop details
            // ToDo : add validation

            // set the new status
            workshop.Status = model.Status;

            // update the workshop
            var updateResult = await _dataAccess.UpdateAsync(workshop);
            if (!updateResult.IsSuccess)
                return Result.From<ConstructionWorkshopModel>(updateResult);

            // return the updated version
            return Map<ConstructionWorkshopModel>(workshop);
        }

        /// <summary>
        /// update Progress Rate of the given workshop
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated version of the workshop</returns>
        public async Task<Result<ConstructionWorkshopModel>> UpdateProgressRateAsync(string constructionWorkshopId, WorkshopUpdateProgressRateModel model)
        {
            // first get the workshop
            var workshop = await GetWorkshopAsync(constructionWorkshopId);

            //update the value
            workshop.ProgressRate = model.ProgressRate;
            var updateResult = await _dataAccess.UpdateAsync(workshop);
            if (!updateResult.IsSuccess)
                return Result.From<ConstructionWorkshopModel>(updateResult);

            // return the updated version
            return Map<ConstructionWorkshopModel>(workshop);
        }

        /// <summary>
        /// retrieve the workshop documents details
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns></returns>
        public async Task<PagedResult<WorkshopDocumentsDetails>> GetDocumentsDetailsAsync(string workshopId, RubricFilterOptions filterOptions)
        {
            filterOptions.WorkshopId = workshopId;

            var data = await _rubricDataAccess.GetPagedResultAsync(filterOptions);

            (int totalQuotes, int totalInvoices, int totalOrders) = await GetTotalDocumentsAsync(workshopId, data);

            var result = data.Value
                .Select(rubric =>
                {
                    var associatedDocumentsCount = 0;

                    if (rubric.Type == DocumentRubricType.Quote)
                        associatedDocumentsCount = totalQuotes;

                    if (rubric.Type == DocumentRubricType.Billing)
                        associatedDocumentsCount = totalInvoices;

                    if (rubric.Type == DocumentRubricType.Orders)
                        associatedDocumentsCount = totalOrders;

                    return new WorkshopDocumentsDetails
                    {
                        Id = rubric.Id,
                        Type = rubric.Type,
                        Value = rubric.Value,
                        WorkshopId = rubric.WorkshopId,
                        AssociatedDocumentsCount = associatedDocumentsCount,
                        TotalDocuments = rubric.Documents.Count,
                        LastModifiedOn = rubric.Documents
                        .OrderByDescending(e => e.LastModifiedOn)
                        .FirstOrDefault()?.LastModifiedOn
                            ?? rubric.LastModifiedOn,
                    };
                });

            return Result.PagedSuccess(result, data);
        }

        private async Task<(int totalQuotes, int totalInvoices, int totalOrders)> GetTotalDocumentsAsync(string workshopId, PagedResult<WorkshopDocumentRubric> data)
        {
            int totalQuotes = 0, totalInvoices = 0, totalOrders = 0;

            if (data.Value.Any(e => e.Type == DocumentRubricType.Quote))
                totalQuotes = await _dataAccess.GetQuotesDocumentsCountAsync(workshopId);

            if (data.Value.Any(e => e.Type == DocumentRubricType.Billing))
                totalInvoices = await _dataAccess.GetInvoiceDocumentsCountAsync(workshopId);

            if (data.Value.Any(e => e.Type == DocumentRubricType.Orders))
                totalOrders = await _dataAccess.GetSupplierOrderDocumentsCountAsync(workshopId);

            return (totalQuotes, totalInvoices, totalOrders);
        }

        /// <summary>
        /// add a new document to the workshop documents
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <param name="model">the model used to create the document</param>
        /// <returns>the create version of the document</returns>
        public async Task<Result<WorkshopDocumentModel>> CreateDocumentAsync(string workshopId, WorkshopDocumentPutModel model)
        {
            // first retrieve the workshop
            var workshop = await GetWorkshopAsync(workshopId);

            // get the workshop document entity
            var document = Map<WorkshopDocument>(model);
            document.WorkshopId = workshop.Id;
            document.User = _loggedInUserService.GetMinimalUser();
            document.Types = model.Types.Select(e =>
            {
                var type = new WorkshopDocumentsTypes
                {
                    DocumentId = document.Id,
                    TypeId = e.Id,
                };

                if (!e.Id.IsValid())
                {
                    type.TypeId = null;
                    type.Type = new WorkshopDocumentType
                    {
                        Value = e.Value,
                    };
                }

                return type;
            }).ToList();

            // save document attachment
            document.Attachments = await _fileService.SaveAsync(model.Attachments.ToArray());

            // save the document
            var addResult = await _documentDataAccess.AddAsync(document);
            if (!addResult.IsSuccess)
            {
                _logger.LogError("Failed to add the document to the workshop with id: [{workshopId}]", workshopId);
                return Result.From<WorkshopDocumentModel>(addResult);
            }

            // update the status of the workshop it required
            var documentType = await _configuration.GetSpecialDocumentTypeAsync();
            if (!(documentType is null) && document.Types.Select(e => e.TypeId).Contains(documentType.Id) && workshop.Status == documentType.AffectedStatus)
            {
                workshop.Status = documentType.NewStatus;
                await _dataAccess.UpdateAsync(workshop);
            }

            return Map<WorkshopDocumentModel>(document);
        }

        public async Task<Result<WorkshopDocumentModel>> UpdateDocumentAsync(string workshopId, string documentId, WorkshopDocumentPutModel model)
        {
            // first retrieve the workshop
            var workshop = await GetWorkshopAsync(workshopId);

            // get the document
            var document = await _documentDataAccess.GetByIdAsync(documentId);
            if (document is null)
                throw new NotFoundException("Failed to retrieve the document with the given id");

            // update the document
            model.Update(document);

            // add the new attachment
            var newAttachementList = await _fileService.SaveAsync(model.Attachments.ToArray());
            foreach (var attachement in newAttachementList)
                document.Attachments.Add(attachement);

            // update types
            var newtypes = model.Types.Select(e =>
            {
                var type = new WorkshopDocumentsTypes
                {
                    DocumentId = document.Id,
                    TypeId = e.Id,
                };

                if (!e.Id.IsValid())
                {
                    type.TypeId = null;
                    type.Type = new WorkshopDocumentType
                    {
                        Value = e.Value,
                    };
                }

                return type;
            }).ToList();

            // delete and insert the types
            await _documentDataAccess.DeleteDocumentTypesAsync(document.Id);
            await _documentDataAccess.AddDocumentsTypesAsync(newtypes.ToArray());

            // update the document
            var updateResult = await _documentDataAccess.UpdateAsync(document);

            // map and return
            return Map<WorkshopDocumentModel>(document);
        }

        public async Task<Result> DeleteDocumentAsync(string constructionWorkshopId, string documentId)
        {
            // get the document
            var document = await _documentDataAccess.GetByIdAsync(documentId);
            if (document is null)
                throw new NotFoundException("Failed to retrieve the document with the given id");

            // delete the attachments
            await _fileService.DeleteAsync(document.Attachments.ToArray());

            return await _documentDataAccess.DeleteAsync(document);
        }

        public async Task<Result<WorkshopDocumentModel>> GetDocumentByIdAsync(string workshopId, string documentId)
        {
            // get the document
            var document = await _documentDataAccess.GetByIdAsync(documentId);
            if (document is null)
                throw new NotFoundException("Failed to retrieve the document with the given id");

            // map and return
            return Map<WorkshopDocumentModel>(document);
        }

        public async Task<ListResult<WorkshopDocumentModel>> GetDocumentsAsync(string workshopId)
        {
            var documents = await _documentDataAccess.GetWorkshopDocumentsAsync(workshopId);
            return Map<List<WorkshopDocumentModel>>(documents);
        }

        /// <summary>
        /// retrieve list of documents of the given
        /// </summary>
        /// <param name="workshopId">the id the workshop</param>
        /// <param name="rubricId">the id the rubric</param>
        /// <returns>the list of documents</returns>
        public async Task<ListResult<WorkshopDocumentModel>> GetRubricDocumentsAsync(string workshopId, string rubricId)
        {
            var result = await _documentDataAccess.GetRubricDocumentsAsync(workshopId, rubricId);
            return Result.ListSuccess(Map<IEnumerable<WorkshopDocumentModel>>(result));
        }

        /// <summary>
        /// get the workshop client
        /// </summary>
        /// <typeparam name="TOut">the type of the output, should be registered with automapper</typeparam>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the client with desired output</returns>
        public async Task<Result<TOut>> GetWorkshopClientAsync<TOut>(string workshopId)
        {
            var client = await _dataAccess.GetWorkshopClientAsync(workshopId);
            if (client is null)
                return Result.Failed<TOut>("Failed to retrieve the client, not exist", MessageCode.NotFound);

            return Map<TOut>(client);
        }

        /// <summary>
        /// retrieve list of documents using the filter option as a paged result
        /// </summary>
        /// <param name="workshopId">the id the workshop</param>
        /// <param name="filterOptions">the Workshop Document FilterOptions</param>
        /// <returns>the paged of documents</returns>
        public async Task<PagedResult<WorkshopDocumentModel>> GetRubricDocumentsAsync(WorkshopDocumentFilterOptions filterOptions)
        {
            var listResult = await _documentDataAccess.GetPagedResultAsync(filterOptions);
            var data = Map<IEnumerable<WorkshopDocumentModel>>(listResult.Value);

            return Result.PagedSuccess(data, listResult);
        }

        /// <summary>
        /// retrieve the workshop documents counts informations
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <returns>the workshop documents count</returns>
        public async Task<Result<WorkshopDocumentsCount>> GetWorkshopDocumentsCountAsync(string constructionWorkshopId)
            => await _dataAccess.GetWorkshopDocumentsCountAsync(constructionWorkshopId);

        public async Task<ListResult<WorkshopDocumentRubricModel>> GetWorkshopDocumentsRubricsAsync(string constructionWorkshopId)
        {
            var data = await _rubricDataAccess.GetAllAsync(options =>
            {
                options.AddInclude(e => e.Documents);
                options.AddPredicate(e => e.WorkshopId == constructionWorkshopId);
            });

            return _mapper.Map<WorkshopDocumentRubricModel[]>(data);
        }

        public async Task<Result<WorkshopDocumentRubricModel>> PutWorkshopDocumentsRubricsAsync(string constructionWorkshopId, WorkshopDocumentRubricModel model)
        {
            if (model.Id.IsValid()) // update
            {
                var entity = await _rubricDataAccess.GetByIdAsync(model.Id);
                if (entity is null)
                    throw new NotFoundException("there is no rubric with the given id");

                model.Update(entity);

                var updateResult = await _rubricDataAccess.UpdateAsync(entity);
                if (!updateResult.IsSuccess)
                    return Result.From<WorkshopDocumentRubricModel>(updateResult);

                return _mapper.Map<WorkshopDocumentRubricModel>(entity);
            }
            else
            {
                var entity = _mapper.Map<WorkshopDocumentRubric>(model);
                entity.Id = Guid.NewGuid().ToString();

                var addResult = await _rubricDataAccess.AddAsync(entity);
                if (!addResult.IsSuccess)
                    return Result.From<WorkshopDocumentRubricModel>(addResult);

                return _mapper.Map<WorkshopDocumentRubricModel>(entity);
            }
        }

        public async Task<Result> DeleteWorkshopDocumentsRubricsAsync(string constructionWorkshopId, string rubricId)
        {
            var entity = await _rubricDataAccess.GetByIdAsync(rubricId);

            if (entity.WorkshopId != constructionWorkshopId)
                return Result.Failed("the given rubric don't belong to the given workshop");

            if (entity.Type != DocumentRubricType.Custom)
                return Result.Failed("you don't have access to delete a system rubric");

            return await _rubricDataAccess.DeleteAsync(entity);
        }

        public async Task<ListResult<WorkshopDocumentModel>> GetWorkshopRubricsDocumentsAsync(string constructionWorkshopId, string rubricId)
        {
            var data = await _documentDataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(e => e.WorkshopId == constructionWorkshopId && e.RubricId == rubricId);
            });

            return _mapper.Map<WorkshopDocumentModel[]>(data);
        }
    }

    /// <summary>
    /// partial part for <see cref="ConstructionWorkshopService"/>
    /// </summary>
    public partial class ConstructionWorkshopService : SynchronizeDataService<ConstructionWorkshop, string>, IConstructionWorkshopService
    {
        private readonly IClientService _clientService;
        private readonly IWorkshopDocumentDataAccess _documentDataAccess;
        private readonly IWorkshopDocumentRubricDataAccess _rubricDataAccess;

        private new IConstructionWorkshopDataAccess _dataAccess => base._dataAccess as IConstructionWorkshopDataAccess;

        public ConstructionWorkshopService(
            IClientService clientService,
            IConstructionWorkshopDataAccess dataAccess,
            IWorkshopDocumentDataAccess documentDataAccess,
            IWorkshopDocumentRubricDataAccess rubricDataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _clientService = clientService;
            _documentDataAccess = documentDataAccess;
            this._rubricDataAccess = rubricDataAccess;
        }

        /// <summary>
        /// before inserting the workshop
        /// </summary>
        /// <typeparam name="TCreateModel">the type of the create model</typeparam>
        /// <param name="workshop">the workshop entity instant</param>
        /// <param name="createModel">the create model instant</param>
        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(ConstructionWorkshop workshop, TCreateModel createModel)
        {
            workshop.Status = WorkshopStatus.Study;

            if (createModel is ConstructionWorkshopPutModel model)
            {
                var client = await _clientService.GetByIdAsync<ClientDocument>(model.ClientId);
                if (!client.IsSuccess)
                    throw new ValidationException(client.Message, client.MessageCode);

                workshop.Client = client;
                workshop.Rubrics = WorkshopDocumentRubric.Default;

                if (model.Rubrics.Any())
                {
                    workshop.Rubrics = workshop.Rubrics
                        .Concat(model.Rubrics
                        .Select(value => new WorkshopDocumentRubric
                        {
                            Value = value,
                            WorkshopId = workshop.Id,
                            Id = Guid.NewGuid().ToString(),
                            Type = DocumentRubricType.Custom,
                        }))
                        .ToList();
                }
            }
        }

        protected override async Task InUpdate_AfterUpdateAsync<TUpdateModel>(ConstructionWorkshop workshop, TUpdateModel updateModel)
        {
            if (updateModel is ConstructionWorkshopPutModel model)
            {
                if (model.Rubrics.Any())
                {
                    var workshopRubrics = await _rubricDataAccess.GetAllAsync(options =>
                    {
                        options.AddPredicate(e => e.WorkshopId == workshop.Id);
                    },
                    rubric => rubric.Value);

                    var rubricsToAdd = model.Rubrics
                        .Except(workshopRubrics)
                        .Select(value => new WorkshopDocumentRubric
                        {
                            Value = value,
                            WorkshopId = workshop.Id,
                            Id = Guid.NewGuid().ToString(),
                            Type = DocumentRubricType.Custom,
                        })
                        .ToList();

                    await _rubricDataAccess.AddRangeAsync(rubricsToAdd);
                }
            }
        }

        protected override async Task InUpdate_ValidateEntityAsync(ConstructionWorkshop workshop, IUpdateModel<ConstructionWorkshop> updateModel)
        {
            if (updateModel is ConstructionWorkshopPutModel model)
            {
                if (model.ClientId.IsValid() && model.ClientId != workshop.ClientId)
                {
                    var client = await _clientService.GetByIdAsync<ClientDocument>(model.ClientId);
                    if (!client.IsSuccess)
                        throw new ValidationException(client.Message, client.MessageCode);

                    workshop.Client = client;
                }
            }
        }

        /// <summary>
        /// set the workshop deletion validation logic
        /// </summary>
        /// <param name="workshop">the workshop entity</param>
        protected override async Task InDelete_BeforDeleteAsync(ConstructionWorkshop workshop)
        {
            // validate that the work shop has no documents associated with
            await _validate.WorkshopHasNoDocumentAsync(workshop);
        }

        /// <summary>
        /// get the workshop with the given id
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <returns>the workshop instant</returns>
        private async Task<ConstructionWorkshop> GetWorkshopAsync(string constructionWorkshopId)
        {
            // first get the workshop
            var workshop = await _dataAccess.GetByIdAsync(constructionWorkshopId);
            if (workshop is null)
                throw new NotFoundException("there is no workshop with the given id");

            return workshop;
        }
    }
}
