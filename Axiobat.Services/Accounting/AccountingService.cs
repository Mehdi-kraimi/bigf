﻿namespace Axiobat.Services.Accounting
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Models;
    using Application.Services;
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the accounting service
    /// </summary>
    public partial class AccountingService
    {
        /// <summary>
        /// get the journal details
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of journal details</returns>
        public Task<PagedResult<JournalEntry>> GetJournalAsync(JournalFilterOptions filterOptions)
        {
            switch (filterOptions.JournalType)
            {
                case JournalType.Sales:
                    return GetSalesJournalAsync(filterOptions);
                case JournalType.Expense:
                    return GetExpenseJournalAsync(filterOptions);
                case JournalType.Bank:
                case JournalType.Cash:
                    return GetCashJournalAsync(filterOptions);
                default:
                    return Task.FromResult(Result.PagedFailed<JournalEntry>("the given journal type is not supported", MessageCode.NotSupportedOperation));
            }
        }

        /// <summary>
        /// export the journal using the given options
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the exported file as a byte array</returns>
        public async Task<Result<byte[]>> ExportJournalAsync(JournalFilterOptions filterOptions)
        {
            var journalData = await GetJournalAsync(filterOptions);
            return _fileService.ExportJournal(journalData.Value, filterOptions.JournalType);
        }
    }

    /// <summary>
    /// partial part for <see cref="AccountingService"/>
    /// </summary>
    public partial class AccountingService : BaseService, IAccountingService
    {
        private readonly IChartOfAccountsService _chartOfAccountsService;
        private readonly IAccountingDataAccess _dataAccess;
        private readonly IFileService _fileService;
        private readonly IDateService _dateService;

        protected override string EntityName => "Accounting";

        public AccountingService(
            IChartOfAccountsService chartOfAccountsService,
            IAccountingDataAccess DataAccess,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService configurationService,
            IFileService fileService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IDateService dateService,
            IMapper mapper)
            : base(loggedInUserService, configurationService, translationService, loggerFactory, mapper)
        {
            _chartOfAccountsService = chartOfAccountsService;
            _dataAccess = DataAccess;
            _fileService = fileService;
            _dateService = dateService;
        }

        private async Task<PagedResult<JournalEntry>> GetCashJournalAsync(JournalFilterOptions filterOptions)
        {
            // get the plan charts
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();
            var internalTransfers = charts[ChartOfAccounts.General, ChartAccountItem.InternalTransfers];
            var defaultBankCode = charts[ChartOfAccounts.General, ChartAccountItem.Bank].Code;
            var defaultCashCode = charts[ChartOfAccounts.General, ChartAccountItem.Cash].Code;

            // first set the dates ranges
            _dateService.SetDateRange(filterOptions);

            var AccountType = filterOptions.JournalType == JournalType.Cash
                ? FinancialAccountsType.Cash
                : FinancialAccountsType.Bank;

            // get the payments
            var paymentsResult = await _dataAccess.GetPaymentsAsync(filterOptions, AccountType);

            // format data
            var result = new List<JournalEntry>();
            var accountingCode = filterOptions.JournalType == JournalType.Cash ? T("CAISSE") : T("BANQUE");

            foreach (var payment in paymentsResult.Value)
            {
                // add the transfer payment operation
                if (payment.Type == PaymentType.Transfer)
                {
                    result.Add(new JournalEntry
                    {
                        Amount = (float) payment.TransferAmount,
                        JournalCode = accountingCode,
                        EntryDate = payment.DatePayment,
                        PieceNumber = payment.Account.Label,
                        EntryType = JournalEntryType.Main,
                        ChartAccount = payment.Account.Code,
                        Credits = new List<JournalEntry>
                        {
                            new JournalEntry
                            {
                                Amount =(float) payment.TransferAmount,
                                JournalCode = accountingCode,
                                EntryDate = payment.DatePayment,
                                PieceNumber = payment.Account.Label,
                                EntryType = JournalEntryType.Payment,
                                ChartAccount = internalTransfers.Code,
                            }
                        },
                    });

                    continue;
                }

                // add the invoice payments
                if (payment.Invoices.Count > 0)
                {
                    result.AddRange(payment.Invoices.Select(invoicePayment => new CashJournalModel
                    {
                        JournalCode = accountingCode,
                        Tiers = payment.Account.Label,
                        Amount = (float) invoicePayment.Amount,
                        EntryDate = payment.DatePayment,
                        EntryType = JournalEntryType.Main,
                        PaymentType = payment.Account.Type,
                        PieceNumber = invoicePayment.Invoice.Reference,
                        ExternalPartner = invoicePayment.Invoice.Client.FullName,

                        ChartAccount = payment.Account.Code.IsValid()
                            ? payment.Account.Code
                            : (payment.Account.Type == FinancialAccountsType.Bank
                                ? defaultBankCode
                                : defaultCashCode),

                        Credits = new List<JournalEntry>
                        {
                            new CashJournalModel
                            {
                                JournalCode = accountingCode,
                                EntryDate = payment.DatePayment,
                                Amount = (float)invoicePayment.Amount,
                                ExternalPartner = invoicePayment.Invoice.Client.FullName,
                                PieceNumber = invoicePayment.Invoice.Reference,
                                EntryType = JournalEntryType.Payment,
                                ChartAccount = payment.Account.Code,
                                PaymentType = payment.Account.Type,
                                Tiers = payment.Account.Label,
                            }
                        }
                    }));

                    continue;
                }

                // add the expense payments
                if (payment.Expenses.Count > 0)
                {
                    result.AddRange(payment.Expenses.Select(expensesPayments => new CashJournalModel
                    {
                        JournalCode = accountingCode,
                        Tiers = payment.Account.Label,
                        EntryDate = payment.DatePayment,
                        Amount = (float)expensesPayments.Amount,
                        EntryType = JournalEntryType.Main,
                        PaymentType = payment.Account.Type,
                        ExternalPartner = expensesPayments.Expense.Supplier.FullName,
                        PieceNumber = expensesPayments.Expense.Reference,

                        ChartAccount = payment.Account.Code.IsValid()
                            ? payment.Account.Code
                            : (payment.Account.Type == FinancialAccountsType.Bank
                                ? defaultBankCode
                                : defaultCashCode),

                        Credits = new List<JournalEntry>
                        {
                            new CashJournalModel
                            {
                                JournalCode = accountingCode,
                                EntryDate = payment.DatePayment,
                                Amount =(float) expensesPayments.Amount,
                                ExternalPartner = expensesPayments.Expense.Supplier.FullName,
                                PieceNumber = expensesPayments.Expense.Reference,
                                EntryType = JournalEntryType.Payment,
                                ChartAccount = payment.Account.Code,
                                PaymentType = payment.Account.Type,
                                Tiers = payment.Account.Label,
                            }
                        }
                    }));
                }
            }

            // return result
            return Result.PagedSuccess(result, paymentsResult);
        }

        private async Task<PagedResult<JournalEntry>> GetExpenseJournalAsync(JournalFilterOptions filterOptions)
        {
            // get the plan charts
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();

            var deductibleVAT = charts.GetTaxDeductibleCode();
            var expenseCharts = charts[ChartOfAccounts.Expenses];
            var defaultSupplierCode = charts[ChartOfAccounts.General, ChartAccountItem.Suppliers];

            // first set the dates ranges
            _dateService.SetDateRange(filterOptions);

            // get expenses
            var expenseResult = await _dataAccess.GetExpensesAsync(filterOptions);

            // format data
            var result = new List<JournalEntry>();
            var journalCode = T("ACHAT");

            foreach (var expense in expenseResult.Value)
            {
                var documentSupplierCode = expense.Supplier.AccountingCode.IsValid()
                    ? expense.Supplier.AccountingCode
                    : defaultSupplierCode.Code;

                // create the first journal entry
                var journalEntry = new JournalEntry
                {
                    JournalCode = journalCode,
                    PieceNumber = expense.Reference,
                    EntryDate = expense.CreationDate,
                    EntryType = JournalEntryType.Main,
                    ChartAccount = documentSupplierCode,
                    Amount = expense.OrderDetails.TotalTTC,
                    ExternalPartner = expense.Supplier.FullName,
                };

                // add taxes
                journalEntry.Credits.AddRange(expense.OrderDetails.GetTaxDetails()
                   .Select(e => new JournalEntry
                   {
                       Amount = e.TotalTax,
                       JournalCode = journalCode,
                       ChartAccount = deductibleVAT,
                       PieceNumber = expense.Reference,
                       EntryDate = expense.CreationDate,
                       EntryType = JournalEntryType.TaxDetails,
                       ExternalPartner = expense.Supplier.FullName,
                   }));

                // add products
                journalEntry.Credits.AddRange(expense.OrderDetails.GetAllProduct().Where(e => !(e.Product.Category is null))
                    .GroupBy(e => e.Product.Category).Select(e => new
                    {
                        e.Key.ChartAccountItem,
                        // ChartAccountItem = expenseCharts[e.Key.ChartAccountItemId],
                        Total = e.Sum(p => p.Quantity * p.Product.ProductSuppliers
                            .FirstOrDefault(s => s.SupplierId == expense.SupplierId).Price)
                    })
                    .Where(e => !(e is null) && (e.ChartAccountItem != null))
                    .Select(e => new JournalEntry
                    {
                        JournalCode = journalCode,
                        Amount = e.Total.Round(),
                        PieceNumber = expense.Reference,
                        EntryDate = expense.CreationDate,
                        ChartAccount = e.ChartAccountItem.Code,
                        ExternalPartner = expense.Supplier.FullName,
                        EntryType = JournalEntryType.ProductCategory,
                    }));

                result.Add(journalEntry);
            }

            // return result
            return Result.PagedSuccess(result, expenseResult);
        }


    }
}
