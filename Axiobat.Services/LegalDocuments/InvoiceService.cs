﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.MailService;
    using AutoMapper;
    using Axiobat.Application.Services;
    using Axiobat.Application.Services.Maintenance;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Invoice"/>
    /// </summary>
    public partial class InvoiceService : DocumentService<Invoice>, IInvoiceService
    {
        /// <summary>
        /// send the quote with the given id with an email using the given options
        /// </summary>
        /// <param name="invoiceId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendAsync(string invoiceId, SendEmailOptions emailOptions)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(invoiceId);
            if (document is null)
                throw new NotFoundException("there is no invoice with the given id");

            // generate the document Pdf
            //var filePDF = await ExportDataAsync(invoiceId, new DataExportOptions { ExportType = ExportType.PDF });
            //if (!filePDF.IsSuccess)
            //    return filePDF;

            // generate an id to the email
            emailOptions.Id = $"{invoiceId}-".AppendTimeStamp();

            // add the PDF file to the email attachments
            //emailOptions.Attachments.Add(new AttachmentModel
            //{
            //    FileType = "application/pdf",
            //    Content = Convert.ToBase64String(filePDF),
            //    FileName = $"{DocumentType.Invoice}-{DateHelper.Timestamp}.pdf",
            //});

            // send the email
            var result = await _emailService.SendEmailAsync(emailOptions);
            if (!result.IsSuccess)
                return result;

            // add the email document
            document.Emails.Add(new DocumentEmail
            {
                To = emailOptions.To,
                SentOn = DateTime.Now,
                MailId = emailOptions.Id,
                Content = emailOptions.Body,
                Subject = emailOptions.Subject,
                User = _loggedInUserService.GetMinimalUser(),
            });

            // update the document
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await base._dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// create a new invoice form the given quote
        /// </summary>
        /// <param name="document">the quote document instant</param>
        /// <returns>the generated invoice document</returns>
        public async Task<Result<InvoiceModel>> GenerateFromQuoteAsync(Quote document)
        {
            // generate a reference for the invoice
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.Invoice);

            // create invoice by mapping it form the quote
            var invoice = Map<Invoice>(document);
            invoice.Id = Generator.GenerateRandomId();
            invoice.Status = InvoiceStatus.InProgress;
            invoice.Reference = reference;

            // other properties
            var addResult = await _dataAccess.AddAsync(invoice);
            if (!addResult.IsSuccess)
                return Result.From<InvoiceModel>(addResult);

            // increment reference
            await IncrementRefenereceAsync();

            // return the result
            return Map<InvoiceModel>(addResult.Value);
        }

        /// <summary>
        /// cancel the invoice with the given id using the given credit note
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="creditNote">the credit note to be used for the cancellation</param>
        /// <returns>the operation result</returns>
        public async Task<Result<CreditNoteModel>> CancelInvoiceAsync(string invoiceId)
        {
            // retrieve the invoice
            var invoice = await _dataAccess.GetByIdAsync(invoiceId);
            if (invoice is null)
                throw new NotFoundException("there is no invoice with the given id");

            await _validate.CanCancelInvoiceAsync(invoice);

            // create credit note for the invoice
            var creditNoteResult = await _creditNoteService.GenerateForInvoiceCancelationAsync(invoice);
            if (!creditNoteResult.IsSuccess)
                return creditNoteResult;

            _history.Recored(
                invoice,
                ChangesHistoryType.Updated,
                fields: new[]
                {
                    new ChangedField
                    {
                        Champ = nameof(Invoice.Status),
                        PropName = nameof(Invoice.Status),
                        ValeurFinal = InvoiceStatus.Canceled,
                        ValeurInitial = invoice.Status,
                    }
                });

            invoice.Status = InvoiceStatus.Canceled;
            var quoteId = invoice.QuoteId;
            //invoice.QuoteId = null;

            var updateResult = await _dataAccess.UpdateAsync(invoice);
            if (!updateResult.IsSuccess)
                _logger.LogWarning("failed to update the invoice [{invoiceId}] status to [{newStatus}]", invoice.Id, InvoiceStatus.Canceled);

            // update quote data
            //   await _quoteService.QuoteInvoiceDeletedAsync(quoteId, invoice.Id);
              await _quoteService.QuoteInvoiceChangeStatutCancelAsync(quoteId, invoice.Id);





            // return result
            return creditNoteResult;
        }

        /// <summary>
        /// get the Holdback Details
        /// </summary>
        /// <param name="workshopId">the id of the workshop id</param>
        /// <returns>the list of workshop details</returns>
        public async Task<ListResult<HoldbackDetailsModel>> GetHoldbackDetailsAsync(string workshopId)
        {
            var result = await _dataAccess.GetHoldbackDetailsModelAsync(workshopId);
            var holdbacks = result.Where(e => !(e.OrderDetails.HoldbackDetails is null))
                .Select(e => new HoldbackDetailsModel
                {
                    InvoiceId = e.Id,
                    InvoiceReference = e.Reference,
                    Status = e.OrderDetails.HoldbackDetails.Status,
                    Holdback = e.OrderDetails.HoldbackDetails.Holdback,
                    WarrantyPeriod = e.OrderDetails.HoldbackDetails.WarrantyPeriod,
                    WarrantyExpirationDate = e.OrderDetails.HoldbackDetails.WarrantyExpirationDate,
                    Amount = e.GetTotalHT() * (e.OrderDetails.HoldbackDetails.Holdback / 100)
                });

            return Result.ListSuccess(holdbacks);
        }

        /// <summary>
        /// update the holdback details of the invoice
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Result> UpdateHoldbackDetailsAsync(string invoiceId, HoldbackDetailsUpdateModel model)
        {
            var invoice = await _dataAccess.GetByIdAsync(invoiceId);
            if (invoice is null)
                return Result.Failed("there is no invoice with the given id");

            if (invoice.OrderDetails.HoldbackDetails is null)
                return Result.Failed("the given invoice doesn't have a Holdback");

            // update the invoice
            invoice.OrderDetails.HoldbackDetails.Status = model.Status;
            invoice.OrderDetails.HoldbackDetails.WarrantyExpirationDate = model.WarrantyExpirationDate;

            var updateResult = await _dataAccess.UpdateAsync(invoice);
            if (!updateResult.IsSuccess)
                return Result.From(updateResult);

            return Result.Success();
        }

        public async Task<Result<ExportByte>> UpdateExportReleve(ExportByPeriodByte model)
        {
            List<byte[]> bytes = new List<byte[]>();
            List<byte[]> filesDetail = new List<byte[]>();

            var avoirs = await _creditNoteService.GetAsPagedResultAsync<CreditNote, DocumentFilterOptions>(new DocumentFilterOptions()
            {
                PeriodType = (model.endDate == null && model.startDate == null) ? Application.Enums.FilterPeriodType.None : Application.Enums.FilterPeriodType.Interval,
                DateEnd = model.endDate,
                DateStart = model.startDate,
                Status = new string[]
                    {
                    CreditNoteStatus.InProgress
                    },
                ExternalPartnerId = model.idClient,
                IgnorePagination = true,

            });

            var factures = await _dataAccess.GetPagedResultAsync<DocumentFilterOptions>(new DocumentFilterOptions()
            {
                PeriodType = (model.endDate == null && model.startDate == null) ? Application.Enums.FilterPeriodType.None : Application.Enums.FilterPeriodType.Interval,
                DateEnd = model.endDate,
                DateStart = model.startDate,
                Status = model.type == TypeExportInvoice.releve ? new string[] {
                    InvoiceStatus.InProgress,
                    InvoiceStatus.Late,
                    InvoiceStatus.Closed,
                } : new string[] {
                    InvoiceStatus.Late,
                },
                ExternalPartnerId = model.idClient,
                IgnorePagination = true,
            });

            if (model.type == TypeExportInvoice.releve)
            {
                var grouping = Map<IEnumerable<DocumentSharedModel>>(avoirs.Value)
                .Concat(Map<IEnumerable<DocumentSharedModel>>(factures.Value))
                .GroupBy(e => e.Client.Id);

                foreach (var clientGrouping in grouping)
                {
                    var res = _fileService.generateInvoicebyFilter(model, clientGrouping);
                    bytes.Add(res);
                }

            }

            if (model.type == TypeExportInvoice.relance)
            {
                var grouping = Map<IEnumerable<DocumentSharedModel>>(factures.Value)
                .GroupBy(e => e.Client.Id);

                foreach (var clientGrouping in grouping)
                {
                    var res = _fileService.generateInvoicebyFilter(model, clientGrouping);
                    bytes.Add(res);
                }
            }

            if (model.exportFcature)
            {

                var invoicesClient = factures.Value.GroupBy(e => e.Client.Id);
                foreach (IEnumerable<Invoice> InvoiceGrouping in invoicesClient)
                {
                    var res = _fileService.generateInvoicebyFilterClient(InvoiceGrouping);
                    filesDetail.Add(res);
                }
            }

            if (model.type == TypeExportInvoice.releve && model.exportFcature)
            {

                var CrditsClient = avoirs.Value.GroupBy(e => e.Client.Id);
                foreach (IEnumerable<CreditNote> creditGrouping in CrditsClient)
                {
                    var res = _fileService.generateCreditNotebyFilterClient(creditGrouping);
                    filesDetail.Add(res);
                }

            }


            return new ExportByte
            {
                InvoiceCredit = bytes,
                Documents = filesDetail
            };

        }
    }

    /// <summary>
    /// partial part for <see cref="InvoiceService"/>
    /// </summary>
    public partial class InvoiceService : DocumentService<Invoice>, IInvoiceService
    {
        private readonly IQuoteService _quoteService;
        private readonly IMaintenanceOperationSheetService _maintenanceOPService;
        private readonly IOperationSheetService _operationSheetService;
        private readonly IClientService _clientService;
        private readonly IConstructionWorkshopService _workshopService;
        private readonly ICreditNoteService _creditNoteService;
        private readonly IEmailService _emailService;

        private new IInvoiceDataAccess _dataAccess => base._dataAccess as IInvoiceDataAccess;

        public InvoiceService(
            IInvoiceDataAccess dataAccess,
            IQuoteService quoteService,
            IConstructionWorkshopService workshopService,
            IMaintenanceOperationSheetService maintenanceOPService,
            ISynchronizationResolverService synchronizeDataService,
            IOperationSheetService operationSheetService,
            IClientService clientService,
            ICreditNoteService creditNoteService,
            IFileService fileService,
            IEmailService emailService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizeDataService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _workshopService = workshopService;
            _creditNoteService = creditNoteService;
            _quoteService = quoteService;
            _operationSheetService = operationSheetService;
            _clientService = clientService;
            _emailService = emailService;
            _maintenanceOPService = maintenanceOPService;
        }

        protected override async Task InDelete_BeforDeleteAsync(Invoice invoice)
        {
            if (invoice.TypeInvoice == InvoiceType.Situation || invoice.TypeInvoice == InvoiceType.Acompte)
            {
                await _quoteService.DeleteInvoiceFromListSituationsAsync(invoice.QuoteId, invoice.Id);
            }

            // check if the invoice has a quote
            if (invoice.QuoteId.IsValid())
            {
                // update quote status
                await _quoteService.UpdateStatusAsync(invoice.QuoteId, new DocumentUpdateStatusModel
                {
                    Status = QuoteStatus.Accepted,
                });
            }

            // and finally update operation sheets associated with the invoice
            await _operationSheetService.UpdateInvoiceOperationSheetStatusAsync(invoice.Id, new DocumentUpdateStatusModel
            {
                Status = OperationSheetStatus.Planned,
            });
        }

        protected override async Task InCreate_AfterInsertAsync<TCreateModel>(Invoice invoice, TCreateModel createModel)
        {
            if (createModel is InvoicePutModel model)
            {
                if (model.OperationSheetsIds.Count > 0)
                    await _operationSheetService.BillOperationSheetsAsync(invoice.Id, model.OperationSheetsIds.ToArray());

                // update the quote in case of the invoice is not general
                if (invoice.TypeInvoice != InvoiceType.General)
                {
                    await _quoteService.UpdateListSituationsAsync(invoice.QuoteId, new QuoteSituations
                    {
                        InvoiceId = invoice.Id,
                        Reference = invoice.Reference,
                        Situation = invoice.Situation,
                        TypeInvoice = invoice.TypeInvoice,
                        TotalHT = invoice.GetTotalHT(),
                        TotalTTC = invoice.GetTotalTTC(),
                        Status = invoice.Status,
                        DueDate = invoice.DueDate,
                        CreationDate = invoice.CreationDate,
                        Purpose = invoice.Purpose,
                    });
                }
                if (invoice.QuoteId.IsValid() && invoice.Status != InvoiceStatus.Draft)
                {
                    await _quoteService.UpdateStatusAsync(invoice.QuoteId, new DocumentUpdateStatusModel
                    {
                        Status = QuoteStatus.Billed
                    });
                }

                if (invoice.OperationSheetMaintenanceId.IsValid() && invoice.Status != InvoiceStatus.Draft)
                {
                    await _maintenanceOPService.UpdateStatusAsync(invoice.OperationSheetMaintenanceId, new DocumentUpdateStatusModel
                    {
                        Status = MaintenanceOperationSheetStatus.Billed
                    });
                }

                if (invoice.WorkshopId.IsValid())
                    await _workshopService.UpdateStatusAsync(invoice.WorkshopId, new WorkshopUpdateStatusModel { Status = WorkshopStatus.Accepted });
            }
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(Invoice invoice, TUpdateModel updateModel)
        {
            invoice.Workshop = null;
            invoice.Contract = null;
            return Task.CompletedTask;
        }

        protected override async Task InUpdate_AfterUpdateAsync<TUpdateModel>(Invoice invoice, TUpdateModel updateModel)
        {
            if (updateModel is InvoicePutModel model)
            {
                if (model.OperationSheetsIds.Count > 0)
                    await _operationSheetService.BillOperationSheetsAsync(invoice.Id, model.OperationSheetsIds.ToArray());

                // update the quote in case of the invoice is not general
                if (invoice.TypeInvoice != InvoiceType.General && invoice.TypeInvoice != InvoiceType.Cloture)
                {
                    await _quoteService.UpdateListSituationsAsync(invoice.QuoteId, new QuoteSituations
                    {
                        InvoiceId = invoice.Id,
                        Reference = invoice.Reference,
                        Situation = invoice.Situation,
                        TypeInvoice = invoice.TypeInvoice,
                        TotalHT = invoice.GetTotalHT(),
                        TotalTTC = invoice.GetTotalTTC(),
                        Status = invoice.Status,
                        DueDate = invoice.DueDate,
                        CreationDate = invoice.CreationDate
                    });
                }
                if (invoice.QuoteId.IsValid() && invoice.Status != InvoiceStatus.Draft)
                {
                    await _quoteService.UpdateStatusAsync(invoice.QuoteId, new DocumentUpdateStatusModel
                    {
                        Status = QuoteStatus.Billed
                    });
                }

                if (invoice.OperationSheetMaintenanceId.IsValid() && invoice.Status != InvoiceStatus.Draft)
                {
                    await _maintenanceOPService.UpdateStatusAsync(invoice.OperationSheetMaintenanceId, new DocumentUpdateStatusModel
                    {
                        Status = MaintenanceOperationSheetStatus.Billed
                    });
                }
            }
        }

        protected override Task InGet_AfterMappingAsync<TOut>(Invoice entity, TOut mappedEntity)
        {
            if (mappedEntity is InvoiceModel model)
            {
                var documents = Map<IEnumerable<AssociatedDocument>>(entity.CreditNotes)
                    .Concat(Map<IEnumerable<AssociatedDocument>>(entity.OperationSheets))
                    .Concat(Map<IEnumerable<AssociatedDocument>>(entity.CreditNotes));

                if (!(entity.Quote is null))
                {
                    model.AssociatedDocuments.Add(Map<AssociatedDocument>(entity.Quote));
                }

                if (!(entity.OperationSheetMaintenance is null))
                {
                    model.AssociatedDocuments.Add(Map<AssociatedDocument>(entity.OperationSheetMaintenance));
                }

                foreach (var item in documents)
                    model.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }
    }
}
