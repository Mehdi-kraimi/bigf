﻿namespace Axiobat.Services.AccountManagement
{
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using AutoMapper;
    using Axiobat.Application.Exceptions;
    using Axiobat.Application.Services.FileService;
    using Axiobat.Application.Services.Localization;
    using Domain.Entities;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// this class is the role management service implementation
    /// </summary>
    public partial class RoleManagementService
    {
        /// <summary>
        /// check if a role with the given id is exist
        /// </summary>
        /// <param name="roleName">the id of the role</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsRoleExistAsync(Guid roleId)
            => _roleDataAccess.IsExistAsync(roleId);

        /// <summary>
        /// check if a role with the given name exist
        /// </summary>
        /// <param name="roleName">the name of the role</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsRoleExistAsync(string roleName)
            => _roleDataAccess.IsExistAsync(r => r.NormalizedName == Normalize(roleName));

        /// <summary>
        /// get the list of roles owned by a user
        /// </summary>
        /// <returns>the list of roles, with their permissions</returns>
        public async Task<ListResult<RoleModel>> GetRoleListAsync()
            => _mapper.Map<List<RoleModel>>(await _roleDataAccess.GetAllAsync(null));

        public override async Task<ListResult<TOut>> GetAllAsync<TOut>()
        {
            var data = await _dataAccess.GetAllAsync(null);
            return _mapper.Map<List<TOut>>(data);
        }

        /// <summary>
        /// create a new role
        /// </summary>
        /// <param name="roleModel">the role model for creating new role entity</param>
        /// <returns>the newly created entity</returns>
        public async Task<Result<RoleModel>> CreateRoleAsync(RoleUpdateModel roleModel)
        {
            if (!await IsRoleExistAsync(roleModel.Name))
            {
                _logger.LogWarning(LogEvent.InsertARecored, "user [id= {userId}] trying to create a role with [name= {roleName}] that is already exist", _loggedInUserService.User.UserId, roleModel.Name);
                return Result.Failed<RoleModel>("a role with the given name already exist", MessageCode.DuplicateEntry);
            }

            var role = _mapper.Map<Role>(roleModel);
            role.Type = Domain.Enums.RoleType.UserDefined;
            role.NormalizedName = Normalize(roleModel.Name);

            var result = await _roleDataAccess.AddAsync(role);

            if (!result.IsSuccess)
                return Result.From<RoleModel, Role>(result);

            _logger.LogInformation("role added successfully");
            return _mapper.Map<RoleModel>(result.Value);
        }

        /// <summary>
        /// delete the role with the given id
        /// </summary>
        /// <param name="roleId">the id of the role to be deleted</param>
        /// <returns>operation result</returns>
        public async Task<Result> DeleteRoleAsync(Guid roleId)
        {
            var role = await _roleDataAccess.GetByIdAsync(roleId);
            if (role is null)
            {
                _logger.LogDebug(LogEvent.ConfirmEmail, "Failed to retrieve the role with [id: {roleId}]", roleId);
                throw new NotFoundException("the role with given id not exist");
            }

            // the user is trying to delete the system defined roles
            if (role.IsSystemRole())
            {
                _logger.LogWarning(LogEvent.DeleteARecored, "user [id= {UserId}] trying to delete one of system defined roles", _loggedInUserService.User.UserId, role.Id);
                throw new UnauthorizedException("you don't have the permission to delete this Role");
            }

            return await _roleDataAccess.DeleteAsync(roleId);
        }

        /// <summary>
        /// update the role with the given id using the passed in model
        /// </summary>
        /// <param name="roleId">the id of the role to be updated</param>
        /// <param name="roleModel">the role model that will be used for update</param>
        /// <returns>the updated version of the role</returns>
        public async Task<Result<RoleModel>> UpdateRoleAsync(Guid roleId, RoleUpdateModel roleModel)
        {
            var updateResult = await _roleDataAccess.UpdateAsync(roleId, role =>
            {
                role.Name = roleModel.Name;
            });

            if (!updateResult.IsSuccess)
                return Result.From<RoleModel>(updateResult);

            // everything is OK map the new user to UserModel and return the result
            return _mapper.Map<RoleModel>(updateResult.Value);
        }

        /// <summary>
        /// update the role Access rights
        /// </summary>
        /// <param name="roleId">the id of the role to update</param>
        /// <param name="model">the role access model</param>
        /// <returns>the operation result</returns>
        public async Task UpdateRolePermissionsAsync(Guid roleId, RolePermissionsModel model)
        {
            var role = await _roleDataAccess.GetByIdAsync(roleId);
            if (role is null)
            {
                _logger.LogDebug(LogEvent.ConfirmEmail, "Failed to retrieve the role with [id: {roleId}]", roleId);
                throw new NotFoundException("the role with given id not exist");
            }

            if (role.IsSystemRole())
            {
                _logger.LogWarning(LogEvent.DeleteARecored, "user [id= {UserId}] trying to update the permissions of a system defined roles", _loggedInUserService.User.UserId, role.Id);
                throw new UnauthorizedException("you don't have the permission to update this Role");
            }

            role.Permissions = model.Permissions;
            await _roleDataAccess.UpdateAsync(role);
        }
    }

    /// <summary>
    /// partial part for <see cref="RoleManagementService"/>
    /// </summary>
    public partial class RoleManagementService : DataService<Role, Guid>, IRoleManagementService
    {
        private readonly IUserDataAccess _userDataAccess;
        private readonly IRoleDataAccess _roleDataAccess;
        private readonly ILookupNormalizer _keyNormalizer;

        /// <summary>
        /// name of the entity
        /// </summary>
        protected override string EntityName => nameof(Role);

        public RoleManagementService(
            IRoleDataAccess roleDataAccess,
            IUserDataAccess userDataAccess,
            ILookupNormalizer keyNormalizer,
            IFileService fileService,
            IValidationService validationService,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appConfigService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(roleDataAccess, fileService, validationService, historyService, loggedInUserService, appConfigService, translationService, loggerFactory, mapper)
        {
            _roleDataAccess = roleDataAccess;
            _keyNormalizer = keyNormalizer;
            _userDataAccess = userDataAccess;
        }

        /// <summary>
        /// normalize the given key
        /// </summary>
        /// <param name="key">the key to normalize</param>
        /// <returns>the string normalized</returns>
        private string Normalize(string key)
            => _keyNormalizer.Normalize(key);

        public Task<Result> UpdateRolePermissionsAsync(string id, RolePermissionsModel roleAcessModel)
        {
            throw new NotImplementedException();
        }
    }
}
