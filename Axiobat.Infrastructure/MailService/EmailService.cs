﻿namespace Axiobat.Infrastructure.MailService
{
    using App.Common;
    using Application.Models;
    using Application.Services.Configuration;
    using Application.Services.MailService;
    using Application.Exceptions;
    using Domain.Constants;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using SharedServices.EmailServices.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Net.Mail;
    using System.Net;
    using System.Net.Mime;

    /// <summary>
    /// the implementation of IMailService
    /// </summary>
    public partial class EmailService
    {
        /// <summary>
        /// this method is used to send emails
        /// </summary>
        /// <param name="mail">the email model</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendEmailAsync(SendEmailOptions mail)
        {
            try
            {
                // get the messaging configuration
                var configuration = await _appConfig.GetAsync<MessagingSettings>(ApplicationConfigurationType.Messaging);
                mail.From = configuration.UserName;

                if (!mail.From.IsValid())
                    throw new ValidationException("you must specify a valid From email, (must be specify in the Messaging settings)", MessageCode.EmailIsRequired);

                var module = new SendEmailModel
                {
                    MailId = mail.Id,
                    Body = mail.Body,
                    Subject = mail.Subject,
                    From = new Email(mail.From),
                    MessagingType = MessagingType.server,
                    ServerConfiguration = configuration.ToJson(),
                    To = mail.To.Select(e => new Email(e)).ToList(),
                    Cc = mail.Cc.Select(e => new Email(e)).ToList(),
                    Bcc = mail.Bcc.Select(e => new Email(e)).ToList(),
                    Attachments = mail.Attachments.Select(e => new EmailAttachment
                    {
                        Base64File = e.Content,
                        FileName = e.FileName,
                        FileType = e.FileType
                    })
                    .ToList(),
                };

                // send the email
                //await _emailService.SendAsync(module);
                await SendAsync(module, configuration);

                return Result.Success();
            }
            catch(SmtpException ex)
            {
                if (ex.InnerException is System.Security.Authentication.AuthenticationException authexception)
                {
                    return Result.Failed($"this server don't support SSL", MessageCode.SSLNotSupported, ex);
                }

                return Result.Failed($"Failed to send the email, exception has been thrown", MessageCode.OperationFailedException, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.SendingEmail, ex, "Failed to send the email, using the model {@mailModel}", mail);
                return Result.Failed($"Failed to send the email, exception has been thrown", MessageCode.OperationFailedException, ex);
            }
        }

        /// <summary>
        /// this method is used to send a rest password email,
        /// and also it responsible for building the URL
        /// </summary>
        /// <param name="user">the user to send the rest password email to</param>
        /// <param name="token">the rest password token</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendRestPasswordEmailAsync(User user, string token)
        {
            try
            {
                var actionBaseUrl = _applicationSettings.GetUrl(ApplicationsURLs.RestPassword);
                var url = $"{actionBaseUrl}?id={user.Id.Encode()}&email={user.Email.Encode()}&token={token.Encode()}";

                await _emailService.SendAsync(new SendEmailModel
                {
                    MessagingType = MessagingType.server,
                    MailId = Generator.GenerateRandomId(),
                    Subject = "Axiobat password rest",
                    Body = $"use this link to rest your password : {url}",
                    From = new Email("infra@artinove.fr", "Axiobat"),
                    To = new List<Email> { new Email(user.Email, user.FullName) },
                });

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.SendingRestPasswordEmail, "Failed sending the rest password email to user with [email: {userEmail}]", user.Email);
                return Result.Failed($"Failed sending the rest password email", MessageCode.OperationFailed, ex);
            }
        }

        /// <summary>
        /// send an email to user to confirm his email
        /// </summary>
        /// <param name="user">the user to send the email to</param>
        /// <param name="companyType">type of the company</param>
        /// <param name="token">the confirmation token</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendConfirmEmailUrlAsync(User user, string token)
        {
            try
            {
                var actionBaseUrl = _applicationSettings.GetUrl(ApplicationsURLs.RestPassword);
                var url = $"{actionBaseUrl}?id={user.Id.Encode()}&email={user.Email.Encode()}&token={token.Encode()}";

                await _emailService.SendAsync(new SendEmailModel
                {
                    MailId = Generator.GenerateRandomId(),
                    Body = $"use this link to confirm your email: {url}",
                    Subject = "Axiobat email confirmation",
                    From = new Email("support@axiobat.com", "Axiobat"),
                    MessagingType = MessagingType.server,
                    To = new List<Email> { new Email(user.Email, user.FullName) },
                });

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.SendingConfiramtionEmail, "Failed to send the confirmation Email, to user with [email: {userEmail}]", user.Email);
                return Result.Failed($"Failed to send the confirmation Email", MessageCode.OperationFailed, ex);
            }
        }
    }

    /// <summary>
    /// the partial part for <see cref="EmailService"/>
    /// </summary>
    public partial class EmailService : IEmailService
    {
        private readonly ILogger<EmailService> _logger;
        private readonly IApplicationConfigurationService _appConfig;
        private readonly SharedServices.EmailServices.IEmailService _emailService;
        private readonly IApplicationSettingsAccessor _applicationSettings;

        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="resultBuilder">the result builder</param>
        public EmailService(
            SharedServices.EmailServices.IEmailService emailService,
            IApplicationSettingsAccessor applicationSettings,
            IApplicationConfigurationService appConfigService,
            ILogger<EmailService> logger)
        {
            _logger = logger;
            _appConfig = appConfigService;
            _emailService = emailService;
            _applicationSettings = applicationSettings;
        }

        /// <summary>
        /// send the given email model information over SMTP
        /// </summary>
        /// <param name="model">the Model to be sent</param>
        /// <param name="configuration">the SMTP configuration, if no configuration is supplied the default one will be used</param>
        private async Task SendAsync(SendEmailModel model, MessagingSettings configuration)
        {
            using (var client = new SmtpClient())
            {
                client.Port = int.Parse(configuration.Port);
                client.Host = configuration.Server;
                client.EnableSsl = configuration.SSL;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(configuration.UserName, configuration.Password);

                _logger.LogDebug("setting the base mail message object");
                var message = new MailMessage
                {
                    From = new MailAddress(model.From.Address, model.From.DisplayName),
                    Subject = model.Subject,
                    Body = model.Body,
                };

                _logger.LogDebug("Adding the emails (TOs, Bcc, and Cc)...");
                foreach (var email in model.To) message.To.Add(new MailAddress(email.Address, email.DisplayName));
                foreach (var email in model.Bcc) message.Bcc.Add(new MailAddress(email.Address, email.DisplayName));
                foreach (var email in model.Cc) message.CC.Add(new MailAddress(email.Address, email.DisplayName));

                SetAttachments(model, message);

                _logger.LogDebug("sending the email ...");
                await client.SendMailAsync(message);
            }
        }


        /// <summary>
        /// add list of attachments
        /// </summary>
        /// <param name="model">the models that contains the attachments</param>
        /// <param name="message">the mail message instant</param>
        private void SetAttachments(SendEmailModel model, MailMessage message)
        {
            _logger.LogDebug("Adding attachments ...");
            foreach (var file in model.Attachments)
            {
                byte[] bitmapData = FileHelper.GetByteArray(file.Base64File);
                System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);

                var imageToInline = new LinkedResource(streamBitmap)
                {
                    ContentId = file.FileName,
                    ContentType = new ContentType(file.FileType),
                };

                var attachment = new System.Net.Mail.Attachment(imageToInline.ContentStream, imageToInline.ContentType)
                {
                    Name = file.FileName,
                    TransferEncoding = TransferEncoding.Base64
                };

                message.Attachments.Add(attachment);
            }
        }
    }

    /// <summary>
    /// a class that holds information about SMTP configuration
    /// </summary>
    public partial class SMTPConfiguration
    {
        /// <summary>
        /// user name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// server
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// port
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// SSL
        /// </summary>
        public bool SSL { get; set; }
    }
}