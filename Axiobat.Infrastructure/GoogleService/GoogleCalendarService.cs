﻿namespace Axiobat.Infrastructure.GoogleService
{
    using App.Common;
    using Application.Services.Configuration;
    using Domain.Constants;
    using Domain.Entities;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Calendar.v3;
    using Google.Apis.Calendar.v3.Data;
    using Google.Apis.Services;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the Google service implementation of <see cref="IGoogleCalendarService"/>
    /// </summary>
    public partial class GoogleCalendarService : IGoogleCalendarService
    {
        /// <summary>
        /// delete the event with the given id
        /// </summary>
        /// <param name="eventId">the id of the Google calendar event to be deleted</param>
        public async Task DeleteEventAsync(string eventId)
        {
            try
            {
                using (var service = await GetCalendarServiceAsync())
                {
                    var GoogleConfiguration = await _configuration.GetAsync<GoogleConfiguration>(ApplicationConfigurationType.GoogleConfiguration);
                    if (!GoogleConfiguration.CalendarId.IsValid())
                    {
                        _logger.LogDebug("there is no calendar id set in the application configuration");
                        return;
                    }

                    var result = await service.Events.Delete(GoogleConfiguration.CalendarId, eventId).ExecuteAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "failed to delete Google calendar event with [id: {eventId}]", eventId);
            }
        }

        /// <summary>
        /// add a new event to Google calendar
        /// </summary>
        /// <param name="documentType">the type of the document</param>
        /// <param name="reference">the document reference</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the generated code from Google calendar</returns>
        public async Task<string> RegisterEventAsync(string documentType, string reference, DateTime startDate, DateTime endDate)
        {
            try
            {
                using (var service = await GetCalendarServiceAsync())
                {
                    var GoogleConfiguration = await _configuration.GetAsync<GoogleConfiguration>(ApplicationConfigurationType.GoogleConfiguration);
                    if (!GoogleConfiguration.CalendarId.IsValid())
                    {
                        _logger.LogDebug("there is no calendar id set in the application configuration");
                        return null;
                    }

                    Event calendarEvent = new Event()
                    {
                        Summary = $"{documentType} {reference}",
                        Start = new EventDateTime() { DateTime = startDate },
                        End = new EventDateTime() { DateTime = endDate },
                    };

                    var result = await service.Events.Insert(calendarEvent, GoogleConfiguration.CalendarId).ExecuteAsync();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "failed to register a Google calendar event for document [{docType}] with reference: [{reference}]", documentType, reference);
                return null;
            }
        }

        /// <summary>
        /// update Google calendar event with the given id
        /// </summary>
        /// <param name="eventId">the Google Calendar event Id</param>
        /// <param name="documentType">the type of the document</param>
        /// <param name="reference">the document reference</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the code from Google calendar</returns>
        public async Task<string> UpdateEventAsync(string eventId, string documentType, string reference, DateTime startDate, DateTime endDate)
        {
            try
            {
                using (var service = await GetCalendarServiceAsync())
                {
                    var GoogleConfiguration = await _configuration.GetAsync<GoogleConfiguration>(ApplicationConfigurationType.GoogleConfiguration);
                    if (!GoogleConfiguration.CalendarId.IsValid())
                    {
                        _logger.LogDebug("there is no calendar id set in the application configuration");
                        return null;
                    }

                    Event calendarEvent = new Event()
                    {
                        Summary = $"{documentType} {reference}",
                        Start = new EventDateTime() { DateTime = startDate },
                        End = new EventDateTime() { DateTime = endDate },
                    };

                    var result = await service.Events.Update(calendarEvent, GoogleConfiguration.CalendarId, eventId).ExecuteAsync();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "failed to update Google calendar event [{eventId}] for document [{docType}] with reference: [{reference}]", eventId, documentType, reference);
                return null;
            }
        }
    }

    /// <summary>
    /// partial part for <see cref="GoogleCalendarService"/>
    /// </summary>
    public partial class GoogleCalendarService
    {
        private readonly string[] _scopes = new string[] { CalendarService.Scope.Calendar };
        private readonly IApplicationConfigurationService _configuration;
        private readonly IApplicationSecretAccessor _appSecrets;
        private readonly ILogger _logger;

        /// <summary>
        /// create an instant of <see cref="GoogleCalendarService"/>
        /// </summary>
        public GoogleCalendarService(
            IApplicationConfigurationService applicationConfiguration,
            IApplicationSecretAccessor applicationSecrets,
            ILoggerFactory loggerFactory)
        {
            _appSecrets = applicationSecrets;
            _configuration = applicationConfiguration;
            _logger = loggerFactory.CreateLogger<GoogleCalendarService>();
        }

        /// <summary>
        /// build and get the CalendarService
        /// </summary>
        /// <returns>the <see cref="CalendarService"/> instant</returns>
        private async Task<CalendarService> GetCalendarServiceAsync()
        {
            var googleOptions = await _appSecrets.GetGoogleCalendarOptions();

            var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(googleOptions.Client_Email) { Scopes = _scopes }
                .FromPrivateKey(googleOptions.Private_Key));

            return new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
            });
        }
    }
}
