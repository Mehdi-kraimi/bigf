﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using App.Common;
    using Axiobat.Application.Models;
    using Axiobat.Domain.Entities.Configuration;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public partial class Invoice_FilterPDFService
    {
        /// <summary>
        /// generate a PDF for Invoice CreditNote
        /// </summary>
        /// <param name="invoice">the invoice document instant</param>
        /// <returns>the PDF document as Byte[]</returns>
        internal byte[] Generate()
        {
            using (var memoryStream = new MemoryStream())
            {
                // create document and PDF writer
                var document = new iTextSharp.text.Document(PageSize.A4, 30, 30, 5, 60);
                var writer = PdfWriter.GetInstance(document, memoryStream);

                var client = _document.First().Client;

                // open the document
                document.Open();

                // add document header section
                document.Add(CreateHeaderSection(_configuration, _model , client));

                BodyArticles(document, _document);

                // close document
                document.Close();

                // export the file as byte array
                return memoryStream.ToArray();
            }
        }
    }

    public partial class Invoice_FilterPDFService
    {
        private readonly DocumentConfiguration _configuration;
        private readonly ExportByPeriodByte _model;
        private readonly IEnumerable<DocumentSharedModel> _document;

        public Invoice_FilterPDFService(DocumentConfiguration configuration , ExportByPeriodByte model , IEnumerable<DocumentSharedModel> document, PdfOptions pdfOptions)
        {
            _configuration = configuration;
            _model = model;
            _document = document;
        }

        private static IElement CreateHeaderSection(DocumentConfiguration configuration, ExportByPeriodByte model , ClientDocument client)
        {

            PdfPTable table_header = new PdfPTable(2);
            table_header.WidthPercentage = 100;

            // Left header
            PdfPTable leftRow = new PdfPTable(1);
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;

            var logo = Image.GetInstance(FileHelper.GetByteArray(configuration.PDFConfiguration.Logo));
            logo.ScaleAbsolute(130f, 50f);


            leftRow.AddCell(new PdfPCell(logo) { FixedHeight = 50f, BorderWidth = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
            leftRow.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });

            PdfPTable leftRowseco = new PdfPTable(1);

            leftRow.AddCell(new PdfPCell(CreateHTMLParagraph(configuration.Header.EnsureValue())) { BorderWidth = 0, PaddingLeft = 15f, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });

            leftRow.AddCell(new PdfPCell(leftRowseco) { BorderWidth = 0 });

            leftRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            // Right table
            PdfPTable rightRow = new PdfPTable(1);
            rightRow.WidthPercentage = 80;
            rightRow.DefaultCell.Border = Rectangle.NO_BORDER;


            PdfPTable secondRight = new PdfPTable(1);
            rightRow.AddCell(new PdfPCell(secondRight) { BorderWidth = 0 });
            rightRow.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });

            //
            if (model.type == TypeExportInvoice.releve)
            {
                rightRow.AddCell(new Paragraph("Relevé des Factures du " + formatDateString(model.startDate) + " au " + formatDateString(model.endDate), PDFFonts.H10Bold));
            }
            if (model.type == TypeExportInvoice.releve)
            {
                rightRow.AddCell(new Paragraph("Relance des Factures du " + formatDateString(model.startDate) + " au " + formatDateString(model.startDate), PDFFonts.H10Bold));
            }
            rightRow.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });
            PdfPTable infClient = new PdfPTable(1) { SpacingBefore = 4f };

            //InfoClient
            PdfPTable infoClient = new PdfPTable(6);

            infClient.AddCell(new PdfPCell(new Paragraph(new Chunk((client.BillingAddress == null) ? " " : client.BillingAddress.Street, PDFFonts.H10))) { BorderWidth = 0 });
            infClient.AddCell(new PdfPCell(new Paragraph(new Chunk((client.BillingAddress == null) ? " " : client.BillingAddress.Complement, PDFFonts.H10))) { BorderWidth = 0 });
            infClient.AddCell(new PdfPCell(new Paragraph(new Chunk((client.BillingAddress == null) ? " " : client.BillingAddress.PostalCode + " " + client.BillingAddress.City, PDFFonts.H10))) { BorderWidth = 0 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Nom Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Code Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Adresse Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(client.FullName, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(client.Code, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(infClient) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            rightRow.AddCell(new PdfPCell(infoClient) { BorderWidth = 0 });

            rightRow.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 0 });



            //document.Add(table_header);
            //writer.PageEvent = new Footer();
            // logo

            return table_header;
        }

        public void BodyArticles(iTextSharp.text.Document document, IEnumerable<DocumentSharedModel> documents)
        {

                PdfPTable articlestab = new PdfPTable(8) { SpacingBefore = 5f, WidthPercentage = 100 };
                articlestab.DefaultCell.Border = Rectangle.NO_BORDER;

                // Header table
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Date", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Nature", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Référence interne", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Montant (€)", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Reste à payer (€)", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

                double? MONTANT = 0;
                double? sommePaye = 0;

            foreach (var item in documents)
            {

                if (item.Payments != null)
                {
                    MONTANT = MONTANT + item.Payments.Sum(s => s.Amount);
                    sommePaye = item.Payments.Sum(s => s.Amount);
                }


                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(Convert.ToString(formatDateString(item.CreationDate)), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk( (item.DocumentType == DocumentType.Invoice ) ? Convert.ToString("Facture client") : Convert.ToString("Avoir client"), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(item.Reference, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00 €}", item.OrderDetails.TotalHT), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00 €}", (item.OrderDetails.TotalHT - sommePaye)), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            }


                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("MONTANT", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, PaddingTop = 30f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00} €", MONTANT), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, PaddingTop = 30f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                document.Add(articlestab);

        }

        private static string formatDateString(DateTime? myDate)
        {
            DateTime moment = System.Convert.ToDateTime(myDate);
            string date = moment.Day < 10 ? ("0" + moment.Day) : Convert.ToString(moment.Day);
            string month = moment.Month < 10 ? ("0" + moment.Month) : Convert.ToString(moment.Month);
            return date + "-" + month + "-" + moment.Year;
        }

        private static Paragraph CreateHTMLParagraph(string text)
        {
            Paragraph p = new Paragraph();
            using (StringReader sr = new StringReader(text))
            {
                foreach (IElement e in HtmlWorker.ParseToList(sr, null))
                {
                    p.Add(e);
                }
            }
            return p;
        }
    }
}
