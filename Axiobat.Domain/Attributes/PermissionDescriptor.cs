﻿namespace Axiobat.Domain.Attributes
{
    using App.Common;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using System;

    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class PermissionDescriptorAttribute : Attribute
    {
        /// <summary>
        /// the permission instant
        /// </summary>
        public Permission Permission { get; }

        /// <summary>
        /// add description to an the permission enum flied
        /// </summary>
        /// <param name="id">the id of the permission</param>
        /// <param name="name">the name of the permission</param>
        /// <param name="description">the description</param>
        /// <param name="moduleId">the id of the module</param>
        public PermissionDescriptorAttribute(int id, string name, PermissionType type, string description, AppModules module)
        {
            if (id <= 0)
                throw new ArgumentException("id must be a positive integer");

            if (!name.IsValid())
                throw new ArgumentException("Name must have a value");

            // create permission instant
            Permission = new Permission
            {
                Id = id,
                Name = name,
                Description = description,
                Type = type
            };
        }
    }
}
