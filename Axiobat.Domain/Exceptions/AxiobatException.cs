﻿namespace Axiobat.Domain.Exceptions
{
    using App.Common.Exceptions;
    using System;

    [Serializable]
    public class AxiobatException : AppException
    {
        public AxiobatException() { }
        public AxiobatException(string message) : base(message) { }

        /// <summary>
        /// create an instant of <see cref="AppException"/>
        /// </summary>
        /// <param name="message">the message associated with the exceptions</param>
        /// <param name="messageCode">the message code associated with the exceptions</param>
        public AxiobatException(string message, MessageCode messageCode)
            : base(message, ((int)messageCode).ToString()) { }

        public AxiobatException(string message, Exception inner) : base(message, inner) { }
        protected AxiobatException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
