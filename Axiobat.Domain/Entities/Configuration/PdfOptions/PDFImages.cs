﻿namespace Axiobat.Domain.Entities.Configuration
{
    public partial class PDFImages
    {
        public string Logo { get; set; }
        public string Cachet { get; set; }

    }

}
