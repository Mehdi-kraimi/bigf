﻿namespace Axiobat.Domain.Entities.Configuration
{
    public partial class PDFHeader
    {
        public string CompanyName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public Address Address { get; set; }
    }
}
