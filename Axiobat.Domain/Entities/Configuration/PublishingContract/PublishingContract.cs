﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class describe publishing contracts
    /// </summary>
    [DocType(Enums.DocumentType.PublishingContract)]
    public partial class PublishingContract : Entity<int>
    {
        /// <summary>
        /// the name of contract
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// the name of file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// the original file name
        /// </summary>
        public string OrginalFileName { get; set; }

        /// <summary>
        /// the id of associate who associate with this entity
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// the tags of contract
        /// </summary>
        public IDictionary<string, string> Tags { get; set; }

        /// <summary>
        /// the user associated with this publishing contract
        /// </summary>
        public User User { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="PublishingContract"/>
    /// </summary>
    public partial class PublishingContract : Entity<int>
    {
        /// <summary>
        /// the name of the signature tag
        /// </summary>
        public const string SignatureTag = "signature";

        /// <summary>
        /// create an instant of <see cref="PublishingContract"/>
        /// </summary>
        public PublishingContract()
        {
            Tags = new Dictionary<string, string>();
        }

        /// <summary>
        /// builder of search term
        /// </summary>
        public override void BuildSearchTerms() => SearchTerms = $"{Title} {OrginalFileName}";
    }
}
