﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class holds the tax details
    /// </summary>
    public partial class TaxDetails
    {
        /// <summary>
        /// the Tax value
        /// </summary>
        public float Tax { get; }

        /// <summary>
        /// the total with tax excluded
        /// </summary>
        public float TotalHT { get; }

        /// <summary>
        /// the total of the Tax
        /// </summary>
        public float TotalTax => TotalHT * Tax / 100;

        /// <summary>
        /// the total with the tax included
        /// </summary>
        public float TotalTI => TotalHT + TotalTax;
    }

    /// <summary>
    /// partial part for <see cref="TaxDetails"/>
    /// </summary>
    public partial class TaxDetails
    {
        /// <summary>
        /// create an instant of <see cref="TaxDetails"/>
        /// </summary>
        /// <param name="tax">the tax value</param>
        /// <param name="totalHT">the total HT value</param>
        public TaxDetails(float tax, float totalHT)
        {
            Tax = tax;
            TotalHT = totalHT;
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"tax: {Tax}, Total HT: {TotalHT}, Total TAX: {TotalTax}, the total: {TotalTI}";
    }
}
