﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines a label
    /// </summary>
    public partial class Label : Entity<string>
    {
        /// <summary>
        /// the id of the <see cref="Label"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomId();
        }

        /// <summary>
        /// the value of the label
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// list of products associated with the Label
        /// </summary>
        public ICollection<ProductLabel> Products { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Label"/>
    /// </summary>
    public partial class Label
    {
        private string _id;

        /// <summary>
        /// create an instant of <see cref="Label"/>
        /// </summary>
        public Label()
        {
            Products = new HashSet<ProductLabel>();
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Value}";

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, value: {Value}";
    }
}
