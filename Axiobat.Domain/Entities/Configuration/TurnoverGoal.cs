﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// this class represent the turnover goals registers by the user
    /// </summary>
    public partial class TurnoverGoal : Entity<int>
    {
        /// <summary>
        /// create an instant of <see cref="TurnoverGoal"/>
        /// </summary>
        public TurnoverGoal()
        {
            MonthlyGoals = new HashSet<TurnoverMonthlyGoal>();
        }

        /// <summary>
        /// the goal of the year
        /// </summary>
        public float Goal { get; set; }

        /// <summary>
        /// the predicted goal to be achieved, per month
        /// </summary>
        public ICollection<TurnoverMonthlyGoal> MonthlyGoals { get; set; }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"";
    }

    /// <summary>
    /// this class defines the months goals
    /// </summary>
    public partial class TurnoverMonthlyGoal
    {
        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the goal to be achieved
        /// </summary>
        public float Goal { get; set; }
    }
}
