﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class represent a country information
    /// </summary>
    public partial class Country
    {
        /// <summary>
        /// the code of the country
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// the name of the country
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the language of the country
        /// </summary>
        public string LanguageCode { get; set; }

        /// <summary>
        /// is this country is the default country
        /// </summary>
        public bool IsDefault { get; set; }
    }

    /// <summary>
    /// the partial part of the <see cref="Country"/> entity
    /// </summary>
    public partial class Country
    {
        /// <summary>
        /// Global country identifier
        /// </summary>
        public const string Global = "GLB";

        /// <summary>
        /// the default constructor
        /// </summary>
        public Country()
        {
        }

        /// <summary>
        /// the string value of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name} [{CountryCode}], Language: {LanguageCode}, IsDefault: {IsDefault}";
    }
}
