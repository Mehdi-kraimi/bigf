﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// a additional information
    /// </summary>
    public partial class AdditionalInformation
    {
        /// <summary>
        /// the label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the value
        /// </summary>
        public string Valeur { get; set; }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"info sup Label: {Label}, value: {Valeur}";
    }
}
