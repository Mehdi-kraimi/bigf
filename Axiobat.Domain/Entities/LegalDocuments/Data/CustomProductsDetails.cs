﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the details of the products prices in case that the order product details is a file
    /// </summary>
    public partial class CustomProductsDetails
    {
        /// <summary>
        /// the total estimated hours for all the products in the document
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// the estimated material cost of all products
        /// </summary>
        public float MaterialCost { get; set; }

        /// <summary>
        /// the cost of selling the products, the price is Hourly based.
        /// </summary>
        public float HourlyCost { get; set; }

        /// <summary>
        /// this i don't know the use of it YET
        /// </summary>
        public float AchatsMateriels { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="CustomProductsDetails"/>
    /// </summary>
    public partial class CustomProductsDetails
    {
        /// <summary>
        /// the total amount
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public float TotalHT => (HourlyCost * TotalHours) + MaterialCost;

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Total Hours: {TotalHours}, Hourly Cost: {HourlyCost}, Material Cost : {MaterialCost}, total = {TotalHT}";
    }
}
