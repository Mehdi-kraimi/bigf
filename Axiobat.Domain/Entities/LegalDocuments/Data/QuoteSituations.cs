﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;

    /// <summary>
    /// this class defines the Situation of the quote
    /// </summary>
    public partial class QuoteSituations
    {
        /// <summary>
        /// the id of the invoice
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// the invoice reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the type of the invoice
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the invoice Situation
        /// </summary>
        public float Situation { get; set; }

        /// <summary>
        /// the TotalHT of the invoice
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the TotalTTC of the invoice
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the status of invoice
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Purpose of invoice
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// get the tax percent
        /// </summary>
        /// <returns>the tax percent</returns>
        public float GetTaxPercent() => GetTotalTax() / TotalHT * 100;

        /// <summary>
        /// get the total Tax
        /// </summary>
        /// <returns>the total tax</returns>
        public float GetTotalTax() => TotalTTC - TotalHT;
    }
}
