﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// the details of the Products included in the order
    /// </summary>
    public partial class OrderProductDetails
    {
        /// <summary>
        /// the selected quantity of the product
        /// </summary>
        public float Quantity { get; set; }

        /// <summary>
        /// the type of the product
        /// </summary>
        public ProductType Type { get; set; }

        /// <summary>
        /// the product details
        /// </summary>
        public dynamic Product { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OrderProductDetails"/>
    /// </summary>
    public partial class OrderProductDetails
    {
        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"selected {Quantity} of {Type}";

        /// <summary>
        /// get total amount (tax excluded)
        /// </summary>
        /// <returns>the total</returns>
        public float GetTotalHT()
        {
            if (Product is null)
                return 0;
            float TotalHt = 0;

            if (Product.GetType().GetProperty("totalHT") != null)
            {
                TotalHt = (float)Product.totalHT;
            }
            else if (Product.GetType().GetProperty("TotalHT") != null)
            {
                TotalHt = (float)Product.TotalHT;
            }
            float Quantity = 0;

            if (Product.GetType().GetProperty("quantity") != null)
            {
                Quantity = (float)Product.quantity;
            }
            else if(Product.GetType().GetProperty("Quantity") != null)
            {
                Quantity = (float)Product.Quantity;
            } else
            {
                Quantity = 1;
            }

            //var TotalHt = (float)( Product.totalHT ?? Product.TotalHT ?? 1) ;
            //var Quantity = (float)(Product.quantity ?? Product.Quantity ?? 1);
            return TotalHt * Quantity;
        }

        /// <summary>
        /// get total amount (tax included)
        /// </summary>
        /// <returns>the total</returns>
        public float GetTotalTTC()
        {
            if (Product is null)
                return 0;

            float totalTTC = 0;

            if(Product.GetType().GetProperty("totalTTC") != null)
            {
                totalTTC = (float)Product.totalTTC;
            }
            else
            {
                totalTTC = (float)Product.TotalTTC;
            }

            return totalTTC;
        }

        /// <summary>
        /// get the product cost details
        /// </summary>
        /// <returns>the product cost details</returns>
        public ProductCostDetails GetCostDetails()
        {
            if (Product is null || Type == ProductType.Line)
                return new ProductCostDetails();

            if (Type == ProductType.Product)
            {
                var product = (Product as JObject).ToObject<MinimalProductDetails>();
                return product.CostDetails;
            }

            var lot = (Product as JObject).ToObject<MinimalLotDetails>();
            return lot.CostDetails;
        }
    }
}
