﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the order details for a supplier order
    /// </summary>
    public partial class OrderDetails
    {
        /// <summary>
        /// total HT
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total With Tax included
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the discount
        /// </summary>
        public Discount GlobalDiscount { get; set; }

        /// <summary>
        /// list of the products associated with this order details
        /// </summary>
        public ICollection<OrderProductDetails> LineItems { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OrderDetails"/>
    /// </summary>
    public partial class OrderDetails
    {
        private List<ProductsDetails> _productsDetails;

        /// <summary>
        /// create an instant of <see cref="OrderDetails"/>
        /// </summary>
        public OrderDetails()
        {
            LineItems = new HashSet<OrderProductDetails>();
        }

        /// <summary>
        /// this function is used to calculate the total HT of the order detail
        /// </summary>
        public void CalculateTotalHT()
            => TotalHT = GetTotalHT();

        /// <summary>
        /// this function is used to calculate the total including tax of the order detail
        /// </summary>
        public void CalculateTotalTI()
            => TotalTTC = GetTotalTI();

        /// <summary>
        /// get the total HT of the Order
        /// </summary>
        /// <returns>the total HT</returns>
        public virtual float GetTotalHT()
            => LineItems.Sum(e => e.GetTotalHT());

        /// <summary>
        /// get the total including Tax of the Order
        /// </summary>
        /// <returns>the total TTC</returns>
        public virtual float GetTotalTI()
            => LineItems.Sum(e => e.GetTotalTTC());

        /// <summary>
        /// get the product cost details
        /// </summary>
        /// <returns>the product cost details</returns>
        public virtual ProductCostDetails GetCostDetails()
        {
            var productsCostDetails = LineItems.Select(e => e.GetCostDetails());
            return new ProductCostDetails
            {
                TotalCost = productsCostDetails.Sum(e => e.TotalCost),
                TotalHours = productsCostDetails.Sum(e => e.TotalHours),
                TotalMaterialCost = productsCostDetails.Sum(e => e.TotalMaterialCost)
            };
        }

        /// <summary>
        /// get the tax details informations
        /// </summary>
        public virtual IEnumerable<TaxDetails> GetTaxDetails()
        {
            var discountAmount = GlobalDiscount?.Calculate(TotalHT) ?? 0;

            return GetAllProduct().GroupBy(e => e.Product.VAT)
                .Select(e =>
                {
                    var taxAmount = e.Sum(p => p.Quantity * p.Product.TotalHT);
                    var amountToReduce = discountAmount * taxAmount / TotalHT;
                    return new TaxDetails(e.Key, taxAmount - amountToReduce);
                });
        }

        /// <summary>
        /// retrieve the list of product with their quantity
        /// </summary>
        public virtual IEnumerable<ProductsDetails> GetAllProduct()
        {
            if (!(_productsDetails is null))
                return _productsDetails;

            // set and extract the product information
            ProductsDetails();

            // return the list
            return _productsDetails;
        }

        private void ProductsDetails()
        {
            _productsDetails = new List<ProductsDetails>();
            foreach (var productOrderDetail in LineItems)
            {
                if (productOrderDetail.Type == ProductType.Line)
                    continue;

                // if the product is a product add it directly
                if (productOrderDetail.Type == ProductType.Product)
                {
                    var product = (productOrderDetail.Product as JObject).ToObject<MinimalProductDetails>();
                    _productsDetails.Add(new ProductsDetails(productOrderDetail.Quantity, product));
                    continue;
                }

                // the product is a Lot, we need to extract the articles
                var lot = (productOrderDetail.Product as JObject).ToObject<MinimalLotDetails>();
                foreach (var lotProduct in lot.Products)
                {
                    _productsDetails.Add(new ProductsDetails(productOrderDetail.Quantity * lotProduct.Quantity, lotProduct.ProductDetails));
                }
            }
        }
    }
}
