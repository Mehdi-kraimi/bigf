﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the relationship between all the <see cref="Domain.Entities.OperationSheet"/> and <see cref="User"/>
    /// </summary>
    public partial class OperationSheets_Technicians
    {
        /// <summary>
        /// the id of the operation sheet
        /// </summary>
        public string OperationSheetId { get; set; }

        /// <summary>
        /// the id of the Technician
        /// </summary>
        public Guid TechnicianId { get; set; }

        /// <summary>
        /// the Technician user profile
        /// </summary>
        public User Technician { get; set; }

        /// <summary>
        /// the operation sheet instant
        /// </summary>
        public OperationSheet OperationSheet { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheets_Technicians"/>
    /// </summary>
    public partial class OperationSheets_Technicians : IEquatable<OperationSheets_Technicians>
    {
        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(OperationSheets_Technicians other)
            => !(other is null) && other.TechnicianId == TechnicianId && other.OperationSheetId == OperationSheetId;

        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(OperationSheets_Technicians)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as OperationSheets_Technicians);
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Technician: {TechnicianId}, OperationSheet: {OperationSheetId}";

        /// <summary>
        /// get the hash value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 901867850;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(OperationSheetId);
                hashCode = hashCode * -1521134295 + TechnicianId.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(OperationSheets_Technicians left, OperationSheets_Technicians right) => EqualityComparer<OperationSheets_Technicians>.Default.Equals(left, right);
        public static bool operator !=(OperationSheets_Technicians left, OperationSheets_Technicians right) => !(left == right);
    }
}
