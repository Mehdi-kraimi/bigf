﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the relationships between <see cref="Expense"/> and <see cref="Payment"/>
    /// </summary>
    public partial class Expenses_Payments
    {
        /// <summary>
        /// the id of the Expenses
        /// </summary>
        public string ExpenseId { get; set; }

        /// <summary>
        /// the id of the payments
        /// </summary>
        public string PaymentId { get; set; }

        /// <summary>
        /// the amount that the payment is made for the Expense
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// the Expense
        /// </summary>
        public Expense Expense { get; set; }

        /// <summary>
        /// the payment
        /// </summary>
        public Payment Payment { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Expenses_Payments"/>
    /// </summary>
    public partial class Expenses_Payments
    {
        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Expenses_Payments other)
            => !(other is null) && other.ExpenseId == ExpenseId && other.PaymentId == PaymentId;

        /// <summary>
        /// check if the given instant equals to the current instant
        /// </summary>
        /// <param name="other">the object to check</param>
        /// <returns>true if equals false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Expenses_Payments)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Expenses_Payments);
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Expense: {ExpenseId}, Payment: {PaymentId}";

        /// <summary>
        /// get the hash value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 901867850;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ExpenseId);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PaymentId);
                return hashCode;
            }
        }

        public static bool operator ==(Expenses_Payments left, Expenses_Payments right) => EqualityComparer<Expenses_Payments>.Default.Equals(left, right);
        public static bool operator !=(Expenses_Payments left, Expenses_Payments right) => !(left == right);
    }

    /// <summary>
    /// the EqualityComparer for <see cref="Expenses_Payments"/>
    /// </summary>
    public partial class Expenses_PaymentsEqualityComparer : IEqualityComparer<Expenses_Payments>
    {
        public bool Equals(Expenses_Payments x, Expenses_Payments y)
        {
            if (x is null || y is null) return false;
            if (ReferenceEquals(x, y)) return true;
            return x.ExpenseId == y.ExpenseId && x.PaymentId == y.PaymentId && x.Amount == x.Amount;
        }

        public int GetHashCode(Expenses_Payments obj)
        {
            unchecked
            {
                var hashCode = 701867890;
                hashCode = hashCode * -152554295 + EqualityComparer<string>.Default.GetHashCode(obj.ExpenseId);
                hashCode = hashCode * -152554295 + EqualityComparer<string>.Default.GetHashCode(obj.PaymentId);
                hashCode = hashCode * -152554295 + EqualityComparer<double>.Default.GetHashCode(obj.Amount);
                return hashCode;
            }
        }
    }
}
