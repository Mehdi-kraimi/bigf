﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent a Quote
    /// </summary>
    [DocType(DocumentType.Quote)]
    public partial class Quote
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Quote;

        /// <summary>
        /// Quote creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Quote due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Signature of the client
        /// </summary>
        public  Signature ClientSignature { get; set; }

        /// <summary>
        /// the information of the client associated with this Quote
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// list of invoices associated with the Quote
        /// </summary>
        public ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// list of Supplier Orders associated with this Quote
        /// </summary>
        public ICollection<SupplierOrder> SupplierOrders { get; set; }

        /// <summary>
        /// list of operation sheets associated with this quote
        /// </summary>
        public ICollection<OperationSheet> OperationSheets { get; set; }

        /// <summary>
        /// the list of Situations
        /// </summary>
        public ICollection<QuoteSituations> Situations { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Quote"/>
    /// </summary>
    public partial class Quote : Document, IReferenceable<Quote>
    {
        /// <summary>
        /// create an instant of <see cref="Quote"/>
        /// </summary>
        public Quote() : base()
        {
            Memos = new HashSet<Memo>();
            Invoices = new HashSet<Invoice>();
            Emails = new HashSet<DocumentEmail>();
            Situations = new HashSet<QuoteSituations>();
            SupplierOrders = new HashSet<SupplierOrder>();
            OperationSheets = new HashSet<OperationSheet>();
        }

        /// <summary>
        /// create search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms =$"{Client.FullName} {Purpose} {Reference}".ToLower();

        /// <summary>
        /// check if we can increment the reference on the update
        /// </summary>
        /// <param name="oldState">the old state of the entity</param>
        /// <returns>true if we can, false if not</returns>
        public bool CanIncrementOnUpdate(Quote oldState) => base.CanIncrementOnUpdate(oldState);

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails.TotalTTC;
        }
    }
}
