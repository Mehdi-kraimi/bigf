﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Enums;
    using System.Collections.Generic;

    /// <summary>
    /// the <see cref="Supplier"/> entity
    /// </summary>
    [DocType(DocumentType.Supplier)]
    public partial class Supplier : ExternalPartner, IReferenceable<Supplier>
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Supplier;

        /// <summary>
        /// the address of the supplier
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// list of supplier products
        /// </summary>
        public ICollection<ProductSupplier> Products { get; set; }

        /// <summary>
        /// list of supplier orders associated with this supplier
        /// </summary>
        public ICollection<SupplierOrder> SupplierOrders { get; set; }

        /// <summary>
        /// list of expense associated with this supplier
        /// </summary>
        public ICollection<Expense> Expenses { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Supplier"/>
    /// </summary>
    public partial class Supplier
    {
        /// <summary>
        /// create an instant of <see cref="Supplier"/>
        /// </summary>
        public Supplier()
        {
            Expenses = new HashSet<Expense>();
            Products = new HashSet<ProductSupplier>();
            SupplierOrders = new HashSet<SupplierOrder>();
        }

        public bool CanIncrementOnUpdate(Supplier oldState) => base.CanIncrementOnUpdate(oldState);
    }
}
