﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using Axiobat.Domain.Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// this class represent a group
    /// </summary>
    public partial class Group : Entity<int>
    {

        /// <summary>
        /// name of the group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// list of the clients associated with the groupe
        /// </summary>
        public virtual ICollection<Client> Clients { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Group"/>
    /// </summary>
    public partial class Group: IDeletable
    {
        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Id} {Name}";
    }
}
