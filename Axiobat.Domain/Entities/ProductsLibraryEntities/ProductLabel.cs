﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// a class that defines the relationship between a product and a label
    /// </summary>
    public class ProductLabel
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// the id of the label
        /// </summary>
        public string LabelId { get; set; }

        /// <summary>
        /// the product
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// the label
        /// </summary>
        public Label Label { get; set; }
    }
}
