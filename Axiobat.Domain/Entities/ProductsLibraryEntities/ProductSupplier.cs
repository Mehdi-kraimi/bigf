﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class describe the relationship between the Product and the supplier
    /// </summary>
    public partial class ProductSupplier
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// the price of the product by supplier
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// if this product is the default one for this supplier
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// the product navigation property
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// the supplier product
        /// </summary>
        public Supplier Supplier { get; set; }

        public override string ToString()
            => $"supplier: {SupplierId}, product: {ProductId}, price: {Price}";
    }
}
