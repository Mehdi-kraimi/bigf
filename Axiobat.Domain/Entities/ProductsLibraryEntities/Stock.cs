﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the stock class
    /// </summary>
    public partial class Stock
    {
        /// <summary>
        /// the quantity in the stock
        /// </summary>
        public int Quantite { get; set; }

        /// <summary>
        /// the minimum quantity
        /// </summary>
        public int QuantiteMin { get; set; }
    }
}
