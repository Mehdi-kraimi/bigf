﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a class that defines the history of properties values changes in a given entity
    /// </summary>
    public partial class ChangesHistory
    {
        /// <summary>
        /// the date of the changes
        /// </summary>
        public DateTime DateAction { get; set; }

        /// <summary>
        /// the id of the user who made the change
        /// </summary>
        public MinimalUser User { get; set; }

        /// <summary>
        /// the type of the action performed, example : Added / Modified / Deleted
        /// </summary>
        public ChangesHistoryType Action { get; set; }

        /// <summary>
        /// information
        /// </summary>
        public string Information { get; set; }

        /// <summary>
        /// list of Fields that has been changed
        /// </summary>
        public ICollection<ChangedField> Fields { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ChangesHistory"/>
    /// </summary>
    public partial class ChangesHistory : IEquatable<ChangesHistory>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public ChangesHistory()
        {
            DateAction = DateTime.UtcNow;
            Fields = new List<ChangedField>();
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"user: {User?.UserName}, has made {Fields.Count} changes";

        /// <summary>
        /// check if the given object is equals the current instant
        /// </summary>
        /// <param name="obj">the object to check the equality for it</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj) => Equals(obj as ChangesHistory);

        /// <summary>
        /// check if the given object is equals the current instant
        /// </summary>
        /// <param name="obj">the object to check the equality for it</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(ChangesHistory other)
            => other != null &&
                   Action == other.Action &&
                   DateAction == other.DateAction &&
                   Information == other.Information &&
                   EqualityComparer<MinimalUser>.Default.Equals(User, other.User) &&
                   Fields.ListsEquals(other.Fields);

        /// <summary>
        /// get the has value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            int hashCode = -1391333307;
            hashCode = hashCode * -1521134295 + DateAction.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<MinimalUser>.Default.GetHashCode(User);
            hashCode = hashCode * -1521134295 + Action.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Information);
            hashCode = hashCode * -1521134295 + EqualityComparer<ICollection<ChangedField>>.Default.GetHashCode(Fields);
            return hashCode;
        }

        public static bool operator ==(ChangesHistory left, ChangesHistory right) => EqualityComparer<ChangesHistory>.Default.Equals(left, right);
        public static bool operator !=(ChangesHistory left, ChangesHistory right) => !(left == right);
    }
}
