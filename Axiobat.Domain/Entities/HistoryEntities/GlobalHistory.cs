﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;

    /// <summary>
    /// the class that defines the global history object
    /// </summary>
    public partial class GlobalHistory : Entity<int>
    {
        /// <summary>
        /// the id of the user that did this change
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// the date of the action
        /// </summary>
        public DateTime ActionDate { get; set; }

        /// <summary>
        /// type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the id of the document
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// the reference of the document
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the action type
        /// </summary>
        public ChangesHistoryType Action { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="GlobalHistory"/>
    /// </summary>
    public partial class GlobalHistory
    {
        /// <summary>
        /// user that owns this history
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// build the search terms
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Reference}";
    }
}