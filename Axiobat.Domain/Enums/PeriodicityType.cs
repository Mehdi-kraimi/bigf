﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the ending type of the of periodicity
    /// </summary>
    public enum PeriodicityEndingType
    {
        /// <summary>
        /// the ending is not defined, this will require a manual stopping of the background service
        /// </summary>
        Undifined,

        /// <summary>
        /// the background service will end after a reaching a specific date
        /// </summary>
        SpecificDate,

        /// <summary>
        /// the background service will end after a specific number of time
        /// </summary>
        SpecificNumberOfTime
    }

    /// <summary>
    /// the type of the recurring
    /// </summary>
    public enum PeriodicityRecurringType
    {
        /// <summary>
        /// run every day
        /// </summary>
        EveryDay,

        /// <summary>
        /// run every week
        /// </summary>
        EveryWeek,

        /// <summary>
        /// run every month
        /// </summary>
        EveryMonth
    }

    /// <summary>
    /// type of the Periodicity
    /// </summary>
    public enum PeriodicityType
    {
        /// <summary>
        /// a custom date configuration
        /// </summary>
        Custom,

        /// <summary>
        /// every month
        /// </summary>
        Monthly,

        /// <summary>
        /// every two months
        /// </summary>
        BiMonthly,

        /// <summary>
        /// every 3 months
        /// </summary>
        Quarterly,

        /// <summary>
        /// every 4 months
        /// </summary>
        EveryFourMonths,

        /// <summary>
        /// every 6 months
        /// </summary>
        Biannual,

        /// <summary>
        /// every year
        /// </summary>
        Annually,
    }
}
