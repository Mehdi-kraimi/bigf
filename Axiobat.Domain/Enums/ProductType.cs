﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// type of the product
    /// </summary>
    public enum ProductType
    {
        /// <summary>
        /// the product is single product unite instant of <see cref="Entities.Product"/>
        /// </summary>
        Product = 1,

        /// <summary>
        /// the product is collection of product also named Lot, it an instant of <see cref="Entities.Lot"/>
        /// </summary>
        Lot = 2,

        /// <summary>
        /// the product is single product unite instant of <see cref="Entities.Product"/>
        /// </summary>
        Line = 3
    }
}
