﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the MaintenanceOperationSheet
    /// </summary>
    public enum MaintenanceOperationSheetType
    {    
        /// <summary>
        /// the type is maintenance
        /// </summary>
        Maintenance,

        /// <summary>
        /// the type is after-sales service
        /// </summary>
        AfterSalesService,

    }
}
