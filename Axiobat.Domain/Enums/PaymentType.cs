﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the payment
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        /// a payment for an expense
        /// </summary>
        Expense,

        /// <summary>
        /// a payment for an invoice
        /// </summary>
        Invoice,

        /// <summary>
        /// the payment is a Transfer of funds
        /// </summary>
        Transfer
    }
}
