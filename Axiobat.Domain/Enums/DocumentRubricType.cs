﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// Document Rubric Type
    /// </summary>
    public enum DocumentRubricType
    {
        /// <summary>
        /// a Rubric of type Quote
        /// </summary>
        Quote = 1,

        /// <summary>
        /// a Rubric of type Orders
        /// </summary>
        Orders = 2,

        /// <summary>
        /// a Rubric of type Billing
        /// </summary>
        Billing = 3,

        /// <summary>
        /// a Rubric of type Custom
        /// </summary>
        Custom = 4,

        /// <summary>
        /// a Rubric of type CreditNote
        /// </summary>
        CreditNote = 5,

        /// <summary>
        /// a Rubric of type Expenses
        /// </summary>
        Expenses = 6,

        /// <summary>
        /// a Rubric of type OperationSheet
        /// </summary>
        OperationSheet = 7,
    }
}
