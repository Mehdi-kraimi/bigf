﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the status of an object
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// the pack is Inactive
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// the pack is active
        /// </summary>
        Active = 1,
    }
}
