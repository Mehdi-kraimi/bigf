﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// type of the account
    /// </summary>
    public enum FinancialAccountsType
    {
        /// <summary>
        /// the Financial Accounts is a cash
        /// </summary>
        Cash = 1,

        /// <summary>
        /// the Financial Accounts is a bank
        /// </summary>
        Bank = 2,
    }
}
