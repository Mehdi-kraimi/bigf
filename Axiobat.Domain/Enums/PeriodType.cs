﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the period types
    /// </summary>
    public enum PeriodType
    {
        /// <summary>
        /// the period is in days
        /// </summary>
        Day = 1,

        /// <summary>
        /// the period is in weeks
        /// </summary>
        Week = 7,

        /// <summary>
        /// the period is in months
        /// </summary>
        Month = 30,

        /// <summary>
        /// the period is in year
        /// </summary>
        Year = 365,
    }
}
