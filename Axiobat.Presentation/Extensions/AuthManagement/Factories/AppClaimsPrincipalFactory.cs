﻿namespace Axiobat.Presentation.AuthManagement
{
    using App.Common;
    using Application.Data;
    using Domain.Entities;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;

    /// <summary>
    /// the claim factory
    /// </summary>
    public partial class AppClaimsPrincipalFactory : UserClaimsPrincipalFactory<User>
    {
        /// <summary>
        /// generate the claims for the given user
        /// </summary>
        /// <param name="user">the current logged in user</param>
        /// <returns>the claims identity</returns>
        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(User user)
        {
            var role = await _roleDataAccess.GetUserRoleAsync(user.Id);

            var claims = new List<Claim>
            {
                new Claim(AppClaimTypes.UserEmail, user.Email),
                new Claim(AppClaimTypes.UserName, user.UserName),
                new Claim(AppClaimTypes.UserId, user.Id.ToString()),
                new Claim("fullName", user.FullName?? ""),
            };

            if (!(role is null))
            {
                claims.Add(new Claim(AppClaimTypes.RoleName, role.Name));
                claims.Add(new Claim(AppClaimTypes.RoleId, role.Id.ToString()));
                claims.Add(new Claim(AppClaimTypes.RoleType, ((int)role.Type).ToString()));
                claims.Add(new Claim(AppClaimTypes.UserPermissions, Permission.Pack(role.Permissions)));
            }
            else
            {
                claims.Add(new Claim(AppClaimTypes.RoleName, "demo"));
                claims.Add(new Claim(AppClaimTypes.RoleId, "-1"));
                claims.Add(new Claim(AppClaimTypes.UserPermissions, "[]"));
            }

            return new ClaimsIdentity(claims);
        }
    }

    /// <summary>
    /// partial part for <see cref="AppClaimsPrincipalFactory"/>
    /// </summary>
    public partial class AppClaimsPrincipalFactory
    {
        private readonly IRoleDataAccess _roleDataAccess;

        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="dataAccess">the db context</param>
        /// <param name="companyDataAccess"></param>
        /// <param name="roleDataAccess"></param>
        /// <param name="userManager">the user manager</param>
        /// <param name="optionsAccessor">the options accessor</param>
        public AppClaimsPrincipalFactory(
            IRoleDataAccess roleDataAccess,
            UserManager<User> userManager,
            IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, optionsAccessor)
        {
            _roleDataAccess = roleDataAccess;
        }
    }
}
