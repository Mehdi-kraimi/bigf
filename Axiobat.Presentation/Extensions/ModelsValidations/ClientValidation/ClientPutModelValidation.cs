﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using Application.Services.Contacts;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// client validation model
    /// </summary>
    public class ClientPutModelValidation : BaseValidator<ClientPutModel>
    {
        private readonly IClientService _clientService;
        private readonly IGroupService _groupservice;

        /// <summary>
        /// default constructor
        /// </summary>
        public ClientPutModelValidation(ILoggerFactory logger, IClientService clientService, IGroupService groupservice)
            : base(logger)
        {
            _clientService = clientService;
            _groupservice = groupservice;

            RuleFor(e => e.Reference)
                .NotEmpty()
                    .WithErrorCode(MessageCode.ReferenceIsRequired)
                    .WithMessage("client reference is required")
                .NotNull()
                    .WithErrorCode(MessageCode.ReferenceIsRequired)
                    .WithMessage("client reference is required");

            RuleFor(e => e.FirstName)
                .NotEmpty()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("client Name is required")
                .NotNull()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("client Name is required");

            When(e => e.GroupeId.HasValue, () =>
            {
                RuleFor(e => e.GroupeId)
                    .MustAsync(GroupExistAsync)
                        .WithMessage("the specified role not exist")
                        .WithErrorCode(MessageCode.GroupNotExist);
            });

            When(e => e.Email.IsValid(), () =>
            {
                RuleFor(e => e.Email)
                    .IsValidEmail();
            });
        }

        private Task<bool> GroupExistAsync(int? groupId, CancellationToken arg2)
            => _groupservice.IsGroupExistAsync((int)groupId);
    }
}
