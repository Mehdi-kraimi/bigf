﻿namespace Axiobat.Presentation.Models.Validations
{
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    public class OrderDetailsModelValidation : BaseValidator<OrderDetails>
    {
        public OrderDetailsModelValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
        }
    }
}
