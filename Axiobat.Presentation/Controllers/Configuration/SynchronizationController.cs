﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using Domain.Enums;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Synchronization management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class SynchronizationController
    {
        /// <summary>
        /// this endpoint is used to sync the data between the mobile storage and main app storage
        /// </summary>
        /// <param name="synchronizationOption">the Synchronization object</param>
        /// <returns></returns>
        [HttpPost("Sync")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<dynamic>> Sync([FromBody] SynchronizationOption synchronizationOption)
        {
            // check if requesting first synchronization
            if (synchronizationOption.IsFistSynchronization)
                return await ProcessFisrtSynchronizationAsync(synchronizationOption);

            var documentsToSync = synchronizationOption.Objects
                .Where(document => !document.Object.IsEmpty())
                .GroupBy(document => document.DocType);

            // create the result instant
            var result = new LinkedList<dynamic>();

            foreach (var documents in documentsToSync)
            {
                var synchronizationService = GetSynchronizationService((DocumentType)documents.Key);
                if (synchronizationService is null)
                {
                    _logger.LogError("there no synchronization service defined for the document of type [{docType}]", documents.Key);
                    continue;
                }

                result.AddLast(await synchronizationService.SynchronizeAsync(synchronizationOption.LastSyncDate, documents));
            }

            // add read only sync objects
            result.AddLast(await GetSynchronizationService(DocumentType.Configuration).SynchronizeAsync(synchronizationOption.LastSyncDate, null));
            result.AddLast(await GetSynchronizationService(DocumentType.Classification).SynchronizeAsync(synchronizationOption.LastSyncDate, null));

            // return the result
            return Result.ListSuccess(result);
        }
    }

    /// <summary>
    /// partial part for <see cref="SynchronizationController"/>
    /// </summary>
    public partial class SynchronizationController : BaseController
    {
        public SynchronizationController(
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
        }

        private async Task<ListResult<dynamic>> ProcessFisrtSynchronizationAsync(SynchronizationOption synchronizationOption)
        {
            // create the result instant
            var result = new List<dynamic>();

            // loop over the types that support the Synchronization
            foreach (var docType in SynchronizationOption.SynchronizedDocumentTypes)
            {
                var synchronizationService = GetSynchronizationService(docType);
                if (synchronizationService is null)
                    continue;

                var syncResult = await synchronizationService.SynchronizeAsync(synchronizationOption.LastSyncDate, Enumerable.Empty<SyncObject>());
                if (syncResult is null)
                    continue;

                result.Add(syncResult);
            }

            // return the result
            return Result.ListSuccess(result);
        }

        private ISynchronize GetSynchronizationService(DocumentType docType)
        {
            var serviceType = Helper.GetEntityService(Helper.GetEntityType(docType));
            if (serviceType is null)
                return null;

            if (!(HttpContext.RequestServices.GetService(serviceType) is ISynchronize service))
                return null;

            return service;
        }
    }
}