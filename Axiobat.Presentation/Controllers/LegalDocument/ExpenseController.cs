﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Documents;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Expense API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class ExpenseController : BaseController<Expense>
    {
        /// <summary>
        /// get a paged result of the Expenses list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Expenses as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<ExpenseListModel>>> Get([FromBody] DocumentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<ExpenseListModel, DocumentFilterOptions>(filter));

        /// <summary>
        /// retrieve Expense with the given id
        /// </summary>
        /// <param name="ExpenseId">the id of the Expense to be retrieved</param>
        /// <returns>the Expense</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ExpenseModel>>> Get([FromRoute(Name = "id")] string ExpenseId)
            => ActionResultForAsync(_service.GetByIdAsync<ExpenseModel>(ExpenseId));

        /// <summary>
        /// create a new Expense record
        /// </summary>
        /// <param name="ExpenseModel">the model to create the Expense from it</param>
        /// <returns>the newly created Expense</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ExpenseModel>>> Create(
            [FromBody] ExpensePutModel ExpenseModel)
        {
            var result = await _service.CreateAsync<ExpenseModel, ExpensePutModel>(ExpenseModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Expense informations
        /// </summary>
        /// <param name="ExpenseModel">the model to use for updating the Expense</param>
        /// <param name="ExpenseId">the id of the Expense to be updated</param>
        /// <returns>the updated Expense</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ExpenseModel>>> Update(
            [FromBody] ExpensePutModel ExpenseModel,
            [FromRoute(Name = "id")] string ExpenseId)
            => ActionResultForAsync(_service.UpdateAsync<ExpenseModel, ExpensePutModel>(ExpenseId, ExpenseModel));

        /// <summary>
        /// delete the Expense with the given id
        /// </summary>
        /// <param name="ExpenseId">the id of the Expense to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string ExpenseId)
            => ActionResultForAsync(_service.DeleteAsync(ExpenseId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// cancel the document with the given id
        /// </summary>
        /// <param name="expenseId">the id of the expense</param>
        /// <param name="model">the cancellation model</param>
        /// <returns>the attached document</returns>
        [HttpPost("{id}/Cancel")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MemoModel>>> CheckReferenceIfUnique(
            [FromRoute(Name = "id")] string expenseId,
            [FromBody] MemoModel model)
            => ActionResultForAsync(_service.CancelExpenseAsync(expenseId, model));
    }

    public partial class ExpenseController : BaseController<Expense>
    {
        private readonly IExpenseService _service;

        /// <summary>
        /// create an instant of <see cref="ExpenseController"/>
        /// </summary>
        /// <param name="loggedInUserService">the logged in user service</param>
        /// <param name="translationService">the translation service</param>
        /// <param name="loggerFactory">the logger factory</param>
        public ExpenseController(
            IExpenseService expenseService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = expenseService;
        }
    }
}