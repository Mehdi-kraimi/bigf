﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// controller for managing <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [Route("api/[controller]")]
    public partial class MaintenanceOperationSheetController
    {
        /// <summary>
        /// get list of all Maintenance Operation Sheet
        /// </summary>
        /// <returns>list of all Maintenance operation sheets</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<MaintenanceOperationSheetListModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<MaintenanceOperationSheetListModel>());

        /// <summary>
        /// get a paged result of the Maintenance OperationSheets list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Maintenance OperationSheets as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<MaintenanceOperationSheetListModel>>> Get(
            [FromBody] MaintenanceOperationSheetFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<MaintenanceOperationSheetListModel, MaintenanceOperationSheetFilterOptions>(filter));

        /// <summary>
        /// retrieve Maintenance OperationSheet with the given reference
        /// </summary>
        /// <param name="operationSheetReference">the reference of the Maintenance operation sheet to be retrieved</param>
        /// <returns>the Maintenance OperationSheet</returns>
        [HttpGet("Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceOperationSheetModel>>> GetByReference([FromRoute(Name = "reference")] string operationSheetReference)
            => ActionResultForAsync(_service.GetByReferenceAsync<MaintenanceOperationSheetModel>(operationSheetReference));

        /// <summary>
        /// retrieve Maintenance OperationSheet with the given id
        /// </summary>
        /// <param name="operationSheetId">the id of the Maintenance OperationSheet to be retrieved</param>
        /// <returns>the Maintenance OperationSheet</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceOperationSheetModel>>> Get([FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.GetByIdAsync<MaintenanceOperationSheetModel>(operationSheetId));

        /// <summary>
        /// create a new OperationSheet record
        /// </summary>
        /// <param name="OperationSheetModel">the model to create the OperationSheet from it</param>
        /// <returns>the newly created OperationSheet</returns>
        [HttpPost("Create/AfterSalesService")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<MaintenanceOperationSheetModel>>> CreateAfterSalesService(
            [FromBody] AfterSalesServiceOperationSheetCreateModel OperationSheetModel)
        {
            var result = await _service.CreateAsync<MaintenanceOperationSheetModel, AfterSalesServiceOperationSheetCreateModel>(OperationSheetModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// create a new OperationSheet record
        /// </summary>
        /// <param name="OperationSheetModel">the model to create the OperationSheet from it</param>
        /// <returns>the newly created OperationSheet</returns>
        [HttpPost("Create/Maintenance")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<MaintenanceOperationSheetModel>>> CreateMaintenance(
            [FromBody] MaintenanceOperationSheetCreateModel OperationSheetModel)
        {
            var result = await _service.CreateAsync<MaintenanceOperationSheetModel, MaintenanceOperationSheetCreateModel>(OperationSheetModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the OperationSheet informations
        /// </summary>
        /// <param name="OperationSheetModel">the model to use for updating the OperationSheet</param>
        /// <param name="OperationSheetId">the id of the OperationSheet to be updated</param>
        /// <returns>the updated OperationSheet</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceOperationSheetModel>>> Update([FromBody] MaintenanceOperationSheetUpdateModel OperationSheetModel, [FromRoute(Name = "id")] string OperationSheetId)
            => ActionResultForAsync(_service.UpdateAsync<MaintenanceOperationSheetModel, MaintenanceOperationSheetUpdateModel>(OperationSheetId, OperationSheetModel));

        /// <summary>
        /// delete the OperationSheet with the given id
        /// </summary>
        /// <param name="OperationSheetId">the id of the OperationSheet to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string OperationSheetId)
            => ActionResultForAsync(_service.DeleteAsync(OperationSheetId));

        /// <summary>
        /// send the operationSheet with the given id via an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to be sent</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> EmailDocument([FromRoute(Name = "id")] string operationSheetId, [FromBody] SendEmailOptions emailOptions)
            => ActionResultForAsync(_service.SendAsync(operationSheetId, emailOptions));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// update the Operation Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet status</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateStatus([FromBody] DocumentUpdateStatusModel model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateStatusAsync(operationSheetId, model));

        /// <summary>
        /// update the Operation Sheet status to UnDone
        /// </summary>
        /// <param name="model">the motif for setting the status to unDone</param>
        /// <param name="operationSheetId">the id of the Operation Sheet</param>
        /// <returns>the Operation result</returns>
        [HttpPut("{id}/Update/Status/UnDone")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> SetStatusToUndone(
            [FromRoute(Name = "id")] string operationSheetId,
            [FromBody] MaintenanceOperationSheetMotif model)
            => ActionResultForAsync(_service.SetStatusToUndoneAsync(operationSheetId, model));

        /// <summary>
        /// update the signature Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet signature</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Signature")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateSignature([FromBody] DocumentUpdateSignatureModel model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateSignatureAsync(operationSheetId, model));

        /// <summary>
        /// export the operation sheet with the given id
        /// </summary>
        /// <param name="OperationSheetID">the id of the operation sheet</param>
        /// <param name="exportOptions">the operation sheet</param>
        /// <returns>generate file as byte array</returns>
        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string OperationSheetID, [FromBody] DataExportOptions exportOptions)
            => ActionResultForAsync(_service.ExportDataAsync(OperationSheetID, exportOptions));
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceOperationSheetController"/>
    /// </summary>
    public partial class MaintenanceOperationSheetController : BaseController<MaintenanceOperationSheet>
    {
        private readonly IMaintenanceOperationSheetService _service;

        public MaintenanceOperationSheetController(
            IMaintenanceOperationSheetService maintenanceOperationSheetService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = maintenanceOperationSheetService;
        }
    }
}