﻿namespace Axiobat.Presentation.BackgroundServices
{
    using Application.Services.Configuration;
    using Coravel.Invocable;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the base Background Service
    /// </summary>
    public abstract class BaseBackgroundService : IInvocable
    {
        /// <summary>
        /// the services provider
        /// </summary>
        protected readonly IServiceProvider _services;

        /// <summary>
        /// create an instant of <see cref="BaseBackgroundService"/>
        /// </summary>
        /// <param name="services">the service provider</param>
        protected BaseBackgroundService(IServiceProvider services)
        {
            _services = services;
        }

        /// <summary>
        /// invoice the background service logic, this function will only call <see cref="ExecuteAsync"/>
        /// so make sure all your logic is set in that function
        /// </summary>
        public Task Invoke() => ExecuteAsync();

        /// <summary>
        /// Execute the back ground task
        /// </summary>
        public void Execute() => ExecuteAsync().GetAwaiter().GetResult();

        /// <summary>
        /// Execute the back ground task
        /// </summary>
        public async Task ExecuteAsync()
        {
            using (var scope = _services.CreateScope())
            {
                var applicationSettings = scope.GetService<IApplicationSettingsAccessor>();
                if (!applicationSettings.BackgroundServicesAllowed())
                {
                    return;
                }

                await RunAsync(scope);
            }
        }

        /// <summary>
        /// the function that will be executed
        /// </summary>
        protected abstract Task RunAsync(IServiceScope scope);
    }
}
