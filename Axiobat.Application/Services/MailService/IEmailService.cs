﻿namespace Axiobat.Application.Services.MailService
{
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the base interface for all Mail services
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// use this method to send an email
        /// </summary>
        /// <param name="sendEmailModel">the send email model</param>
        /// <returns>the operation result</returns>
        Task<Result> SendEmailAsync(SendEmailOptions mail);

        /// <summary>
        /// this method is used to send a rest password email,
        /// and also it responsible for building the URL
        /// </summary>
        /// <param name="user">the user to send the rest password email to</param>
        /// <param name="token">the rest password token</param>
        /// <returns>the operation result</returns>
        Task<Result> SendRestPasswordEmailAsync(User user, string token);

        /// <summary>
        /// send an email to user to confirm his email
        /// </summary>
        /// <param name="user">the user to send the email to</param>
        /// <param name="companyType">type of the company</param>
        /// <param name="token">the confirmation token</param>
        /// <returns>the operation result</returns>
        Task<Result> SendConfirmEmailUrlAsync(User user, string token);
    }
}
