﻿namespace Axiobat.Application.Services.Products
{
    using Models;
    using Configuration;
    using Domain.Entities;
    using FileService;
    using System.Threading.Tasks;

    /// <summary>
    /// the <see cref="Product"/> service
    /// </summary>
    public interface IProductService : IProductBaseService<Product>, IMemoService, IReferenceService
    {
        /// <summary>
        /// get the list of products of the supplier
        /// </summary>
        /// <typeparam name="TOut">the out put type</typeparam>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of products</returns>
        Task<ListResult<TOut>> GetSupplierProductsAsync<TOut>(string supplierId);

        /// <summary>
        /// check if the given category type id belongs to an existing category
        /// </summary>
        /// <param name="categoryTypeId">the id of the category to check</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsProductCategoryTypeExistAsync(string categoryTypeId);

        Task<TOut[]> GetProductCategoriesTypeAsync<TOut>();
    }
}
