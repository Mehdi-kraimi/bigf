﻿namespace Axiobat.Application.Services.Contacts
{
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Group"/>
    /// </summary>
    public interface IGroupService : IDataService<Group, int>
    {
        /// <summary>
        /// check if the group with the given id exist
        /// </summary>
        /// <param name="groupId">the id of the group</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsGroupExistAsync(int groupId);

        /// <summary>
        /// check if the group name is unique or not
        /// </summary>
        /// <param name="groupName">name of the group to be checked</param>
        /// <returns>true if unique, false if not</returns>
        Task<bool> IsUniqueGroupNameAsync(string groupName);
    }
}
