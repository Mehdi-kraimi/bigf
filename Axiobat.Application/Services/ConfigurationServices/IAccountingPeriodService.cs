﻿namespace Axiobat.Application.Services.Configuration
{
    using Domain.Entities;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// service for managing accounting periods
    /// </summary>
    public interface IAccountingPeriodService : IDataService<AccountingPeriod, int>
    {
        /// <summary>
        /// get the current accounting period
        /// </summary>
        /// <returns>the starting date of the period, and the ending of the period</returns>
        Task<TOut> GetCurrentAccountingPeriodAsync<TOut>();

        /// <summary>
        /// get the accounting Period that will end today
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <returns>the accounting period</returns>
        Task<TOut> GetAccountingPeriodEndsTodayAsync<TOut>();

        /// <summary>
        /// close the accounting period with the given id
        /// </summary>
        /// <param name="accountingPeriodId">the id of the accounting period</param>
        Task CloseAccountingPeriodAsync(int accountingPeriodId);

        /// <summary>
        /// this function will check if the given date is in this accounting period
        /// </summary>
        /// <param name="date">the date to be checked</param>
        /// <returns>true if in the current accounting period, false if not</returns>
        Task<bool> IsInCurrentAccountingPeriod(DateTime date);

        /// <summary>
        /// close the current accounting period
        /// </summary>
        Task CloseCurrentAccountingPeriodAsync();
    }
}
