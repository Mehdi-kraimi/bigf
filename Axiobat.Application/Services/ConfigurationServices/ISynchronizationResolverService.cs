﻿namespace Axiobat.Application.Services.Configuration
{
    using Axiobat.Domain.Interfaces;

    /// <summary>
    /// a service for resolving synchronization
    /// </summary>
    public interface ISynchronizationResolverService
    {
        /// <summary>
        /// this function is meant to resolve the conflict between two entities, it will compare the two entries looking for changes,
        /// and use the changes history list to get the information about the newest changes on a property, and tack the newest value
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <param name="databaseEntity">the entity in the database</param>
        /// <param name="synchronizeWith">the entity that we synchronize with it</param>
        /// <returns>if there is any changes to the entity true will be returned, otherwise false</returns>
        bool Resolve<TEntity>(TEntity databaseEntity, TEntity synchronizeWith)
             where TEntity : IRecordable;
    }
}
