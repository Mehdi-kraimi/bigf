﻿namespace Axiobat.Application.Services.Configuration
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// service for managing RecurringDocuments
    /// </summary>
    public interface IRecurringDocumentService : IDataService<RecurringDocument, string>, ISynchronize
    {
        /// <summary>
        /// update status of the recurring with the given id
        /// </summary>
        /// <param name="recurringId">the id of the recurring to be updated</param>
        /// <param name="model">the model</param>
        Task UpdateStatusAsync(string recurringId, DocumentUpdateStatusModel model);

        /// <summary>
        /// update the recurring document info with given info
        /// </summary>
        /// <typeparam name="TDocument">the type of the document</typeparam>
        /// <param name="recurringId">the id of the recurring to update</param>
        /// <param name="document">the document instant</param>
        Task UpdateRecurringDocumentInfoAsync<TDocument>(string recurringId, TDocument document);
    }
}
