﻿namespace Axiobat.Application.Services.Configuration
{
    using System.Threading.Tasks;

    /// <summary>
    /// a service for all IReferenceable entities
    /// </summary>
    public interface IReferenceService
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        Task<bool> IsRefrenceUniqueAsync(string reference);
    }
}
