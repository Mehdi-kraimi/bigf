﻿namespace Axiobat.Application.Services
{
    using Data;
    using Domain.Interfaces;
    using FileService;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the base interface for all DataServices
    /// </summary>
    public interface IDataService<TEntity, TKey> : IBaseDataService, IDataExportService<TKey>
        where TEntity : class, IEntity<TKey>
    {
        /// <summary>
        /// this method is used to create a recode for of the entity
        /// </summary>
        /// <typeparam name="TOut">the type of the out put, useful if you want to return a deferent type than the model</typeparam>
        /// <param name="createModel">the create model</param>
        /// <returns>a result operation, containing the newly created entity</returns>
        Task<Result<TOut>> CreateAsync<TOut, TCreateModel>(TCreateModel createModel) where TCreateModel : class;

        /// <summary>
        /// this method is used to update the entity white the given id, using the update model
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="id">the id of the entity to be updated</param>
        /// <param name="updateModel">the create model</param>
        /// <returns>the updated version of the entity</returns>
        Task<Result<TOut>> UpdateAsync<TOut, TUpdateModel>(TKey id, TUpdateModel updateModel) where TUpdateModel : class, IUpdateModel<TEntity>;

        /// <summary>
        /// delete the entity with the given id
        /// </summary>
        /// <param name="id">the id of the entity to be deleted</param>
        /// <returns>the deletion operation result</returns>
        Task<Result> DeleteAsync(TKey id);

        /// <summary>
        /// get the entity with the given id
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="id">the id of the entity to retrieve</param>
        /// <returns>desired output</returns>
        Task<Result<TOut>> GetByIdAsync<TOut>(TKey id);

        /// <summary>
        /// get the list of all the entities
        /// </summary>
        /// <typeparam name="TOut">output type</typeparam>
        /// <returns>desired output</returns>
        Task<ListResult<TOut>> GetAllAsync<TOut>();

        /// <summary>
        /// get list of all entities by the given identifiers
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="identifiers">list of identifiers</param>
        /// <returns>list of entities</returns>
        Task<TOut[]> GetAllAsync<TOut>(params TKey[] identifiers);

        /// <summary>
        /// get the list of the entities as paged result
        /// </summary>
        /// <typeparam name="TOut">output type</typeparam>
        /// <param name="filterOption">the pagination options</param>
        /// <returns>desired output</returns>
        Task<PagedResult<TOut>> GetAsPagedResultAsync<TOut, TFilter>(TFilter filterOptions) where TFilter : IFilterOptions;

        /// <summary>
        /// check if the entity with the given id exist or not
        /// </summary>
        /// <param name="entityId">the id of the entity</param>
        /// <returns>true if exist false if not</returns>
        Task<bool> IsExistAsync(TKey entityId);
    }
}
