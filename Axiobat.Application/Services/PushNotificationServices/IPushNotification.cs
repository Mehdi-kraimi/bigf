﻿namespace Axiobat.Application.Services.PushNotification
{
    using App.Common;
    using Application.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the push notification service
    /// </summary>
    public interface IPushNotificationService
    {
        /// <summary>
        /// send a push notification to list of users, using the given template
        /// </summary>
        /// <param name="usersIds">the ids of the users to send the messages to them</param>
        /// <param name="messageTemplteId">the id of the template to be used</param>
        /// <param name="placeHolders">the place holders to set for the message template if any</param>
        /// <returns>the operation result</returns>
        Task SendNotificationAsync(IEnumerable<string> usersIds, string messageTemplteId, params PlaceHolder[] placeHolders);

        /// <summary>
        /// send a push notification to a specific user, using the given template
        /// </summary>
        /// <param name="usersId">the id of the user to send the messages to it</param>
        /// <param name="messageTemplteId">the id of the template to be used</param>
        /// <param name="placeHolders">the place holders to set for the message template if any</param>
        /// <returns>the operation result</returns>
        Task SendNotificationAsync(string userId, string messageTemplate, params PlaceHolder[] placeHolders);
    }
}