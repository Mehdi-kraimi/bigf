﻿namespace Axiobat.Application.Services.FileService
{
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// this interface defines the data export operation
    /// </summary>
    public interface IDataExportService<TKey>
    {
        /// <summary>
        /// export data using the given export options
        /// </summary>
        /// <typeparam name="TExportOption">the type of the export options</typeparam>
        /// <param name="exportOptions">the export options</param>
        /// <returns>the file as a byte[]</returns>
        Task<Result<byte[]>> ExportDataAsync<TExportOption>(TExportOption exportOptions)
            where TExportOption : DataExportOptions;

        /// <summary>
        /// export data of the entity with the given id
        /// </summary>
        /// <typeparam name="TExportOption">the type of the export options</typeparam>
        /// <typeparam name="Tkey">the type of the entity key</typeparam>
        /// <param name="entityId">the id of the entity to export</param>
        /// <param name="exportOptions">the export options</param>
        /// <returns>the file as a byte[]</returns>
        Task<Result<byte[]>> ExportDataAsync<TExportOption>(TKey entityId, TExportOption exportOptions)
            where TExportOption : DataExportOptions;
    }
}
