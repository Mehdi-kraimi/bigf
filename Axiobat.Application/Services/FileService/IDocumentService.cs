﻿namespace Axiobat.Application.Services.FileService
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public interface IDocumentService
    {
        /// <summary>
        /// get the tags from the file
        /// </summary>
        /// <param name="stream">the stream</param>
        /// <returns>the list of tags</returns>
        string[] GetTags(Stream stream);

        /// <summary>
        /// remove file with the given name
        /// </summary>
        /// <param name="fileFullName">the full name of file (with the path)</param>
        void RemoveFile(string fileFullName);

        /// <summary>
        /// save the file form the given stream
        /// </summary>
        /// <param name="fileStream">the file stream</param>
        /// <param name="fileFullName">the full name of file (with the path)</param>
        Task Async(Stream fileStream, string fileFullName);

        /// <summary>
        /// get the file as a byte array from the given file path
        /// </summary>
        /// <param name="filePath">the path of the file</param>
        /// <param name="tags">the list of tags to be updated in the document</param>
        /// <returns>the file as a byte array</returns>
        Task<byte[]> GetFileAsync(string filePath, IDictionary<string, string> tags);
    }
}
