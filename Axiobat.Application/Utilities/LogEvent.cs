﻿namespace Axiobat
{
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// list of log events
    /// </summary>
    public static class LogEvent
    {
        public static readonly EventId DataNotFound = new EventId(1001, "DataNotFound");
        public static readonly EventId deletingFile = new EventId(1002, "deletingFile");
        public static readonly EventId FileSaving = new EventId(1003, "FileSaving");
        public static readonly EventId RetrieveFile = new EventId(1004, "RetrieveFile");
        public static readonly EventId InsertARecored = new EventId(1005, "InsertARecored");
        public static readonly EventId UpdatingData = new EventId(1006, "UpdatingData");
        public static readonly EventId UpdateRangeOfRecoreds = new EventId(1007, "UpdateRangeOfRecoreds");
        public static readonly EventId RetrievingData = new EventId(1008, "RetrievingData");
        public static readonly EventId SendingConfiramtionEmail = new EventId(1009, "SendingConfiramtionEmail");
        public static readonly EventId ConfirmEmail = new EventId(1010, "ConfirmEmail");
        public static readonly EventId ForgetPassword = new EventId(1011, "ForgetPassword");
        public static readonly EventId UpdatePassword = new EventId(1012, "UpdatePassword");
        public static readonly EventId DeleteARecored = new EventId(1013, "DeleteARecored");
        public static readonly EventId UpdateRolePermissions = new EventId(1014, "UpdateRolePermissions");
        public static readonly EventId CreateARecord = new EventId(1015, "CreateARecord");
        public static readonly EventId ExportData = new EventId(1016, "ExportData");
        public static readonly EventId SavingMemo = new EventId(1017, "SavingMemo");
        public static readonly EventId DeleteMemos = new EventId(1018, "DeleteMemos");
        public static readonly EventId HandleSocketLabsWebHook = new EventId(1019, "HandleSocketLabsWebHook");
        public static readonly EventId SendingEmail = new EventId(1020, "SendingEmail");
        public static readonly EventId SendingRestPasswordEmail = new EventId(1021, "SendingRestPasswordEmail");
        public static readonly EventId SendTemplateEmail = new EventId(1022, "SendTemplateEmail");
        public static readonly EventId InsertARangeOfRecoreds = new EventId(1023, "InsertARangeOfRecoreds");
        public static readonly EventId DeleteARangeOfRecoreds = new EventId(1024, "DeleteARangeOfRecoreds");
        public static readonly EventId CheckingExistence = new EventId(1025, "CheckingExistence");
        public static readonly EventId RetrievePagedListOfData = new EventId(1026, "RetrievePagedListOfData");
        public static readonly EventId ValidateUserLogin = new EventId(1027, "ValidateUserLogin");
        public static readonly EventId ModelValidation = new EventId(1028, "ModelValidation");
        public static readonly EventId SendPushNotification = new EventId(1029, "SendPushNotification");
        public static readonly EventId AxiobatpublicException = new EventId(1030, "AxiobatpublicException");

    }
}
