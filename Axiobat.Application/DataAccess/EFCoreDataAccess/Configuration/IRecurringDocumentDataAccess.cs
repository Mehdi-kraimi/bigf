﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the <see cref="RecurringDocument"/> DataAccess
    /// </summary>
    public interface IRecurringDocumentDataAccess : IDataAccess<RecurringDocument, string>
    {

    }
}
