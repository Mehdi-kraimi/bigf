﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the <see cref="Classification"/> DataAccess
    /// </summary>
    public interface IClassificationDataAccess : IDataAccess<Classification, int>
    {

    }
}
