﻿namespace Axiobat.Application.Data
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;
    using Axiobat.Domain.Enums;

    /// <summary>
    /// the accounting data access
    /// </summary>
    public interface IAccountingDataAccess
    {
        /// <summary>
        /// get the workshop with the given id for financial summary report, this will
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop</param>
        /// <returns>the workshop model</returns>
        Task<ConstructionWorkshop> GetWorkshopsByIdAsync(string constructionWorkshopId);

        /// <summary>
        /// get the list of payments
        /// </summary>
        /// <param name="filterOptions">the date filter options</param>
        /// <param name="accountType">the type of the journal</param>
        /// <returns>the list of payment</returns>
        Task<PagedResult<Payment>> GetPaymentsAsync(DateRangeFilterOptions filterOptions, FinancialAccountsType accountType);

        /// <summary>
        /// get the list of expenses
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of expenses</returns>
        Task<PagedResult<Expense>> GetExpensesAsync(DateRangeFilterOptions filterOptions);

        /// <summary>
        /// get the list of invoices
        /// </summary>
        /// <param name="filterOptions">the list of invoices</param>
        /// <returns>the list of invoices</returns>
        Task<PagedResult<Invoice>> GetInvoicesAsync(JournalFilterOptions filterOptions);

        /// <summary>
        /// get the list of credit notes
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of credit notes</returns>
        Task<PagedResult<CreditNote>> GetCreditNoteAsync(JournalFilterOptions filterOptions);
    }
}
