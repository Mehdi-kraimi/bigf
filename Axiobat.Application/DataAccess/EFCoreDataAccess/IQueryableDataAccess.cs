﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Linq;

    /// <summary>
    /// give access to the IIQueryable instant of the entity for easy access of data
    /// </summary>
    /// <typeparam name="TEntity">the entity type</typeparam>
    /// <typeparam name="Tkey">the entity Key type</typeparam>
    public interface IQueryableDataAccess
    {
        /// <summary>
        /// return an <see cref="IQueryable{T}"/> collection of <see cref="TEntity"/>
        /// </summary>
        IQueryable<TEntity> Entities<TEntity>() where TEntity : Entity;
    }
}
