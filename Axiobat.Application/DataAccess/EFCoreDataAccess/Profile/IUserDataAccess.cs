﻿namespace Axiobat.Application.Data
{
    using Domain.Enums;
    using Domain.Entities;
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="User"/>
    /// </summary>
    public interface IUserDataAccess :
        IDataAccess<User, Guid>,
        IUserStore<User>,
        IUserLoginStore<User>,
        IUserPasswordStore<User>,
        IUserSecurityStampStore<User>,
        IUserEmailStore<User>,
        IUserLockoutStore<User>,
        IUserPhoneNumberStore<User>,
        IUserTwoFactorStore<User>,
        IUserAuthenticationTokenStore<User>,
        IUserAuthenticatorKeyStore<User>,
        IUserTwoFactorRecoveryCodeStore<User>
    {
        /// <summary>
        /// get the list of users with the given role types
        /// </summary>
        /// <param name="roleTypes">the list of roles types</param>
        /// <returns>the list result</returns>
        Task<IEnumerable<User>> GetByRoleTypesAsync(IEnumerable<RoleType> roleTypes);

        /// <summary>
        /// update the time the user last time logged in info
        /// </summary>
        /// <param name="id">the id of the user</param>
        /// <param name="dateLastLoggedIn">the date to set for the user last time logged in</param>
        Task UpdateLastTimeLoggedInAsync(Guid id, DateTime dateLastLoggedIn);

        /// <summary>
        /// get the admin user
        /// </summary>
        /// <returns>get he admin user</returns>
        Task<User> GetAdminUserAsync();

        /// <summary>
        /// get the user by userName
        /// </summary>
        /// <param name="NormalizedEmail">the email value, should be normalized</param>
        /// <returns>the user instant or null if not exist</returns>
        Task<User> GetByEmailAsync(string NormalizedEmail);

        /// <summary>
        /// get the user by user name
        /// </summary>
        /// <param name="NormalizedUserName">the user name normalized</param>
        /// <returns>the user instant</returns>
        Task<User> GetByUserNameAsync(string NormalizedUserName);
    }
}
