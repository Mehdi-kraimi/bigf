﻿namespace Axiobat.Application.Data
{
    using Models;
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Invoice"/>
    /// </summary>
    public interface IInvoiceDataAccess : IDocumentDataAccess<Invoice>
    {
        /// <summary>
        /// the hold back details
        /// </summary>
        /// <param name="workshopId">the workshop id</param>
        /// <returns>the list of <see cref="HoldbackDetailsModel"/></returns>
        Task<IEnumerable<Invoice>> GetHoldbackDetailsModelAsync(string workshopId);
    }
}
