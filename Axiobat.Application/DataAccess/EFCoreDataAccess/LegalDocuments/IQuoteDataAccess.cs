﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="Quote"/>
    /// </summary>
    public interface IQuoteDataAccess : IDocumentDataAccess<Quote>
    {

    }
}
