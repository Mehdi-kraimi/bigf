﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the dataAccess for <see cref="SupplierOrder"/>
    /// </summary>
    public interface ISupplierOrderDataAccess : IDocumentDataAccess<SupplierOrder>
    {
        /// <summary>
        /// update the status of the given suppliers orders
        /// </summary>
        /// <param name="status">the new status</param>
        /// <param name="supplierOrdersIds">the id of the suppliers</param>
        /// <returns>the affected lines</returns>
        Task<int> UpdateStatusAsync(string status, string[] supplierOrdersIds);

        /// <summary>
        /// get the list of supplier orders
        /// </summary>
        /// <param name="workshopId"></param>
        /// <param name="supplierId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<IEnumerable<SupplierOrder>> GetByWorkshopIdSupplierIdStatusAsync(string workshopId, string supplierId, string[] status);
    }
}
