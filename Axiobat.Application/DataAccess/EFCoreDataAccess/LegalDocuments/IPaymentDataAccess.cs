﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the dataAccess for <see cref="Payment"/>
    /// </summary>
    public interface IPaymentDataAccess : IDataAccess<Payment, string>
    {
        /// <summary>
        /// get all payments of type expense and invoice
        /// </summary>
        /// <returns>the list of payments</returns>
        Task<IEnumerable<Payment>> GetAllInvoiceExpensePaymentsAsync();

        /// <summary>
        /// remove the list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        Task RemovePaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities);

        /// <summary>
        /// add the list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        Task AddPaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities);

        /// <summary>
        /// update list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        Task UpdatePaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities);

        /// <summary>
        /// remove all payments of the expenses
        /// </summary>
        /// <param name="entities">the list of entities to be removed</param>
        Task RemoveExpensePaymentAsync(IEnumerable<Expenses_Payments> entities);

        /// <summary>
        /// add the list of payment expenses
        /// </summary>
        /// <param name="entities">the list of payment expenses</param>
        Task AddExpensePaymentAsync(IEnumerable<Expenses_Payments> entities);

        /// <summary>
        /// update list of payment expenses
        /// </summary>
        /// <param name="entities">the list of payment expenses</param>
        Task UpdateExpensePaymentAsync(IEnumerable<Expenses_Payments> entities);
    }
}
