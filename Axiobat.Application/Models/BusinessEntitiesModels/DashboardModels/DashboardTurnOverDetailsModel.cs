﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the model of the dashboard turnOver details
    /// </summary>
    public class DashboardTurnOverDetailsModel
    {
        public DashboardTurnOverDetailsModel()
        {
            CurrentPeriod = new LinkedList<DashboardPerMonthTurnOverDetailsModel>();
            PreCurrentPeriod = new LinkedList<DashboardPerMonthTurnOverDetailsModel>();
        }

        public ICollection<DashboardPerMonthTurnOverDetailsModel> CurrentPeriod { get; set; }

        public ICollection<DashboardPerMonthTurnOverDetailsModel> PreCurrentPeriod { get; set; }
    }

    public class DashboardPerMonthTurnOverDetailsModel : PerMonthTotalDetails
    {
        public DashboardPerMonthTurnOverDetailsModel(int year, int month, float total, float totalPrediction)
            : base(year, month, total)
        {
            TotalPrediction = totalPrediction;
        }

        /// <summary>
        /// the prediction of the Month
        /// </summary>
        public float TotalPrediction { get; }
    }
}
