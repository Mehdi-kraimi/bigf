﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the model of the dashboard Tops details
    /// </summary>
    public class DashboardTopsDetailsModel
    {
        /// <summary>
        /// the list of the top workshops by their turnOver
        /// </summary>
        public IEnumerable<MinimalWorkshopInfo> TopWorkshopsByTurnover { get; set; }

        /// <summary>
        /// the list of the top workshops by the total working hours
        /// </summary>
        public IEnumerable<MinimalWorkshopInfo> TopWorkshopsByWorkingHours { get; set; }

        /// <summary>
        /// the list of the top Technicians by the total working hours
        /// </summary>
        public IEnumerable<TechniciansWorkingHoursDetails> TopTechniciansByWorkingHours { get; set; }

        /// <summary>
        /// the list of the top clients
        /// </summary>
        public IEnumerable<ExternalPartnerTransactionDetails> TopClients { get; set; }

        /// <summary>
        /// the list of the top Products by the total Sales
        /// </summary>
        public IEnumerable<ProductAnalyticsResult> TopProductsByTotalSales { get; set; }

        /// <summary>
        /// list of top labels by the total Sales
        /// </summary>
        public IEnumerable<LabelAnalyticsResult> TopLabelsByTotalSales { get; set; }

        /// <summary>
        /// list of top Suppliers
        /// </summary>
        public IEnumerable<SuppliersAnalyticsResult> TopSuppliers { get; set; }

        /// <summary>
        /// list of top Products by the QTE
        /// </summary>
        public IEnumerable<ProductAnalyticsResult> TopProductsByQuantity { get; set; }

        /// <summary>
        /// list of top products by suppliers price
        /// </summary>
        public IEnumerable<ProductAnalyticsResult> TopProductsBySupplierPrice { get; set; }
    }
}
