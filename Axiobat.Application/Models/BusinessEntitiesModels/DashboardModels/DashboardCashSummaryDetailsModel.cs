﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// this class defines the model of the dashboard CashSummary details
    /// </summary>
    public class DashboardCashSummaryDetailsModel
    {
        /// <summary>
        /// the total payments of Invoices
        /// </summary>
        public float TotalInvoicePayment { get; set; }

        /// <summary>
        /// the total left to be paid for invoices
        /// </summary>
        public float TotalInvoiceLeftToPay { get; set; }

        /// <summary>
        /// the total payments of expenses
        /// </summary>
        public float TotalExpensePayment { get; set; }

        /// <summary>
        /// the total left to be paid for Expenses
        /// </summary>
        public float TotalExpenseLeftToPay { get; set; }

        /// <summary>
        /// the margin of total payment which is <see cref="TotalInvoicePayment"/> - <see cref="TotalExpensePayment"/>
        /// </summary>
        public float MarginTotalPayments => TotalInvoicePayment - TotalExpensePayment;

        /// <summary>
        /// the margin of total left to be paid which is <see cref="TotalInvoiceLeftToPay"/> - <see cref="TotalExpenseLeftToPay"/>
        /// </summary>
        public float MarginTotalLeftToPay => TotalInvoiceLeftToPay - TotalExpenseLeftToPay;
    }
}
