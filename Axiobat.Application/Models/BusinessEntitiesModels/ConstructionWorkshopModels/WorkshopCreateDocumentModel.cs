﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the model used to add a document to the workshop
    /// </summary>
    [ModelFor(typeof(WorkshopDocument))]
    public partial class WorkshopDocumentModel
    {
        /// <summary>
        /// the id of the <see cref="WorkshopDocument"/> entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the comment associated with the Workshop Documentation
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the documentation designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// the user that has added this memo
        /// </summary>
        public MinimalUser User { get; set; }

        /// <summary>
        /// the id of the rubric this workshop is associated with it
        /// </summary>
        public WorkshopDocumentRubricModel Rubric { get; set; }

        /// <summary>
        /// the document types
        /// </summary>
        public ICollection<WorkshopDocumentTypeModel> Types { get; set; }

        /// <summary>
        /// the list of attachment
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }
    }

    /// <summary>
    /// the model used to add a document to the workshop
    /// </summary>
    [ModelFor(typeof(WorkshopDocument))]
    public partial class WorkshopDocumentPutModel : IUpdateModel<WorkshopDocument>
    {
        /// <summary>
        /// the comment associated with the Workshop Documentation
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the documentation designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// the id of the rubric this workshop is associated with it
        /// </summary>
        public string RubricId { get; set; }

        /// <summary>
        /// the list of types this document owns
        /// </summary>
        public ICollection<WorkshopDocumentTypeModel> Types { get; set; }

        /// <summary>
        /// the list of attachment
        /// </summary>
        public ICollection<AttachmentModel> Attachments { get; set; }

        /// <summary>
        /// update the entity for the model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(WorkshopDocument entity)
        {
            entity.Comment = Comment;
            entity.Designation = Designation;
            entity.RubricId = RubricId;
        }
    }
}
