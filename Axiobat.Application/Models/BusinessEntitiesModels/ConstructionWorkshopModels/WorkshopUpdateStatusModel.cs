﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;

    /// <summary>
    /// a model used to update the status of a Workshop
    /// </summary>
    public partial class WorkshopUpdateStatusModel
    {
        /// <summary>
        /// the status value to be affected to a workshop
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"status: {Status}";
    }
}
