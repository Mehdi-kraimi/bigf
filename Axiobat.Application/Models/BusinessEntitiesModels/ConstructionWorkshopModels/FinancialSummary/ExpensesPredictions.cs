﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the expense Predictions
    /// </summary>
    public partial class ExpensesPredictions
    {
        /// <summary>
        /// the total of the Expenses
        /// </summary>
        public float Total => TotalMaterialPurchases + TotalWorkforcePurchases + SubContracting;

        /// <summary>
        /// the total material purchases
        /// </summary>
        public float TotalMaterialPurchases { get; set; }

        /// <summary>
        /// the total purchases of the workforce
        /// </summary>
        public float TotalWorkforcePurchases { get; set; }

        /// <summary>
        /// the total of the Sub-contracting
        /// </summary>
        public float SubContracting { get; set; }

        /// <summary>
        /// total Workforce Hours
        /// </summary>
        public int TotalWorkforceHours { get; set; }

        /// <summary>
        /// Sub Contracting total Hours
        /// </summary>
        public int SubContractingHours { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"total: {Total}";
    }
}
