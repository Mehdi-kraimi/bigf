﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    [ModelFor(typeof(Mission))]
    public partial class MissionPutModel : IUpdateModel<Mission>
    {
        /// <summary>
        /// the title of the of the task
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// the kind of the mission one of <see cref="Constants.MissionKind"/>
        /// </summary>
        public string MissionKind { get; set; }

        /// <summary>
        /// the type of the mission
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the duration of the mission, saved as HH:MM
        /// </summary>
        public string Duration { get; set; }

        /// <summary>
        /// Object of the task
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// the phone number of client
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Type the status of the task
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Technician worked all day
        /// </summary>
        public bool AllDayLong { get; set; }

        /// <summary>
        /// the starting date of the task
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the end date of the task
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        ///  the id of the Client that this ETask belongs to it
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the technicien
        /// </summary>
        public Guid TechnicianId { get; set; }

        /// <summary>
        /// the id of the workshop associated with this mission
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// list of Additional Information associated with this mission
        /// </summary>
        public ICollection<JObject> AdditionalInfo { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MissionPutModel"/>
    /// </summary>
    public partial  class MissionPutModel
    {
        /// <summary>
        /// update the given entity from the model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(Mission entity)
        {
            entity.Type = Type;
            entity.Title = Title;
            entity.PhoneNumber = PhoneNumber;
            entity.Object = Object;
            entity.Status = Status;
            entity.ClientId = ClientId;
            entity.EndingDate = EndingDate;
            entity.AllDayLong = AllDayLong;
            entity.MissionKind = MissionKind;
            entity.StartingDate = StartingDate;
            entity.TechnicianId = TechnicianId;
            entity.AdditionalInfo = AdditionalInfo;
        }
    }
}
