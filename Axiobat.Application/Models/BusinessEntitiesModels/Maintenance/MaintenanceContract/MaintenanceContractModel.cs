﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="MaintenanceContract"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceContract))]
    public partial class MaintenanceContractModel
    {
        /// <summary>
        /// the id of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the reference for the entity
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the contract
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the site of the maintenance
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// whether to enable the Enable Auto Renewal or not
        /// </summary>
        public bool EnableAutoRenewal { get; set; }

        /// <summary>
        /// whether the expiration alert has been enabled or not
        /// </summary>
        public bool ExpirationAlertEnabled { get; set; }

        /// <summary>
        /// the expiration alert period
        /// </summary>
        public int ExpirationAlertPeriod { get; set; }

        /// <summary>
        /// the type of the expiration alert period
        /// </summary>
        public PeriodType ExpirationAlertPeriodType { get; set; }

        /// <summary>
        /// the id of the Recurring associated with this contract
        /// </summary>
        public string RecurringDocumentId { get; set; }

        /// <summary>
        /// the client associated with this contract
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of PublishingContract associated with this contract
        /// </summary>
        public ICollection<PublishingContractMinimal> PublishingContracts { get; set; }

        /// <summary>
        /// list of Equipment Maintenance being maintained by this contract argument
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentMaintenance { get; set; }

        /// <summary>
        /// list of associated documents
        /// </summary>
        public ICollection<AssociatedDocument> AssociatedDocuments { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="MaintenanceContractModel"/>
    /// </summary>
    public partial class MaintenanceContractModel : IModel<MaintenanceContract>
    {
        /// <summary>
        /// partial part for <see cref="MaintenanceContractModel"/>
        /// </summary>
        public MaintenanceContractModel()
        {
            Memos = new HashSet<Memo>();
            Attachments = new List<Attachment>();
            ChangesHistory = new List<ChangesHistory>();
            AssociatedDocuments = new HashSet<AssociatedDocument>();
            PublishingContracts = new HashSet<PublishingContractMinimal>();
            EquipmentMaintenance = new HashSet<MaintenanceContractEquipmentDetails>();
        }

        /// <summary>
        /// get string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"contract Ref: {Reference}, status: {Status}, has {EquipmentMaintenance.Count} Equipment Maintenance, duration: {(StartDate - EndDate).TotalDays} Days";
    }
}
