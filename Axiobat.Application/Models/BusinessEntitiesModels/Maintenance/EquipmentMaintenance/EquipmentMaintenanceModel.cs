﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="EquipmentMaintenance"/>
    /// </summary>
    [ModelFor(typeof(EquipmentMaintenance))]
    public partial class EquipmentMaintenanceModel
    {
        /// <summary>
        /// the if of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name of the Equipment
        /// </summary>
        public string EquipmentName { get; set; }

        /// <summary>
        /// list of the equipment operations
        /// </summary>
        public ICollection<EquipmentMaintenanceOperation> MaintenanceOperations { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenanceModel"/>
    /// </summary>
    public partial class EquipmentMaintenanceModel : IModel<EquipmentMaintenance>, IUpdateModel<EquipmentMaintenance>
    {
        /// <summary>
        /// create an instant of <see cref="EquipmentMaintenanceModel"/>
        /// </summary>
        public EquipmentMaintenanceModel()
        {
            MaintenanceOperations = new HashSet<EquipmentMaintenanceOperation>();
        }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(EquipmentMaintenance entity)
        {
            entity.EquipmentName = EquipmentName;
            entity.MaintenanceOperations = MaintenanceOperations;
        }
    }
}
