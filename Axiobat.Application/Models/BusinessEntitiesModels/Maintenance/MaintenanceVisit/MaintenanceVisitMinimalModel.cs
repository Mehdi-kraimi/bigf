﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for <see cref="MaintenanceVisit"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceVisit))]
    public partial class MaintenanceVisitMinimalModel : IModel<MaintenanceVisit>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// status of the Visit, one of the values of <see cref="Constants.MaintenanceVisitStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the id of the client associated with the maintenance visit
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// the id of the contract
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the Technician associated with this Operation Sheet Maintenance
        /// </summary>
        public string Technician { get; set; }

        /// <summary>
        /// the id of the MaintenanceOperationSheet associated with this MaintenanceVisitModel
        /// </summary>
        public string MaintenanceOperationSheetId { get; set; }

        /// <summary>
        /// reference of the Maintenance OperationSheet associated with this visit
        /// </summary>
        public string MaintenanceOperationSheetReference { get; set; }
    }
}
