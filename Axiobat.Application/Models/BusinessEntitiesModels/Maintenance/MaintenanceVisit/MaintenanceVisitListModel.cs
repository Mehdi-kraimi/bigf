﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Constants;

    /// <summary>
    /// the list model for <see cref="MaintenanceVisit"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceVisit))]
    public class MaintenanceVisitListModel
    {
        /// <summary>
        /// the id of Maintenance Visit, this will have value if the Maintenance Visit has been generated
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// status of the Visit, one of the values of <see cref="MaintenanceVisitStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the client associated with the Maintenance visit
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the site of the maintenance visit
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the if of the contract
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the reference of the Maintenance Operation Sheet associated with this maintenance visit
        /// </summary>
        public string MaintenanceOperationSheetReference { get; set; }
    }
}
