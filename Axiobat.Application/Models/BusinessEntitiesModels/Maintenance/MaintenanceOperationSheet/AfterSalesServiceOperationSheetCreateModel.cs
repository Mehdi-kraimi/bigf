﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// create model for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceOperationSheet))]
    public partial class AfterSalesServiceOperationSheetCreateModel : MaintenanceOperationSheetBaseCreateModel
    {
        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the order details associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.AfterSalesService"/>
        /// </summary>
        public OrderDetails OrderDetails { get; set; }
    }
}
