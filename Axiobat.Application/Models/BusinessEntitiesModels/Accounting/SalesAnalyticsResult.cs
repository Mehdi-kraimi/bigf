﻿using Axiobat.Domain.Constants;
using DnsClient;
using System.Collections.Generic;
using System.Linq;

namespace Axiobat.Application.Models
{
    public partial class SalesAnalyticsResult
    {
        public QuoteAnalyticsResult QuoteDetails { get; set; }
        public IEnumerable<ExternalPartnerTransactionDetails> TopClients { get; set; }
        public SalesTurnoverDetails TurnoverDetails { get; set; }
        public SalesProductAnalyticsResult ProductsDetails { get; set; }
    }

    public partial class InvoiceAnalyticsResult
    {
        public float Total { get; set; }

        public int Count { get; set; }

        public IEnumerable<DocumentByStatus> QuotesByStatus { get; set; }
    }

    public class QuoteAnalyticsResult : DocumentAnalyticsResult
    {
        public float AcceptanceRate => QuotesByStatus
            .Where(e =>
                e.Status != QuoteStatus.Refused &&
                e.Status != QuoteStatus.Canceled &&
                e.Status != QuoteStatus.InProgress
            )
            .Sum(e => e.Percent);

        public IEnumerable<DocumentByStatus> QuotesByStatus => DocumentByStatus;
    }

    public class DocumentAnalyticsResult
    {
        public float Total { get; set; }

        public int Count { get; set; }

        public IEnumerable<DocumentByStatus> DocumentByStatus { get; set; }
    }

    public class DocumentByStatus
    {
        private float _total;

        public DocumentByStatus(float total)
        {
            _total = total;
        }

        public string Status { get; set; }

        public IEnumerable<MinimalDocumentModel> Quotes => Documents;

        public IEnumerable<MinimalDocumentModel> Documents { get; set; }

        public float Percent => Total / _total * 100;

        public float Total { get; set; }
    }

    public partial class SalesTurnoverDetails
    {
        public TurnoverPerPeriodIncreaseDetails TurnoverIncreaseDetails { get; set; }

        public TurnoverPerPeriodGoalDetail TurnoverAchievementDetails { get; set; }
    }

    public partial class SalesProductAnalyticsResult
    {
        public IEnumerable<ProductAnalyticsResult> TopProductsByTotalSales { get; set; }
        public IEnumerable<LabelAnalyticsResult> TopLabelsByTotalSales { get; set; }
    }

    public partial class LabelAnalyticsResult
    {
        public LabelModel Label { get; set; }
        public float Total { get; set; }
    }
}
