﻿namespace Axiobat.Application.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the financial Analytics result
    /// </summary>
    public partial class FinancialAnalyticsResult
    {
        /// <summary>
        /// the turn over details
        /// </summary>
        public TurnoverDetails TurnoverDetails { get; set; }

        /// <summary>
        /// the turn over Increase Details
        /// </summary>
        public TurnoverIncreaseDetails TurnoverIncreaseDetails { get; set; }

        /// <summary>
        /// the Turnover Achievement Details compare to the lase period
        /// </summary>
        public TurnoverGoalDetail TurnoverAchievementDetails { get; set; }
    }

    /// <summary>
    /// the details of the turn over
    /// </summary>
    public partial class TurnoverDetails
    {
        public float TotalIncome { get; set; }

        public float TotalUnpaidIncome { get; set; }

        public float TotalExpenses { get; set; }

        public float TotalUnpaidExpenses { get; set; }
    }

    public class ClientTotalTurnOver
    {
        public float TotalTurnOver { get; set; }

        public float TotalPayments { get; set; }
    }

    public class TurnoverIncreaseDetails
    {
        public TurnoverPerPeriodIncreaseDetails CurrentPeriod { get; set; }

        public TurnoverPerPeriodIncreaseDetails PreCurrentPeriod { get; set; }
    }

    public class TurnoverPerPeriodIncreaseDetails
    {
        public TurnoverPerPeriodIncreaseDetails()
        {
            MonthlyIncreaseDetails = new HashSet<TurnoverMonthlyIncreaseDetails>();
        }

        public float TotalIncome { get; set; }

        public float TotalExpense { get; set; }

        public float Margin => TotalIncome - TotalExpense;

        public ICollection<TurnoverMonthlyIncreaseDetails> MonthlyIncreaseDetails { get; set; }
    }

    public class TurnoverMonthlyIncreaseDetails
    {
        public TurnoverMonthlyIncreaseDetails()
        {
        }

        public TurnoverMonthlyIncreaseDetails(int month, float totalIncome, float totalExpense)
        {
            Month = month;
            TotalIncome = totalIncome;
            TotalExpense = totalExpense;
        }

        public int Month { get; set; }

        public float TotalIncome { get; set; }

        public float TotalExpense { get; set; }

        public float Margin => TotalIncome - TotalExpense;
    }

    /// <summary>
    /// set the turn over goal details
    /// </summary>
    public class TurnoverGoalDetail
    {
        public TurnoverGoalDetail(TurnoverGoalModel currentPeriodTurnoverGoal, TurnoverGoalModel perCurrentPeriodTurnover)
        {
            CurrentPeriod = new TurnoverPerPeriodGoalDetail(currentPeriodTurnoverGoal?.Goal ?? 0);
            PreCurrentPeriod = new TurnoverPerPeriodGoalDetail(currentPeriodTurnoverGoal?.Goal ?? 0);
        }

        public TurnoverPerPeriodGoalDetail CurrentPeriod { get; set; }

        public TurnoverPerPeriodGoalDetail PreCurrentPeriod { get; set; }
    }

    public class TurnoverPerPeriodGoalDetail
    {
        private readonly float _yearTurnoverGoal;

        public TurnoverPerPeriodGoalDetail(float yearTurnoverGoal)
        {
            MonthlyDetails = new HashSet<MonthlyTurnoverGoalAchievement>();
            _yearTurnoverGoal = yearTurnoverGoal;
        }

        /// <summary>
        /// the total
        /// </summary>
        public float Total => MonthlyDetails.Sum(e => e.Total);

        /// <summary>
        /// the percentage
        /// </summary>
        public float Percentage => Total / _yearTurnoverGoal * 100;

        /// <summary>
        /// the monthly Turnover Goal Achievement details
        /// </summary>
        public ICollection<MonthlyTurnoverGoalAchievement> MonthlyDetails { get; set; }
    }

    public class MonthlyTurnoverGoalAchievement
    {
        private readonly float _monthlyTurnover;

        public MonthlyTurnoverGoalAchievement(float monthlyTurnover)
        {
            _monthlyTurnover = monthlyTurnover;
        }

        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the total
        /// </summary>
        public float Total { get; set; }

        /// <summary>
        /// the percentage
        /// </summary>
        public float Percentage => Total / _monthlyTurnover * 100;
    }

    public class ClientDocumentDetails
    {
        public QuoteAnalyticsResult QuotesDetails { get; }
        public DocumentAnalyticsResult InvoicesDetails { get; }
        public DocumentAnalyticsResult WorkshopDetails { get; }
        public DocumentAnalyticsResult ContractDetails { get; }
        public DocumentAnalyticsResult OperationSheetDetails { get; }
        public ClientTotalTurnOver TurnOverDetails { get; }

        public ClientDocumentDetails(
            QuoteAnalyticsResult quotesDetails,
            DocumentAnalyticsResult invoicesDetails,
            DocumentAnalyticsResult workshopDetails,
            DocumentAnalyticsResult contractDetails,
            DocumentAnalyticsResult operationSheetDetails,
            ClientTotalTurnOver turnOverDetails)
        {
            QuotesDetails = quotesDetails;
            InvoicesDetails = invoicesDetails;
            WorkshopDetails = workshopDetails;
            ContractDetails = contractDetails;
            OperationSheetDetails = operationSheetDetails;
            TurnOverDetails = turnOverDetails;
        }
    }

    public partial class ClientTurnoverDetail
    {
        public ClientTurnoverDetail()
        {
            PerMonthTurnOver = new List<PerMonthTotalDetails>();
        }

        public ICollection<PerMonthTotalDetails> PerMonthTurnOver { get; set; }
    }

    /// <summary>
    /// the details of the total by month
    /// </summary>
    public partial class PerMonthTotalDetails
    {
        /// <summary>
        /// create and an instant of <see cref="PerMonthTotalDetails"/>
        /// </summary>
        /// <param name="year">the year</param>
        /// <param name="month">the month</param>
        /// <param name="total">the total</param>
        public PerMonthTotalDetails(int year, int month, float total)
        {
            Year = year;
            Month = month;
            Total = total;
            Date = new DateTime(year, month, 1);
        }


        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the turn over amount
        /// </summary>
        public float Total { get; set; }

        /// <summary>
        /// the date representation of the <see cref="Month"/> and <see cref="Year"/> combined
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public DateTime Date { get; }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns>the string vale</returns>
        public override string ToString()
            => $"month: {Month}, total: {Total}";
    }
}
