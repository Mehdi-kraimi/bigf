﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the <see cref="Product"/> model
    /// </summary>
    [ModelFor(typeof(Product))]
    public partial class ProductPutModel : ProductsBasePutModel<Product>
    {
        /// <summary>
        /// the product reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the total estimated hours for the product
        /// </summary>
        public float TotalHours { get; set; }

        /// <summary>
        /// the estimated material cost that the product will consume
        /// </summary>
        public float MaterialCost { get; set; }

        /// <summary>
        /// the cost of selling the product, the price is Hourly based.
        /// </summary>
        public float HourlyCost { get; set; }

        /// <summary>
        /// the Value added tax
        /// </summary>
        public float VAT { get; set; }

        /// <summary>
        /// the product unite
        /// </summary>
        public string Unite { get; set; }

        /// <summary>
        /// the id of the product category type
        /// </summary>
        public string ProductCategoryTypeId { get; set; }

        /// <summary>
        /// the product classification
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// list of labels
        /// </summary>
        public ICollection<LabelModel> Labels { get; set; }

        /// <summary>
        /// the product suppliers informations
        /// </summary>
        public ICollection<ProductSupplierInfo> ProductSuppliers { get; set; }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public override void Update(Product entity)
        {
            base.Update(entity);
            entity.VAT = VAT;
            entity.Unite = Unite;
            entity.Reference = Reference;
            entity.TotalHours = TotalHours;
            entity.Designation = Designation;
            entity.MaterialCost = MaterialCost;
            entity.ClassificationId = CategoryId;
            entity.ProductCategoryTypeId = ProductCategoryTypeId;
        }
    }
}
