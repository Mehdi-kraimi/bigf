﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// a model for <see cref="HoldbackDetails"/>
    /// </summary>
    [ModelFor(typeof(HoldbackDetails))]
    public class HoldbackDetailsModel
    {
        /// <summary>
        /// the id of the invoice
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// the reference of the invoice
        /// </summary>
        public string InvoiceReference { get; set; }

        /// <summary>
        /// the Warranty period
        /// </summary>
        public int WarrantyPeriod { get; set; }

        /// <summary>
        /// The holdback is a sum of money representing a percentage of the total amount of the work.
        /// Note: this value of this is a percentage
        /// </summary>
        /// <remarks>
        /// It is a term used during a public order corresponding to works contracts.
        /// The retention of guarantee refers to the law of July 16, 1971.
        /// If the client (the owner) or the supervisor (the person who organized and supervised the works)
        /// finds defects or faults, the company having carried out the work is withheld a
        /// sum equal to 5% of the amount of the work. The retention of this sum allows the client to exert
        /// pressure on the company until the work is completed and correctly.
        /// </remarks>
        public float Holdback { get; set; }

        /// <summary>
        /// the status of the Holdback
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Holdback Percent from the invoice
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the Warranty expiration Date
        /// </summary>
        public DateTime WarrantyExpirationDate { get; set; }
    }

    /// <summary>
    /// the update model for <see cref="HoldbackDetails"/>
    /// </summary>
    [ModelFor(typeof(HoldbackDetails))]
    public class HoldbackDetailsUpdateModel
    {
        /// <summary>
        /// the status of the Holdback
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Warranty expiration Date
        /// </summary>
        public DateTime WarrantyExpirationDate { get; set; }
    }
}
