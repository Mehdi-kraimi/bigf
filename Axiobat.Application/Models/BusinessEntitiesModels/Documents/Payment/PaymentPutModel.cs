﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Payment"/>
    /// </summary>
    [ModelFor(typeof(Payment))]
    public partial class PaymentPutModel
    {
        /// <summary>
        /// a description associated wit the payment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the type of the payment operation
        /// </summary>
        public PaymentOperation Operation { get; set; }

        /// <summary>
        /// the type of this payment
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// the date of the payment
        /// </summary>
        public DateTime DatePayment { get; set; }

        /// <summary>
        /// the payment amount
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// the id of the account
        /// </summary>
        public int? AccountId { get; set; }

        /// <summary>
        /// the id of the payment method
        /// </summary>
        public int? PaymentMethodId { get; set; }

        /// <summary>
        /// the if of the credit note
        /// </summary>
        public string CreditNoteId { get; set; }

        /// <summary>
        /// this list of invoices associated with this payment
        /// </summary>
        public ICollection<DocumentPaymentsModule> Invoices { get; set; }

        /// <summary>
        /// the list of expenses associated with this payment
        /// </summary>
        public ICollection<DocumentPaymentsModule> Expenses { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="PaymentMethod"/>
    /// </summary>
    public partial class PaymentPutModel : IUpdateModel<Payment>
    {
        /// <summary>
        /// create an instant of <see cref="PaymentPutModel"/>
        /// </summary>
        public PaymentPutModel()
        {
            Invoices = new HashSet<DocumentPaymentsModule>();
            Expenses = new HashSet<DocumentPaymentsModule>();
        }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(Payment entity)
        {
            entity.PaymentMethodId = PaymentMethodId;
            entity.Description = Description;
            entity.DatePayment = DatePayment;
            entity.Operation = Operation;
            entity.AccountId = AccountId;
        }
    }

    /// <summary>
    /// the model that defines the relation between the payment and invoices
    /// </summary>
    [ModelFor(typeof(Invoices_Payments))]
    public partial class DocumentPaymentsModule
    {
        /// <summary>
        /// the id of the invoices
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// the amount that the payment is made for the invoice
        /// </summary>
        public float Amount { get; set; }
    }
}
