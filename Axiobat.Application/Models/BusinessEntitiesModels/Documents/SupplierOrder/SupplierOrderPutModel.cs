﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System;

    /// <summary>
    /// the model for <see cref="SupplierOrder"/>
    /// </summary>
    [ModelFor(typeof(SupplierOrder))]
    public partial class SupplierOrderPutModel : DocumentPutModel<SupplierOrder>
    {
        /// <summary>
        /// the date the Supplier Order should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Supplier Order creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        [PropertyName("Supplier")]
        [PropertyValue("Supplier.FullName")]
        public string SupplierId { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="SupplierPutModel"/>
    /// </summary>
    public partial class SupplierOrderPutModel : DocumentPutModel<SupplierOrder>
    {
        /// <summary>
        /// update the entity from this model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public override void Update(SupplierOrder entity)
        {
            base.Update(entity);
            entity.DueDate = DueDate;
            entity.SupplierId = SupplierId;
            entity.Supplier = Supplier;
            entity.CreationDate = CreationDate;
            entity.OrderDetails = OrderDetails;
        }
    }
}
