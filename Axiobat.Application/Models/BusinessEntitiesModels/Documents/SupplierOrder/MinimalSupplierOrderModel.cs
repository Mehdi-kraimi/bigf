﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;

    /// <summary>
    /// a minimal model for <see cref="SupplierOrder"/>
    /// </summary>
    [ModelFor(typeof(SupplierOrder))]
    public partial class MinimalSupplierOrderModel : IModel<SupplierOrder>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the reference of the supplier order
        /// </summary>
        public string Reference { get; set; }
    }
}
