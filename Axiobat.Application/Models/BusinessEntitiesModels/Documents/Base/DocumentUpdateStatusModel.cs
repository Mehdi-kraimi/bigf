﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for <see cref="Document"/> for updating the status
    /// </summary>
    [ModelFor(typeof(Document))]
    public partial class DocumentUpdateStatusModel
    {
        /// <summary>
        /// status of the quote
        /// </summary>
        public string Status { get; set; }
    }
}
