﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// a shared model between all the documents
    /// </summary>
    public class DocumentSharedModel
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the Billing Address
        /// </summary>
        public Address BillingAddress { get; set; }

        /// <summary>
        /// the client associated with the invoice
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of payments associated with this invoice
        /// </summary>
        public ICollection<PaymentModel> Payments { get; set; }
    }
}
