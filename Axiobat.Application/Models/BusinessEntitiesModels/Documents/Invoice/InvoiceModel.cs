﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Invoice"/>
    /// </summary>
    [ModelFor(typeof(Invoice))]
    public partial class InvoiceModel
    {
        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the type of invoice
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the Billing Address
        /// </summary>
        public Address BillingAddress { get; set; }

        /// <summary>
        /// the Billing Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the client associated with the invoice
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the OperationSheetMaintenance associated with the Invoice
        /// </summary>
        public MaintenanceOperationSheetModel OperationSheetMaintenance { get; set; }

        /// <summary>
        /// the Contract associated with the Invoice
        /// </summary>
        public MaintenanceContractModel Contract { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the id of the Quote
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// list of memos associated with this entity
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of payments associated with this invoice
        /// </summary>
        public ICollection<PaymentModel> Payments { get; set; }

        /// <summary>
        /// the quote associated with this invoice
        /// </summary>
        public QuoteModel Quote { get; set; }

        /// <summary>
        /// a boolean check if you can cancel this invoice
        /// </summary>
        public bool CanCancel { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanUpdate { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="InvoiceModel"/>
    /// </summary>
    public partial class InvoiceModel : DocumentModel<Invoice>
    {
        /// <summary>
        /// create an instant of <see cref="InvoiceModel"/>
        /// </summary>
        public InvoiceModel() : base()
        {
            Emails = new HashSet<DocumentEmail>();
            Payments = new HashSet<PaymentModel>();
            Memos = new HashSet<Memo>();
        }
    }
}
