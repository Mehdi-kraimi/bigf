﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// a module used for updating the external partners entities <see cref="Client"/> and <see cref="Supplier"/>
    /// </summary>
    [ModelFor(typeof(ExternalPartner))]
    public partial class ExternalPartnerPutModel<TEntity>
        where TEntity : ExternalPartner
    {
        /// <summary>
        /// the supplier reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// first name of the client
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// last name of the client
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the LandLine
        /// </summary>
        public string LandLine { get; set; }

        /// <summary>
        /// the email of the client, should be in a proper email format but is not required
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// client website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Directory Identification System institutions
        /// </summary>
        public string Siret { get; set; }

        /// <summary>
        /// Intra-community VAT means value added tax (VAT) applied to commercial transactions between different countries of the European Union.
        /// for more informations <see href="https://debitoor.fr/termes-comptables/tva-intracommunautaire">HERE</see>
        /// </summary>
        public string IntraCommunityVAT { get; set; }

        /// <summary>
        /// the accounting identifier of the client
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// the list of contacts for this supplier
        /// </summary>
        /// <remarks>
        /// this is JSON Object
        /// </remarks>
        public ICollection<ContactInformation> ContactInformations { get; set; }


        /// <summary>
        /// the conditions of the note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ExternalPartnerModel{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public partial class ExternalPartnerPutModel<TEntity> : IUpdateModel<TEntity>
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public virtual void Update(TEntity entity)
        {
            entity.Note = Note;
            entity.Email = Email;
            entity.Siret = Siret;
            entity.Website = Website;
            entity.LandLine = LandLine;
            entity.LastName = LastName;
            entity.FirstName = FirstName;
            entity.PhoneNumber = PhoneNumber;      
            entity.AccountingCode = AccountingCode;
            entity.PaymentCondition = PaymentCondition;
            entity.IntraCommunityVAT = IntraCommunityVAT;
            entity.ContactInformations = ContactInformations;
    }
    }
}
