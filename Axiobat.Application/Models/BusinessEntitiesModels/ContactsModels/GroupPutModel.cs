﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the group model for updating and creating a group
    /// </summary>
    public partial class GroupPutModel : IUpdateModel<Group>
    {
        /// <summary>
        /// name of the group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(Group entity)
        {
            entity.Name = Name;
        }
    }
}
