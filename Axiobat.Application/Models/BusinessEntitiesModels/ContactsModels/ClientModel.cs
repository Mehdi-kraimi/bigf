﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a class that defines a Model for <see cref="Client"/>
    /// </summary>
    [ModelFor(typeof(Client))]
    public partial class ClientModel
    {
        /// <summary>
        /// the client code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// list of Addresses of the client
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// list of Materials of the client
        /// </summary>
        public ICollection<Material> Materials { get; set; }
        

        /// <summary>
        /// the id of the groupe that this client belongs to it
        /// </summary>
        public int? GroupeId { get; set; }

        /// <summary>
        /// the client type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the Billing Address
        /// </summary>
        [Newtonsoft.Json.JsonProperty]
        public Address BillingAddress => Addresses
            .FirstOrDefault(e => e.IsDefault)
            ?? Addresses.FirstOrDefault();
    }

    /// <summary>
    /// the partial part for <see cref="ClientModel"/>
    /// </summary>
    public partial class ClientModel : ExternalPartnerModel<Client>
    {
        /// <summary>
        /// get the default address of the client
        /// </summary>
        /// <returns>the client address</returns>
        public Address DefaultAddress()
            => Addresses.FirstOrDefault(e => e.IsDefault) ?? Addresses.First();
    }
}
