﻿namespace Axiobat.Application.Models.DataImport
{
    public class ProductColumnsDefinitions
    {
        public int Reference { get; set; }
        public int Classification { get; set; }
        public int VAT { get; set; }
        public int? Description { get; set; }
        public int Designation { get; set; }
        public int Unite { get; set; }
        public int TotalHours { get; set; }
        public int MaterialCost { get; set; }
        public int HourlyCost { get; set; }
        public int ProductCategoryTypeId { get; set; }
        public int? RefSupplier { get; set; }
        public int? PriceSupplier { get; set; }
    }
}