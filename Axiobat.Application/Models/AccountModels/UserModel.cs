﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that describe a <see cref="User"/> Model
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserModel : IModel<User, Guid>, IRecordable
    {
        /// <summary>
        /// the id of the user
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// the date the user has been created in
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// the registration number of the user
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Last name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// first name of the user
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// a flag to determine if this user account is the main account of the company
        /// </summary>
        public bool Principal { get; set; }

        /// <summary>
        /// Gets or sets the user name for this user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the email address for this user.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a telephone number for the user.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the status of the user : Active or Not
        /// </summary>
        public Status Statut { get; set; }

        /// <summary>
        /// date of the last login for the user
        /// </summary>
        public DateTime? LastLogin { get; set; }

        /// <summary>
        /// the entity modification history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// the role of the user
        /// </summary>
        public RoleModel Role { get; set; }
    }
}
