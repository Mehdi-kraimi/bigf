﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a class that defines a model that holds the token
    /// </summary>
    [ModelFor(typeof(User))]
    public class LoginModel
    {
        /// <summary>
        /// the generated token
        /// </summary>
        public string Token { get; set; }
    }
}
