﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// this class is used to describe the rest password requirements
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserRestPasswordModel
    {
        /// <summary>
        /// the id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// the token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// the new password
        /// </summary>
        public string Password { get; set; }
    }
}
