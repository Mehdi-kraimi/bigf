﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// The minimal model for the <see cref="Role"/> entity
    /// </summary>
    [ModelFor(typeof(Role))]
    public partial class RoleMinimalModel
    {
        /// <summary>
        /// the id of the role
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name/label of the role
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="RoleMinimalModel"/>
    /// </summary>
    public partial class RoleMinimalModel
    {
        /// <summary>
        /// create an instant of <see cref="RoleMinimalModel"/>
        /// </summary>
        public RoleMinimalModel()
        {

        }

        /// <summary>
        /// create an instant of <see cref="RoleMinimalModel"/>
        /// </summary>
        /// <param name="id">the id of the role</param>
        /// <param name="name">the name of the role</param>
        public RoleMinimalModel(string id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// get the string Application of the entity
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"name: {Name}";
    }
}
