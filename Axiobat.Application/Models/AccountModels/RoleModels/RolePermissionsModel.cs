﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// this model is used to update the access for the role
    /// </summary>
    [ModelFor(typeof(Role))]
    public class RolePermissionsModel : IUpdateModel<Role>
    {
        /// <summary>
        /// list of rights
        /// </summary>
        public ICollection<Permissions> Permissions { get; set; }

        /// <summary>
        /// update the entity from the current model instant
        /// </summary>
        /// <param name="entity">the role entity</param>
        public void Update(Role entity) {}
    }
}
