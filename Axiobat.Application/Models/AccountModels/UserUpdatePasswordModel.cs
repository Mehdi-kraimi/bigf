﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// a model that describe the Updating password Requirements
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserUpdatePasswordModel
    {
        /// <summary>
        /// the old password of the user
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// the new password
        /// </summary>
        public string NewPassword { get; set; }
    }

    /// <summary>
    /// update the user password
    /// </summary>
    public class UpdateUserPasswordModel
    {
        /// <summary>
        /// the id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// the password of the user
        /// </summary>
        public string Password { get; set; }
    }
}
