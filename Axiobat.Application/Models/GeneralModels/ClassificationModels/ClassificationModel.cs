﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="Classification"/>
    /// </summary>
    [ModelFor(typeof(Classification))]
    public class ClassificationMinimalModel : IModel<Classification, int>
    {
        /// <summary>
        /// the id of the classification
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the type of the classification
        /// </summary>
        public ClassificationType Type { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the chart of account associated with this classification
        /// </summary>
        public ChartAccountItemMinimalModel ChartAccountItem { get; set; }

        /// <summary>
        /// the id of the parent classification
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// the implicit conversion between the <see cref="Classification"/> and <see cref="ChartAccountItemMinimalModel"/>
        /// </summary>
        /// <param name="classification">the <see cref="Classification"/> instant</param>
        public static implicit operator ClassificationMinimalModel(Classification classification)
            => classification is null ? null : new ClassificationMinimalModel
            {
                Id = classification.Id,
                Type = classification.Type,
                Label = classification.Label,
                Description = classification.Description,
                ChartAccountItem = classification.ChartAccountItem,
                ParentId = classification.ParentId,
            };
    }

    /// <summary>
    /// model for <see cref="Classification"/>
    /// </summary>
    [ModelFor(typeof(Classification))]
    public class ClassificationModel : IModel<Classification, int>
    {
        /// <summary>
        /// the id of the classification
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the type of the classification
        /// </summary>
        public ClassificationType Type { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the chart of account associated with this classification
        /// </summary>
        public ChartAccountItemMinimalModel ChartAccountItem { get; set; }

        /// <summary>
        /// list of sub classification associated with this classification
        /// </summary>
        public ICollection<ClassificationModel> SubClassification { get; set; }
    }
}
