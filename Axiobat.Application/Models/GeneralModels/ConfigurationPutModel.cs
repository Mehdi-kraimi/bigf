﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// this model is used to pass the configuration value
    /// </summary>
    public partial class ConfigurationPutModel
    {
        public string Value { get; set; }
    }
}
