﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the filter options of the workshop document
    /// </summary>
    public partial class WorkshopDocumentFilterOptions : FilterOptions
    {
        /// <summary>
        /// the id of the rubric
        /// </summary>
        public string RubricId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string WorkshopId { get; set; }
    }
}
