﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the <see cref="Domain.Entities.Payment"/> filter options
    /// </summary>
    public partial class PaymentFilterOptions : DateRangeFilterOptions
    {
        /// <summary>
        /// id of the account used with the payments
        /// </summary>
        public int? AccountId { get; set; }
    }
}
