﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the filter options for mission type
    /// </summary>
    public class MissionTypeFilterOptions : FilterOptions
    {
        public MissionTypeFilterOptions()
        {
            MissionKinds = new HashSet<string>();
        }

        /// <summary>
        /// list of types to retrieve
        /// </summary>
        public ICollection<string> MissionKinds { get; set; }
    }
}
