﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a model for <see cref="PaymentMethod"/>
    /// </summary>
    [ModelFor(typeof(PaymentMethod))]
    public partial class PaymentMethodModel : IModel<PaymentMethod, int>, IUpdateModel<PaymentMethod>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the name of the payment method
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// is this payment method the default one
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// update the entity from the given model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(PaymentMethod entity)
        {
            entity.Value = Value;
        }
    }
}
