﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// Holder for credential parameters
    /// </summary>
    public class GoogleCalendarOptions
    {
        /// <summary>
        /// Type of the credential.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Project ID associated with this credential.
        /// </summary>
        public string Project_Id { get; set; }

        /// <summary>
        /// Client Id associated with UserCredential created by GCloud Auth Login.
        /// </summary>
        public string Client_Id { get; set; }

        /// <summary>
        /// Client Secret associated with UserCredential created by GCloud Auth Login.
        /// </summary>
        public string Client_Secret { get; set; }

        /// <summary>
        /// Client Email associated with ServiceAccountCredential obtained from Google Developers Console
        /// </summary>
        public string Client_Email { get; set; }

        /// <summary>
        /// Private Key associated with ServiceAccountCredential obtained from Google Developers Console.
        /// </summary>
        public string Private_Key { get; set; }

        /// <summary>
        /// Refresh Token associated with UserCredential created by GCloud Auth Login.
        /// </summary>
        public string Refresh_Token { get; set; }
    }
}
