﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;

    /// <summary>
    /// a minimal model for the chart Account Category
    /// </summary>
    public partial class ChartAccountItemMinimalModel
    {
        /// <summary>
        /// the id of the category
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// type of the chart category
        /// </summary>
        public ChartAccountType Type { get; set; }

        /// <summary>
        /// the type of the category
        /// </summary>
        public CategoryType CategoryType { get; private set; }

        /// <summary>
        /// the accounting code associated with this category
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// the id of the parent category
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// the value of the tax, only check for this value when the <see cref="Type"/> is <see cref="ChartAccountType.VAT"/>
        /// </summary>
        public float VATValue { get; set; }

        /// <summary>
        /// the implicit conversion between the <see cref="ChartAccountItem"/> and <see cref="ChartAccountItemMinimalModel"/>
        /// </summary>
        /// <param name="category">the <see cref="ChartAccountItem"/> instant</param>
        public static implicit operator ChartAccountItemMinimalModel(ChartAccountItem category)
            => category is null ? null : new ChartAccountItemMinimalModel
            {
                Id = category.Id,
                Type = category.Type,
                Label = category.Label,
                CategoryType = category.CategoryType,
                Code = category.Code,
                ParentId = category.ParentId,
            };
    }
}
