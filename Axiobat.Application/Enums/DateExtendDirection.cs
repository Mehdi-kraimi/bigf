﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the direction of extending the date period
    /// </summary>
    public enum DateExtendDirection
    {
        /// <summary>
        /// extend the date from both sides
        /// </summary>
        BothSides,

        /// <summary>
        /// extend only from the starting date side
        /// </summary>
        StartingDate,

        /// <summary>
        /// extend only from the ending date side
        /// </summary>
        EndingDate,
    }
}
