﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the type of the journal entry
    /// </summary>
    public enum JournalEntryType
    {
        /// <summary>
        /// the journal entry is the main entry
        /// </summary>
        Main,

        /// <summary>
        /// the journal entry holds the tax details
        /// </summary>
        TaxDetails,

        /// <summary>
        /// the journal entry holds the product journal details
        /// </summary>
        ProductCategory,

        /// <summary>
        /// the journal entry holds the payment infos
        /// </summary>
        Payment
    }
}
