﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this interface represent the information about the current logged in user
    /// </summary>
    public interface ILoggedInUser
    {
        /// <summary>
        /// the id of the current logged in user
        /// </summary>
        Guid UserId { get; }

        /// <summary>
        /// the user name of the user
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// the email of the user
        /// </summary>
        string userEmail { get; }

        /// <summary>
        /// Role Id
        /// </summary>
        Guid RoleId { get; }

        /// <summary>
        /// the role name
        /// </summary>
        string RoleName { get; }

        /// <summary>
        /// the type of the role this user has
        /// </summary>
        RoleType RoleType { get; }

        /// <summary>
        /// the list of permissions this user owns
        /// </summary>
        IEnumerable<Permissions> Permissions { get; }
    }
}