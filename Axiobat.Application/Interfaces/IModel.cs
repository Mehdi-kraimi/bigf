﻿namespace Axiobat.Application.Models
{
    using Domain.Interfaces;

    /// <summary>
    /// every business should implement
    /// </summary>
    /// <typeparam name="TEntity">the entity this model is created for it</typeparam>
    public interface IModel<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        TKey Id { get; set; }
    }

    /// <summary>
    /// every business should implement
    /// </summary>
    /// <typeparam name="TEntity">the entity this model is created for it</typeparam>
    public interface IModel<TEntity> : IModel<TEntity, string>
        where TEntity : class, IEntity<string>
    { }
}
